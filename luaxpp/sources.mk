##############################################################################
# Lunax -- a Lua/C framework for creating multitask applications.
#
# Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
#
# [ Apache 2.0 license - see the file LICENSE. ]
##############################################################################

OBJ_SRCS := 
ASM_SRCS := 
C_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
LIBRARIES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
./src \

