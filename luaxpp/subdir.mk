##############################################################################
# Lunax -- a Lua/C framework for creating multitask applications.
#
# Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
#
# [ Apache 2.0 license - see the file LICENSE. ]
##############################################################################

#DEFS = -DDEBUG
DEFS = 
CC ?= gcc
# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += $(wildcard src/*.c)

OBJS += $(patsubst src/%.c,obj/%.o,$(wildcard src/*.c))

C_DEPS += $(patsubst src/%.c,obj/%.d,$(wildcard src/*.c))

# Each subdirectory must supply rules for building sources it contributes
obj/%.o: src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	$(CC)  -I"./inc" -I"../build" $(DEFS) -O0 -fPIC -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
