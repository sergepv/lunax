/*
** lunax.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include <stdio.h>
#include <stdlib.h>
#include <lua.h>
#include "lauxlib.h"
#include <lualib.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <signal.h>

#include "luaxpp.h"
#include <libgen.h>
#include <sys/wait.h>

void bail(lua_State *L, char *msg)
{
    fprintf(stderr, "\nFATAL ERROR:\n  %s: %s\n\n", msg, lua_tostring(L, -1));
    exit(1);
}

#define RUN_GET_PIDS "lunax=require(\"lunax\"); lunax.init(); t = lunax.app.getPIDs(); __LUNAX_IOTABLE = t;"

static int find(char** arr, char* str)
{
    while (*arr != NULL)
    {
        if (strcmp(*arr, str) == 0)
            return 1;
        arr++;
    }
    return 0;
}

static int get_apps(char** arr)
{
    DIR *dir;
    struct dirent *dp;
    char* file_name;
    char str[256];
    int arr_cnt = 0;
    if (arr)
        arr[0] = 0;
    dir = opendir("/dev/shm");
    while ((dp = readdir(dir)) != NULL)
    {
        // printf("debug: %s\n", dp->d_name);
        if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
        {
            // do nothing (straight logic)
        }
        else
        {
            file_name = dp->d_name; // use it
            str[0] = 0;
            if ((sscanf(file_name, NAME_SHARED_MEM_DEV"%s", str))
                    || (sscanf(file_name, NAME_SEM_MAIN_DEV"%s", str))
                    || (sscanf(file_name, NAME_SEM_ALLOC_DEV"%s", str))
                    || (sscanf(file_name, NAME_SEM_ALLOC_CNT_DEV"%s", str)))
            {
                if (arr)
                {
                    if (str[0] == 0)
                    {
                        if (find(arr, "default") == 0)
                        {
                            arr[arr_cnt] = strdup("default");
                            arr_cnt++;
                            arr[arr_cnt] = 0;
                        }
                    }
                    else
                    {
                        if (find(arr, str) == 0)
                        {
                            arr[arr_cnt] = strdup(str);
                            arr_cnt++;
                            arr[arr_cnt] = 0;
                        }
                    }
                }
                else
                    arr_cnt++;
            }
        }
    }
    closedir(dir);
    return arr_cnt;
}

static int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

static pid_t call_proc(char* str, char * extra_name, char* luadir, char* args)
{
    lua_State* L;
    char* newpath;
    int len_newpath;

    L = luaxL_newstate(extra_name); /* Create Lua state variable + init global semaphore */
    luaL_openlibs(L); /* Load Lua libraries */

    lua_getglobal(L,"package");
    lua_getfield(L,-1,"path");

    len_newpath = strlen(lua_tostring(L,-1)) + strlen(luadir);
    newpath = malloc(len_newpath);
    newpath[0]=0;
    strcat(newpath,lua_tostring(L,-1));
    strcat(newpath,luadir);
    lua_pop(L,1);
    lua_pushstring(L,newpath);
    lua_setfield(L,-2,"path");
    lua_pop(L,1);
    free(newpath);

    lua_newtable(L);
    {
        lua_pushstring(L, extra_name);
        lua_setfield(L, -2, "name");
        lua_pushstring(L, args);
        lua_setfield(L, -2, "args");
    }
    lua_setglobal(L, "__LUNAX_IOTABLE");
    if (luaL_loadfile(L, str)) /* Load but don't run the Lua script */
        bail(L, "luaL_loadfile() failed"); /* Error out if file can't be read */

    if (lua_pcall(L, 0, 1, 0)) /* Run the loaded Lua script */
        bail(L, "lua_pcall() failed"); /* Error out if Lua file has an error */

    if (lua_gettop(L))
    {
//        if (lua_tointeger(L,-1))
//            res = EXIT_FAILURE;
        lua_pop(L,1);
    }
    lua_close(L); /* Clean up, free the Lua state var */

    return 0;
}

static pid_t create_fork(char* str, char * extra_name, char* luadir, char* args)
{
    pid_t pid;
    lua_State* L;
    char* newpath;
    int len_newpath;
    int res = EXIT_SUCCESS;

    pid = fork();
    switch (pid)
    {
    case -1:
        perror("fork");
        exit(1);
        break;
    case 0:
        L = luaxL_newstate(extra_name); /* Create Lua state variable + init global semaphore */
        luaL_openlibs(L); /* Load Lua libraries */

        lua_getglobal(L,"package");
        lua_getfield(L,-1,"path");

        len_newpath = strlen(lua_tostring(L,-1)) + strlen(luadir);
        newpath = malloc(len_newpath);
        newpath[0]=0;
        strcat(newpath,lua_tostring(L,-1));
        strcat(newpath,luadir);
        lua_pop(L,1);
        lua_pushstring(L,newpath);
        lua_setfield(L,-2,"path");
        lua_pop(L,1);
        free(newpath);

        lua_newtable(L);
        {
            lua_pushstring(L, extra_name);
            lua_setfield(L, -2, "name");
            lua_pushstring(L, args);
            lua_setfield(L, -2, "args");
        }
        lua_setglobal(L, "__LUNAX_IOTABLE");

// It is needed for debuging only for investigation lua token filter (debug path in proxy.c)
        lua_pushinteger(L,1);
        lua_setglobal(L,"FILTER");
//

        if (luaL_loadfile(L, str)) /* Load but don't run the Lua script */
            bail(L, "luaL_loadfile() failed"); /* Error out if file can't be read */

        if (lua_pcall(L, 0, 1, 0)) /* Run the loaded Lua script */
            bail(L, "lua_pcall() failed"); /* Error out if Lua file has an error */

        if (lua_gettop(L))
        {
            if (lua_tointeger(L,-1))
                res = EXIT_FAILURE;
            lua_pop(L,1);
        }
        lua_close(L); /* Clean up, free the Lua state var */
        exit(res);
        break;
    default:
        break;
    }
    return pid;

}

static int start_project(char* str, char * extra_name, char* args, int dbg)
{
    lua_State* L;
    char* lpath;
    char* lfile;
    char* str1;
    char* str2;
    int isdir = 0;
    int count_processes = 0;
    pid_t pid = 0;

    char* str_ex = NULL;
    char* extra_name_ex = NULL;
    char* luadir_ex = NULL;

    str1 = strdup(str);
    str2 = strdup(str);

    if (is_regular_file(str) == 0)
    {
        isdir = 1;
        lpath = str;
    }
    else
    {
        lpath = dirname(str1);
        lfile = basename(str2);
    }
    if (isdir || (strcmp(lfile,"project.lnx") == 0))
    {
        char* newpath;
        int len_newpath;
        L = luaL_newstate(); /* Create Lua state variable + init global semaphore */
        luaL_openlibs(L); /* Load Lua libraries */


        lua_getglobal(L,"package");
        lua_getfield(L,-1,"path");

        len_newpath = strlen(lua_tostring(L,-1)) + strlen(lpath)+8;
        newpath = malloc(len_newpath);
        newpath[0]=0;
        strcat(newpath,lua_tostring(L,-1));
        strcat(newpath, ";");
        strcat(newpath,lpath);
        strcat(newpath, "/?.lnx");
        lua_pop(L,1);
        lua_pushstring(L,newpath);
        lua_setfield(L,-2,"path");
        lua_pop(L,1);
        free(newpath);

        lua_newtable(L);
        {
            lua_pushstring(L, lpath);
            lua_setfield(L, -2, "path");
            lua_pushstring(L, extra_name);
            lua_setfield(L, -2, "extra_name");
        }
        lua_setglobal(L, "__LUNAX_IOTABLE");

        if (luaL_loadfile(L, "lunaxprj.lua")) /* Load but don't run the Lua script */
            bail(L, "luaL_loadfile() failed"); /* Error out if file can't be read */

        if (lua_pcall(L, 0, 0, 0)) /* Run the loaded Lua script */
            bail(L, "lua_pcall() failed"); /* Error out if Lua file has an error */

        lua_pushnil(L);
        lua_getglobal(L, "__LUNAX_IOTABLE");
        //L | nil; out_table;
        lua_pushnil(L);
        while (lua_next(L, -2) != 0)
        {
        	int ldebug = 0;
        	 //L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table)
        	lua_getfield(L,-1,"debug");
        	if (lua_isinteger(L,-1))
        	{
        		ldebug = lua_tointeger(L,-1);
        	}
        	//L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table); debug
        	lua_pop(L,1);
        	//L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table);
        	if (dbg && ldebug)
        	{
        		lua_pushvalue(L,-1);
        		//L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table); value(proc_data_table)
        		lua_replace(L,-5);
        		//L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table)
        		lua_pop(L, 1);
        		dbg = 0;
        		continue;
        	}
        	//L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table)
            lua_getfield(L,-1,"main");
            lua_getfield(L,-2,"shared");
            lua_getfield(L,-3,"dir");
            //L | nil(dbg proc table); out_table; key(proc_name); value(proc_data_table); main; shared; dir
            char* shared = ( strcmp((char*)lua_tostring(L,-2),"") == 0)? NULL:(char*)lua_tostring(L,-2);
            pid = create_fork((char*)lua_tostring(L,-3), shared, (char*)lua_tostring(L,-1), args);
            count_processes++;
            lua_pop(L, 4);
            if (!pid) break;
        }
        //L | nil(dbg proc table); out_table;
        lua_pop(L, 1);
        //L | nil(dbg proc table);
        if (pid && (!lua_isnil(L,-1)))
        {
        	//L | dbg proc table;
        	lua_getfield(L,-1,"main");
        	lua_getfield(L,-2,"shared");
        	lua_getfield(L,-3,"dir");
        	//L | dbg proc table; main; shared; dir
        	char* shared = ( strcmp((char*)lua_tostring(L,-2),"") == 0)? NULL:(char*)lua_tostring(L,-2);
        	call_proc((char*)lua_tostring(L,-3), shared, (char*)lua_tostring(L,-1), args);
        	count_processes++;
        	lua_pop(L, 3);
        }
        lua_pop(L, 1);
        lua_close(L); /* Clean up, free the Lua state var */

    }
    else
    {
        if (str) str_ex = strdup(str);
        if (extra_name) extra_name_ex = strdup(extra_name);
        if (lpath)
        {
            luadir_ex = malloc(strlen(lpath)+8);
            luadir_ex[0] = 0;
            strcat(luadir_ex, ";");
            strcat(luadir_ex, lpath);
            strcat(luadir_ex, "/?.lua");
        }
        if (!dbg)
        {
            pid = create_fork(str_ex, extra_name_ex, luadir_ex,args);
        }
        else
        {
            call_proc(str_ex, extra_name_ex, luadir_ex,args);
        }
        count_processes++;
        free(str_ex);
        free(extra_name_ex);
        free(luadir_ex);
    }
    free(str1);
    free(str2);
    return (pid)?count_processes:-1;
}

static void run_fork2delete(char * extra_name)
{
    pid_t pid;
    lua_State* L;

    pid = fork();
    switch (pid)
    {
    case -1:
        perror("fork");
        exit(1);
        break;
    case 0:
        L = luaxL_newstate(extra_name); /* Create Lua state variable + init global semaphore */
        luaL_openlibs(L); /* Load Lua libraries */

        lua_newtable(L);
        {
            lua_pushstring(L, extra_name);
            lua_setfield(L, -2, "name");
        }
        lua_setglobal(L, "__LUNAX_IOTABLE");
        if (luaL_loadstring(L, RUN_GET_PIDS)) /* Load but don't run the Lua script */
            bail(L, "luaL_loadstring() failed"); /* Error out if file can't be read */

        if (lua_pcall(L, 0, 0, 0)) /* Run the loaded Lua script */
            bail(L, "lua_pcall() failed"); /* Error out if Lua file has an error */

        lua_getglobal(L, "__LUNAX_IOTABLE");
        //L | out_table;
        lua_pushnil(L);
        while (lua_next(L, -2) != 0)
        {
            //L | out_table; key(PID); value(COUNT)
            printf("%"LUA_INTEGER_FRMLEN"d [ %"LUA_INTEGER_FRMLEN"d ]\n", lua_tointeger(L, -2), lua_tointeger(L, -1));
            if (lua_tointeger(L,-2) != getpid())
            {
                kill(lua_tointeger(L, -2), SIGINT);
            }
            lua_pop(L, 1);
        }
        lua_pop(L, 1);

        lua_close(L); /* Clean up, free the Lua state var */
        exit(0);
        break;
    default:
        break;
    }
}

//#include <stdlib.h>
//#include <stdio.h>
//double t[0x1];
//const double t1[0x02];
//void segments()
//{
//	static int s = 42;
//	void *p = malloc(1024);
//	printf("stack\t%010p\nbrk\t%010p\nheap\t%010p\n""static\t%010p\nstatic\t%010p\ntext\t%010p\n",&p, sbrk(0), p, t, &s, segments);
//}
//
//int main(int argc, char *argv[])
//{
//	segments();
//	exit(0);
//}
int main(int argc, char **argv)
{
    int i = 1;
    int k;
    char* lname = NULL;
    char** arr;
    char** carr;
    int count;
    char buf[256];

    unsigned int procs = 0;
    int status;
    int procs_status = 0;
    int curr;
    int istest =0;
    int isdebug =0;
    int nofork = -1;
    enum
    {
        CMD_LS, CMD_DEL, CMD_CREATE, CMD_CLEAN
    } cmd;

    cmd = CMD_CREATE;
    printf("Luaxpp  \n");

    if (argc > 1)
    {
        while ((argc > i) && (argv[i][0] == '-'))
        {
            if (strcmp(argv[i], "-ls") == 0)
            {
                cmd = CMD_LS;
            }
            if (strcmp(argv[i], "-name") == 0)
            {
                i++;
                lname = argv[i];
            }
            if (strcmp(argv[i], "-del") == 0)
            {
                cmd = CMD_DEL;
            }
            if (strcmp(argv[i], "-clean") == 0)
            {
                cmd = CMD_CLEAN;
            }
            if (strcmp(argv[i], "-test") == 0)
            {
                istest = 1;
            }
            if (strcmp(argv[i], "-debug") == 0)
            {
                isdebug = 1;
            }
            i++;
        }
        switch (cmd)
        {
        case CMD_LS:
            count = get_apps(NULL);
            arr = calloc(count, sizeof(char*));
            carr = arr;
            count = get_apps(arr);
            while (*carr != NULL)
            {
                printf("--> %s\n", *carr);
                free(*carr);
                carr++;
            }
            free(arr);
            break;
        case CMD_DEL:
            run_fork2delete(lname);
            break;
        case CMD_CLEAN:
            count = get_apps(NULL);
            arr = calloc(count, sizeof(char*));
            carr = arr;
            count = get_apps(arr);
            while (*carr != NULL)
            {
                if (strcmp(*carr,"default") == 0)
                {
                    printf("SERGE_P ========q=====> NULL\n");
                    run_fork2delete(NULL);
                }
                else
                {
                    run_fork2delete(*carr);
                }
                carr++;
            }
            sleep(1);
            carr = arr;
            while (*carr != NULL)
            {
                char* name =NULL;
                if (strcmp(*carr,"default") != 0)
                {
                    name = *carr;
                }
                sprintf(buf, "/dev/shm/"NAME_SHARED_MEM_DEV"%s", name);
                remove(buf);
                sprintf(buf, "/dev/shm/"NAME_SEM_MAIN_DEV"%s", name);
                remove(buf);
                sprintf(buf, "/dev/shm/"NAME_SEM_ALLOC_DEV"%s", name);
                remove(buf);
                sprintf(buf, "/dev/shm/"NAME_SEM_ALLOC_CNT_DEV"%s", name);
                remove(buf);
                free(*carr);
                carr++;
            }
            free(arr);
            break;
        case CMD_CREATE:
            if (argc > i)
            {
                char* task_name;
                int task_args_size = 256;
                int task_args_cnt = 0;
                char* task_args =  malloc(task_args_size);
                task_args[0] = 0;

                task_name = argv[i];

                for (k = i+1; k < argc; k++)
                {
                    if (task_args_size <= task_args_cnt+ strlen(argv[k])+1)
                    {
                        task_args_size = strlen(argv[k]) + 256;
                        task_args = realloc(task_args, task_args_size);
                    }
                    strcat(task_args," ");
                    strcat(task_args,argv[k]);
                    task_args_cnt += strlen(argv[k])+1;
                }
                nofork = start_project(task_name, lname, task_args,isdebug);
                if (nofork != -1)
                {
                	procs += nofork;
                }
                free(task_args);

            }
            break;
        }
    }
    if ((nofork != -1) && (istest) && (procs))
    {
        //printf("Wait finishing %d procs\n",procs);
        curr = 0;
        while (curr < procs)
        {
            wait(&status);
            procs_status += WEXITSTATUS(status);
            curr++;
        }

    }
    return (procs_status)?EXIT_FAILURE:EXIT_SUCCESS;
}
