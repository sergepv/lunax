/*
 * mppa.h
 *
 *  Created on: Aug 19, 2018
 *      Author: serge
 */

#ifndef LUNAX_CODE_INC_SHARED_PMPA_H_
#define LUNAX_CODE_INC_SHARED_PMPA_H_

/*
 * pmpa.h
 * Part of pmpa
 * Copyright (c) 2014 Philip Wernersbach
 *
 * Dual-Licensed under the Public Domain and the Unlicense.
 * Choose the one that you prefer.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#define PMPA_DETECT 0x11436d7LL
#define PMPA_DETECT_AREA 0xf0000LL

#if defined(__i386__)
#define ARCH_32BIT
#elif defined(__x86_64__)
#define ARCH_64BIT
#elif defined(__arm__)
#define ARCH_32BIT
#else
#error "Unknown platform architecture."
#endif

#if defined(ARCH_32BIT)
typedef uint32_t pmpa_memory_int;
#elif defined(ARCH_64BIT)
typedef uint64_t pmpa_memory_int;
#else
#error "ARCH_32BIT or ARCH_64BIT must be specified!"
#endif

/*
 *  Safe functions
 */
void *pmpa_alloc_sf (void* ud, void *ptr, size_t osize, size_t nsize);

void *pmpa_malloc_sf(void* base_addr, size_t size);
void *pmpa_calloc_sf(void* base_addr, size_t nelem, size_t elsize);
void *pmpa_realloc_sf(void* base_addr,void *ptr, size_t size);
void pmpa_free_sf(void *ptr);

/*
 * Externally accessible API functions.
 */

void *pmpa_alloc (void *ud, void *ptr, size_t osize, size_t nsize);
bool pmpa_init_lunax(void* base_addr, pmpa_memory_int size);
int pmpa_get_offset();

/*
 * Externally accessible C memory functions.
 */

void *pmpa_malloc(void* base_addr, size_t size);
void *pmpa_calloc(void* base_addr, size_t nelem, size_t elsize);
void *pmpa_realloc(void* base_addr,void *ptr, size_t size);
void pmpa_free(void *ptr);

#ifdef PMPA_OVERRIDE_C_MEMORY_FUNCTIONS
#ifndef PMPA_NO_OVERRIDE_C_MEMORY_FUNCTIONS

#define malloc(a) pmpa_malloc(a)
#define calloc(a, b) pmpa_calloc(a, b)
#define realloc(a, b) pmpa_realloc(a, b)
#define free(a) pmpa_free(a)

#endif
#endif


#endif /* LUNAX_CODE_INC_PMPA_H_ */
