/*
 * lrel.h
 *
 *  Created on: Aug 31, 2018
 *      Author: serge
 */

#ifndef LUA_CODE_SRC_LREL_H_
#define LUA_CODE_SRC_LREL_H_

#include "shared_pmpa.h"

#if defined(__i386__)
extern uint32_t G_LUNAX_ADDR;
#define POINTER_INT uint32_t
#define ARCH_32BIT
#define POINTER_ADDR uint32_t
#define MARKER 0xF0000000
#define N_MARKER 0x0FFFFFFF
#elif defined(__x86_64__)
extern uint64_t G_LUNAX_ADDR;
#define POINTER_INT uint64_t
#define ARCH_64BIT
#define POINTER_ADDR uint64_t
#define MARKER 0xF000000000000000
#define N_MARKER 0x0FFFFFFFFFFFFFFF
#elif defined(__arm__)
extern uint32_t G_LUNAX_ADDR;
#define POINTER_INT uint32_t
#define ARCH_32BIT
#define POINTER_ADDR uint32_t
#define MARKER 0xF0000000
#define N_MARKER 0x0FFFFFFF
#else
#error "Unknown platform architecture."
#endif

//#define LUNAX_SINGLE
#ifndef LUNAX_SINGLE

//#define REL_CAST(X) (((POINTER_INT)(X) && (POINTER_INT)(X) < PMPA_DETECT_AREA)?(typeof(X))((char*)(X)+G_LUNAX_ADDR):(X))
#define REL_CAST(X) (((POINTER_INT)(X) && ((POINTER_INT)(X) >= MARKER))?(typeof(X))(((POINTER_ADDR)(X) & N_MARKER) +G_LUNAX_ADDR):(X))

#define REL_RE_CASTL(X) (((POINTER_INT)(X) && (POINTER_INT)(L->relative_id) == PMPA_DETECT)?(typeof(X))(((POINTER_ADDR)(X)-G_LUNAX_ADDR)|MARKER):(X))

#define REL_RE_CASTg(X) (((POINTER_INT)(X) && (POINTER_INT)(g->frealloc) == PMPA_DETECT)?(typeof(X))(((POINTER_ADDR)X-G_LUNAX_ADDR)|MARKER):(X))

#define REL_obj2gco(L,x_) (L && (POINTER_INT)(obj2gco(x_)) && (POINTER_INT)(L->relative_id) == PMPA_DETECT)?\
    		(typeof(obj2gco(x_)))(((POINTER_ADDR)obj2gco(x_)-G_LUNAX_ADDR)|MARKER):obj2gco(x_);
#define REL_i_g(L, i_g) ((POINTER_INT)(i_g) && (POINTER_INT)(L->relative_id) == PMPA_DETECT)?\
		(typeof(i_g))(((POINTER_ADDR)i_g-G_LUNAX_ADDR)|MARKER):i_g;
#else
#define REL_CAST(X) (X)
#define REL_RE_CASTL(X) (X)
#define REL_RE_CASTg(X) (X)
#define REL_obj2gco(L,x_) obj2gco(x_);
#define REL_i_g(L, i_g) i_g;
#endif

#define REL_linkgclist_g(o,p)	((o)->gclist = (p), (p) = REL_RE_CASTg(obj2gco(o)))

#define REL_luaM_reallocvector_l(L, v,oldn,n,t) \
   (cast(t *, luaM_reallocv(L, REL_CAST(v), oldn, n, sizeof(t))))

#endif /* LUA_CODE_SRC_LREL_H_ */
