/*
 * mppa_internals.h
 *
 *  Created on: Aug 19, 2018
 *      Author: serge
 */

#ifndef LUNAX_CODE_INC_SHARED_PMPA_INTERNALS_H_
#define LUNAX_CODE_INC_SHARED_PMPA_INTERNALS_H_

/*
 * pmpa_internals.h
 * Part of pmpa
 * Copyright (c) 2014 Philip Wernersbach
 *
 * Dual-Licensed under the Public Domain and the Unlicense.
 * Choose the one that you prefer.
 */

#include <stddef.h>
#include <stdbool.h>
#include "shared_pmpa.h"

typedef struct {
	pmpa_memory_int size;
	bool allocated;
	char data;
} pmpa_memory_block;

#ifdef PMPA_UNIT_TEST
#define PMPA_STATIC_UNLESS_TESTING
extern PMPA_STATIC_UNLESS_TESTING __thread pmpa_memory_block *master_memory_block;
extern PMPA_STATIC_UNLESS_TESTING __thread pmpa_memory_int master_memory_block_size;
#else
#define PMPA_STATIC_UNLESS_TESTING static
#endif

//#define PMPA_MEMORY_BLOCK_HEADER_SIZE ( offsetof(pmpa_memory_block, data) )
#define PMPA_MEMORY_BLOCK_HEADER_SIZE ( sizeof(pmpa_memory_block))
#define PMPA_NEXTBLOCK_SIZE(BSIZE) ((BSIZE + PMPA_MEMORY_BLOCK_HEADER_SIZE) /PMPA_MEMORY_BLOCK_HEADER_SIZE +1)

#define PMPA_NEXTBLOCK_SIZE_INBYTES(BSIZE) ((BSIZE + PMPA_MEMORY_BLOCK_HEADER_SIZE) /PMPA_MEMORY_BLOCK_HEADER_SIZE +1)* PMPA_MEMORY_BLOCK_HEADER_SIZE


#define PMPA_POINTER_IS_IN_RANGE(a, b, c) ( ((a) < ((b) + (c / sizeof(pmpa_memory_block)))) && ((a) >=  (b)) )

#define PMPA_NULL_POINTER_IS_IN_RANGE(a,c) ((a) < (c))




//#define PMPA_FIRST_VALID_ADDRESS_IN_POOL master_memory_block
//#define PMPA_LAST_VALID_ADDRESS_IN_POOL (PMPA_FIRST_VALID_ADDRESS_IN_POOL + master_memory_block_size)
//#define PMPA_POINTER_IS_IN_RANGE(a, b, c) ( ((a) < ((b) + (c))) && ((a) >=  (b)) )
//#define PMPA_POINTER_IS_IN_POOL(a) PMPA_POINTER_IS_IN_RANGE(a, PMPA_FIRST_VALID_ADDRESS_IN_POOL, master_memory_block_size)


#endif /* LUNAX_CODE_INC_SHARED_PMPA_INTERNALS_H_ */
