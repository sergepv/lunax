/*
 * pmpa.c
 * Part of pmpa
 * Copyright (c) 2014 Philip Wernersbach
 *
 * Dual-Licensed under the Public Domain and the Unlicense.
 * Choose the one that you prefer.
 */

#include <semaphore.h>
#include <string.h>
#include "lrel.h"

#define PMPA_NO_OVERRIDE_C_MEMORY_FUNCTIONS
#include "shared_pmpa_internals.h"

//PMPA_STATIC_UNLESS_TESTING __thread pmpa_memory_block *master_memory_block = NULL;
PMPA_STATIC_UNLESS_TESTING __thread pmpa_memory_int master_memory_block_size = 1400000;

sem_t* GLOBAL_SEM = NULL;

void *pmpa_alloc_sf (void *ud, void *ptr, size_t osize, size_t nsize)
{
	void* res;
	sem_wait(GLOBAL_SEM);
	res = pmpa_alloc (ud, ptr, osize, nsize);
	sem_post(GLOBAL_SEM);
	return res;
}

void *pmpa_malloc_sf(void* base_addr, size_t size)
{
	void* res;
	sem_wait(GLOBAL_SEM);
	res = pmpa_malloc(base_addr, size);
	sem_post(GLOBAL_SEM);
	return res;
}

void *pmpa_calloc_sf(void* base_addr, size_t nelem, size_t elsize)
{
	void* res;
	sem_wait(GLOBAL_SEM);
	res = pmpa_calloc(base_addr, nelem, elsize);
	sem_post(GLOBAL_SEM);
	return res;
}

void *pmpa_realloc_sf(void* base_addr,void *ptr, size_t size)
{
	void* res;
	sem_wait(GLOBAL_SEM);
	res = pmpa_realloc(base_addr, ptr, size);
	sem_post(GLOBAL_SEM);
	return res;
}
void pmpa_free_sf(void *ptr)
{
	sem_wait(GLOBAL_SEM);
	pmpa_free(ptr);
	sem_post(GLOBAL_SEM);
}



int pmpa_get_offset()
{
  return PMPA_MEMORY_BLOCK_HEADER_SIZE;
}

/*
 * Internal functions.
 */

static unsigned int  PMPA_POINTER_IS_IN_POOL( pmpa_memory_block * base_addr, pmpa_memory_block * a)
{
	return  PMPA_POINTER_IS_IN_RANGE(a, base_addr, master_memory_block_size);
}

static void concat_sequential_blocks(void* base_addr, pmpa_memory_block *memory_block, bool is_allocated)
{
	pmpa_memory_block *current_memory_block = memory_block;
	pmpa_memory_block *next_memory_block = NULL;

	if (current_memory_block->allocated != is_allocated)
		return;

	while ( (next_memory_block = current_memory_block + PMPA_NEXTBLOCK_SIZE(current_memory_block->size)) &&
			PMPA_POINTER_IS_IN_POOL(base_addr, next_memory_block + 1) &&
			(next_memory_block->allocated == is_allocated) )
				current_memory_block->size += PMPA_NEXTBLOCK_SIZE_INBYTES(next_memory_block->size);


}

static pmpa_memory_block *find_first_block(void* base_addr, bool is_allocated, pmpa_memory_int min_size)
{
	pmpa_memory_block *master_memory_block = base_addr ;
	pmpa_memory_block *memory_block = master_memory_block;
	while (PMPA_POINTER_IS_IN_POOL(base_addr, memory_block + 1)) {
		/* If we're trying to find an block, then defragment the pool as we go along.
		 * This incurs a minor speed penalty, but not having to spend time
		 * iterating over a fragmented pool makes up for it. */
		if (is_allocated == false)
			concat_sequential_blocks(base_addr, memory_block, is_allocated);

		if ( (memory_block->allocated == is_allocated) && (memory_block->size >= min_size) ) {
			return memory_block;
		} else {
			memory_block += PMPA_NEXTBLOCK_SIZE(memory_block->size);
		}
	}

	return NULL;
}

static void split_block(void* base_addr, pmpa_memory_block *memory_block, pmpa_memory_int size)
{
	pmpa_memory_block *second_memory_block = memory_block + PMPA_NEXTBLOCK_SIZE(size);
	pmpa_memory_block *original_second_memory_block = memory_block + PMPA_NEXTBLOCK_SIZE(memory_block->size);
	pmpa_memory_int original_memory_block_size = memory_block->size;

	memory_block->allocated = false;

	/* We can't split this block if there's not enough room to create another one. */
	if ( PMPA_NULL_POINTER_IS_IN_RANGE((second_memory_block + 1), original_second_memory_block) &&
	   ( PMPA_POINTER_IS_IN_POOL(base_addr, second_memory_block + 1) ) ) {
		memory_block->size = size;

		second_memory_block->size = original_memory_block_size - (PMPA_NEXTBLOCK_SIZE_INBYTES(size));
		second_memory_block->allocated = false;
	}
}

/*
 * Externally accessible API functions.
 */


void *pmpa_alloc (void *ud, void *ptr, size_t osize, size_t nsize)
{
  (void)ud; (void)osize;  /* not used */
  if (nsize == 0) {
	 pmpa_free(ptr);
    return NULL;
  }
  else
  {
	  void* pt = pmpa_realloc((void*)G_LUNAX_ADDR, ptr, nsize);
    return pt;
  }
}

bool pmpa_init_lunax(void* base_addr, pmpa_memory_int size)
{
	pmpa_memory_block *master_memory_block;
	if ( (master_memory_block = base_addr) ) {
		master_memory_block->size = PMPA_NEXTBLOCK_SIZE_INBYTES(size);
		master_memory_block->allocated = false;

		master_memory_block_size = size;

		return true;
	} else {
		return false;
	}
}

bool pmpa_uninit_lunax(void)
{
	master_memory_block_size = 0;
	//free(master_memory_block);

	return true;
}

/*
 * Externally accessible C memory functions.
 */

void *pmpa_malloc(void* base_addr, size_t size)
{
	pmpa_memory_block *memory_block = find_first_block(base_addr, false, size);

	if (memory_block) {
		split_block(base_addr, memory_block, size);
		memory_block->allocated = true;

		return memory_block+1;
	} else {
		return NULL;
	}
}

void *pmpa_calloc(void* base_addr, size_t nelem, size_t elsize)
{
	pmpa_memory_int ptr_size = nelem * elsize;
	void *ptr = pmpa_malloc(base_addr,ptr_size);

	if (ptr) {
		memset(ptr, 0, ptr_size);

		return ptr;
	} else {
		return NULL;
	}
}

void *pmpa_realloc(void* base_addr, void *ptr, size_t size)
{
	pmpa_memory_block *memory_block = NULL;
	pmpa_memory_block *new_memory_block = NULL;

	pmpa_memory_int memory_block_original_size = 0;

	/* If ptr is NULL, realloc() behaves like malloc(). */
	if (!ptr)
		return pmpa_malloc(base_addr, size);

	memory_block = ptr - PMPA_MEMORY_BLOCK_HEADER_SIZE;
	memory_block_original_size = memory_block->size;

	/* Try to cheat by concatenating the current block with contiguous
	 * empty blocks after it, and seeing if the new block is big enough. */
	memory_block->allocated = false;
	concat_sequential_blocks(base_addr, memory_block, memory_block->allocated);
	memory_block->allocated = true;

	if (memory_block->size >= size) {
		/* The new block is big enough, split it and use it. */
		split_block(base_addr, memory_block, size);
		memory_block->allocated = true;

		return memory_block+1;
	} else {
		/* The new block is not big enough. */

		/* Restore the memory block's original size. */
		split_block(base_addr, memory_block, memory_block_original_size);
		memory_block->allocated = true;

		/* Find another block and try to use that. */
		if ( !(new_memory_block = find_first_block(base_addr, false, size)) )
			return NULL;

		split_block(base_addr, new_memory_block, size);
		new_memory_block->allocated = true;

		memcpy(new_memory_block+1, memory_block+1, memory_block->size);

		pmpa_free(memory_block+1);
		return new_memory_block+1;
	}

	return NULL;
}

void pmpa_free(void *ptr)
{
	pmpa_memory_block *memory_block = ptr - PMPA_MEMORY_BLOCK_HEADER_SIZE;

	if (ptr == NULL)
		return;

	memory_block->allocated = false;
}

