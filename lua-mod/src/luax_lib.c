/*
** luax_lib.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "lua.h"
#include "lauxlib.h"

#if defined(LUA_LUNAX)

int islunax (lua_State *L)
{
    L = L;
    return 0;
}

#include <stdio.h>

//In
// L  self ; error class(like CError)
//Out
//L | status
static int is_error(lua_State* L)
{
    int res = 0;
    int code;
    lua_getfield(L,-1,"code");
    code = lua_tointeger(L,-1);
    lua_pop(L,2);
    // L  self;
    while (lua_getmetatable(L,-1))
    {
        lua_getfield(L, -1, "__index");
        if (lua_type(L,-1) == LUA_TNIL)
        {
            // L  self; metatable; nill;
            lua_pop(L,2);
            break;
        }
        // L  self; metatable; error_class;
        lua_getfield(L, -1, "code");
        // L  self; metatable; error_class; code
        if (code == lua_tointeger(L, -1))
        {
            res = 1;
            lua_pop(L,3);
            break;
        }
        lua_pop(L, 1);
        lua_remove(L, -3);
        lua_remove(L, -3);
        // L  error_class;
    }
    // L  self;
    lua_pop(L,1);
    lua_pushinteger(L,res);
    return 1;
}

//In
// L|  table;
//Out
//L | value
static int cerror_tostring(lua_State* L)
{
    lua_getfield(L,-1,"msg");
    lua_remove(L,-2);
    return 1;
}

//In
// L  error class(like CError); string (opt)
//Out
//L | error
int new_error(lua_State* L)
{
    const char * msg = NULL;
    if (lua_type(L,-1) == LUA_TSTRING)
    {
        msg = lua_tostring(L,-1);
        lua_insert(L,-2);
    }
    lua_newtable(L);
    {
        if (msg)
        {
            // L   string (opt); error class(like CError); newtable
            lua_pushvalue(L,-3);
            lua_setfield(L,-2,"msg");
        }
    }
    // L  string (opt); error class(like CError) ; newtable
    lua_newtable(L);
    {
        lua_pushstring(L, "__index");
        lua_pushvalue(L,-4);
        // L string (opt);  error class(like CError) ; newtable ;newtable(meta) ;__index; error class(like CError)
        lua_settable(L, -3);
        lua_pushstring(L, "__tostring");
        // L | string (opt); error class(like CError) ; newtable ;newtable(meta) ;__tostring;
        lua_pushvalue(L,-4);
        lua_getmetatable(L,-1);
        lua_getfield(L,-1,"__tostring");
        // L | string (opt); error class(like CError) ; newtable ;newtable(meta) ;__tostring; error class(like CError); meta for error class(like CError); __tosting func;
        lua_remove(L,-2);
        lua_remove(L,-2);
        lua_settable(L, -3);
    }
    // L string (opt);  error class(like CError) ; newtable ;newtable(meta) ;
    lua_setmetatable(L,-2);
    // L  string (opt); error class(like CError) ; newtable ;
    lua_remove(L,-2);
    if (msg)
    {
        lua_remove(L,-2);
    }
    return 1;
}


//In
// L  empty
//Out
//L | empty
int init_errors(lua_State* L)
{
    lua_getglobal(L,"_G");
    // L  global_table;
    lua_newtable(L);
    {
        lua_pushinteger(L,CRETURN_CODE);
        lua_setfield(L,-2,"code");
        lua_pushstring(L,"ERROR. Incorrect error handler.");
        lua_setfield(L,-2,"msgErrHandler");
    }
    lua_setfield(L,-2,"CReturn");
    lua_pushstring(L,"CError");
    lua_newtable(L);
    {
        lua_pushinteger(L,CERROR_CODE);
        lua_setfield(L,-2,"code");

        lua_pushstring(L,"Generic error");
        lua_setfield(L,-2,"msg");

        lua_pushcfunction(L, &new_error);
        lua_setfield(L, -2, "newError");

        lua_pushcfunction(L, &is_error);
        lua_setfield(L, -2, "is");
    }
    // L  global_table; "CError"; newtable
    lua_newtable(L);
    {
        lua_pushcfunction(L,&cerror_tostring);
        lua_setfield(L,-2,"__tostring");
    }
    // L  global_table; "CError"; newtable; newtable(meta)
    lua_setmetatable(L,-2);
    // L  global_table; "CError"; newtable;
    lua_settable(L,-3);
    // L  global_table;
    lua_pop(L,1);
    return 0;
}
//In
// L | string; error class(like CError)
//Out
//L | empty
int reg_error(lua_State* L)
{
    if (lua_type(L,-2) != LUA_TSTRING)
    {
        luaL_error(L, "Error. Bad argument.\n");
    }

    lua_getglobal(L,"_G");
    // L  | string; error class(like CError); global_table;
    lua_insert(L,-3);
    // L  | global_table; string; error class(like CError);
    lua_newtable(L);
    {

    }
    // L  global_table; string; error class(like CError) ; newtable
    lua_newtable(L);
    {
        lua_pushstring(L, "__index");
        lua_pushvalue(L,-4);
        // L | global_table; string; error class(like CError) ; newtable ;newtable(meta) ;__index; error class(like CError)
        lua_settable(L, -3);
        lua_pushstring(L, "__tostring");
        // L | error class(like CError) ; newtable ;newtable(meta) ;__tostring;
        lua_pushvalue(L,-4);
        lua_getmetatable(L,-1);
        lua_getfield(L,-1,"__tostring");
        // L | global_table; string; error class(like CError) ; newtable ;newtable(meta) ;__tostring; error class(like CError); meta for error class(like CError); __tosting func;
        lua_remove(L,-2);
        lua_remove(L,-2);
        // L | global_table; string; error class(like CError) ; newtable ;newtable(meta) ;__tostring;  __tosting func;
        lua_settable(L, -3);

    }
    // L | global_table; string; error class(like CError) ; newtable ;newtable(meta) ;
    lua_setmetatable(L,-2);
    // L  global_table; string; error class(like CError) ; newtable ;
    lua_remove(L,-2);
    // L  global_table; string; newtable ;
    lua_settable(L,-3);
     // L  global_table;
    lua_pop(L,1);
    return 0;
}
//In
// L | empty
//Out
//L | empty
//Example : raise_error(L,"CLunaxError");
int raise_error (lua_State* L,char* errclass)
{
    lua_getglobal(L,"_G");
    lua_getfield(L,-1, errclass);
    // L| _G; class
    lua_remove(L,-2);
    // L| class
    new_error(L);
    // L | error
    luaL_where(L, 1);
    // L | error; string(file/line)
    lua_pushstring(L," ERROR: ");
    lua_getfield(L,-3,"msg");
    // L | error; string(file/line); ERROR ; msg;
    lua_concat(L, 3);
    // L | error; string;
    if (try_get(L))
    {
        lua_setfield(L,-2,"msg");
        // L | error;
    }
    else
    {
        lua_remove(L,-2);
        // L | string;
    }
    return lua_error(L);
}

//In
// L | error class(like CError)(opt); string
//Out
//L | empty
int user_error (lua_State* L)
{
    const char* msg;
    if(lua_gettop(L) == 2)
    {
        msg = lua_tostring(L,-1);
        lua_pushvalue(L,-2);
        // L | error class(like CError); string; error class(like CError);
        lua_remove(L,-3);
    }
    else
    {
        msg = lua_tostring(L,-1);
        //L| string;
        lua_getglobal(L,"_G");
        lua_getfield(L,-1, "CUserError");
        if (lua_type(L,-1) != LUA_TTABLE)
        {
            lua_pop(L,3);
            luaL_error(L,"ERROR: CUserError class doesn't exist.");
            return 0;
        }
        //L| string; _G; error class(like CError);
        lua_remove(L,-2);
    }

    new_error(L);
    // L |  string; error class(like CError);error
    luaL_where(L, 1);
    // L | string; error class(like CError); error; string(file/line)
    lua_pushstring(L," ERROR: ");
    lua_pushstring(L, msg);
    // L |string; error class(like CError); error; string(file/line); ERROR ; msg;
    lua_concat(L, 3);
    // L | string; error class(like CError); error; string
    lua_setfield(L,-2,"msg");
    lua_remove(L,-2);
    lua_remove(L,-2);
    // L | error;
    return lua_error(L);
}
#endif
