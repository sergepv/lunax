/*
* proxy.c
* token filter for ltokenp
* Luiz Henrique de Figueiredo <lhf@tecgraf.puc-rio.br>
* 30 Jul 2018 19:34:04
* This code is hereby placed in the public domain and also under the MIT license
*/

#include <stdio.h>

#if LUA_VERSION_NUM < 503
#define TK_INT  (-1)
#define TK_FLT  TK_NUMBER
#endif

#if 1
#include "lauxlib.h"

static int finishpcall(lua_State *L, int status, lua_KContext extra)
{
    int res;
    int catch_count;
    int catch_cnt = 0;
    extra = extra;
    if (status != LUA_OK && status != LUA_YIELD)
    { /* error? */
        // L | error_class; catch function; error_class; catch function; ....;
        lua_pushboolean(L, 0); /* first result (false) */
        lua_pushvalue(L, -2); /* error message */
        //L| error_class; catch function; error_class; catch function; ....; bool(1);error_message(obj);error_status; error_message(obj);
        if (lua_type(L,-1) != LUA_TTABLE)
        {
            lua_getglobal(L,"_G");
            lua_getfield(L,-1, "CLuaError");
            lua_remove(L,-2);

            new_error(L);
            // L | error_message(string0); error
            lua_insert(L,-2);
            // L | error; message(string0);
            lua_setfield(L,-2,"msg");
        }
        catch_count =  (lua_gettop(L)-4)/2;
        for (catch_cnt = 0; catch_cnt < catch_count; catch_cnt++)
        {
            lua_getfield(L,-1,"is");
            lua_pushvalue(L,-2);
            lua_pushvalue(L, (catch_cnt*2)+1);
            //L .....;is(); error_message(obj); error_class;
            if (lua_type(L, -3) != LUA_TFUNCTION) { lua_pop(L, lua_gettop(L)); luaL_error (L,"ERROR. Incorrect error object.");return 0;}
            lua_call(L, 2, 1);
            if (lua_tointeger(L,-1))
            {
                //L .....;1
                lua_pop(L,1);
                lua_pushvalue(L, (catch_cnt*2)+2);
                //L .....; error_message(obj); catch function;
                lua_insert(L,1);
                lua_insert(L,2);
                //L catch function; error_message(obj);.....;
                lua_pop(L, lua_gettop(L)-2);
                lua_call(L, 1, 1);
                // L | return_value;
                return 1;
            }
            else
            {
                //L .....; 0
                lua_pop(L,1);
            }
        }
        lua_pop(L, lua_gettop(L));
        luaL_error (L,"ERROR. Incorrect error handler.");
        return 0;
    }
    else
    {
        // L | error_class; catch function; error_class; catch function; ....;success_status;
        res = lua_tointeger(L,-1);
        lua_pop(L, lua_gettop(L));
        lua_pushinteger(L,res);
        return 1; //lua_gettop(L) - (int)extra;  /* return all results */
    }
}


//In
// L  main function; error_class; catch function; error_class; catch function; ....
//Out
//L | return _status
int filter_try(lua_State* L)
{
  int status;
  lua_pushvalue(L,1);
  // L | main function; error_class; catch function; error_class; catch function; ....;main function;
  lua_remove(L, 1);
  // L | c error_class; catch function; error_class; catch function; ....;main function;

  lua_pushboolean(L, 1);  /* first result if no errors */
  // L |  error_class; catch function; error_class; catch function; ....; main function; bool
  lua_insert(L, -2);  /* put it in place */
  // L | error_class; catch function; error_class; catch function; ....; bool; main function;
  status = lua_pcallk(L, 0, 1, 0, 0, finishpcall);
  return finishpcall(L, status, 0);
}

static char TRY_MODE_CNT;

//In
// L  empty
//Out
//L | empty
int __try_start(lua_State* L)
{
    int cnt;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRY_MODE_CNT);
    //L | int or null
    if(lua_type(L,-1) == LUA_TNIL)
    {
        lua_pop(L,1);
        cnt =1;
    }
    else
    {
        cnt = lua_tointeger(L,-1);
        lua_pop(L,1);
        cnt++;

    }
    lua_pushinteger(L,cnt);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRY_MODE_CNT);
    return 0;
}

//In
// L  empty
//Out
//L | empty
int __try_stop(lua_State* L)
{
    int cnt;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRY_MODE_CNT);
    //L | int or null
    if(lua_type(L,-1) == LUA_TNIL)
    {
        lua_pop(L,1);
        luaL_error (L,"ERROR. Incorrect try/catch operator.");
    }
    else
    {
        cnt = lua_tointeger(L,-1);
        if (cnt ==0)
        {
            luaL_error (L,"ERROR. Incorrect try/catch operator.");
        }
        else
        {
            lua_pop(L, 1);
            cnt--;
            lua_pushinteger(L, cnt);
            lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRY_MODE_CNT);
        }
    }
    return 0;
}
//In
// L  empty
//Out
//L | empty
int try_get(lua_State* L)
{
    int cnt = 0;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRY_MODE_CNT);
    if(lua_type(L,-1) != LUA_TNIL)
    {
        cnt = lua_tointeger(L,-1);
    }
    lua_pop(L,1);
    return cnt? 1:0;
}

struct filter_param
{
    char* value;
    int t;
    struct filter_param* next;
};
struct filter_leaf
{
    SemInfo *seminfo;
    int t;
    struct filter_leaf* next;
};
struct filter_node
{
    struct filter_leaf* leafs;
    struct filter_node* next_node;
};

struct flt_pattern
{
    int t;
    char* sem_srt;
    int  sem_int;
};

struct filter_node* FILTER_IN;
struct filter_node* FILTER_OUT;
struct filter_param* FILTER_PARAM;

//============================================================================================================================ #try
struct flt_pattern FILTER_TRY_PATTERN[] ={{'#',NULL,-1}, {TK_NAME,"try",-1}, {-1, NULL,-1}};
/* INSERT
do
  __try_start()
  local __status, __exp, __p1,__p2,__p3,__p4,__p5 = pcall(function()
*/
struct flt_pattern FILTER_TRY_PATTERN_N[] ={{TK_DO,NULL,-1}, {TK_NAME,"__try_start",-1}, {'(',NULL,-1}, {')',NULL,-1},
        {TK_LOCAL,NULL,-1}, {TK_NAME,"__status",-1}, {',',NULL,-1},{TK_NAME,"__exp",-1}, {',',NULL,-1},
        {TK_NAME,"__p1",-1}, {',',NULL,-1} ,{TK_NAME,"__p2",-1}, {',',NULL,-1},
        {TK_NAME,"__p3",-1}, {',',NULL,-1} ,{TK_NAME,"__p4",-1}, {',',NULL,-1},{TK_NAME,"__p5",-1},
        {'=',NULL,-1}, {TK_NAME,"pcall",-1}, {'(', NULL, -1},
        {TK_FUNCTION,NULL,-1},{'(', NULL, -1},{')', NULL, -1},
        {-1, NULL,-1}};

//============================================================================================================================ #catch
struct flt_pattern FILTER_CATCH_PATTERN[] ={{'#',NULL,-1}, {TK_NAME,"catch",-1},{'(', NULL, -1},
        {TK_NAME, NULL, 10},{':', NULL, -1},{TK_NAME, NULL, 11},
        {')', NULL, -1},{-1, NULL,-1}};
/* INSERT
return CReturn
end )
__try_stop()
if not __status and type(__exp) ~= "table" then __exp = newError(CLuaError,__exp)  end
if ((not __status) and (__exp:is(CLunaxError) == 1)) then local err = __exp
*/
struct flt_pattern FILTER_CATCH_PATTERN_N[] ={{TK_RETURN,NULL,-1}, {TK_NAME,"CReturn",-1},
        {TK_END,NULL,-1}, {')', NULL, -1}, {TK_NAME,"__try_stop",-1}, {'(',NULL,-1}, {')',NULL,-1},
        //{TK_NAME,"print",-1}, {'(',NULL,-1},{TK_STRING, "0000000000", -1}, {')',NULL,-1},

        {TK_IF, NULL, -1},{TK_NOT, NULL, -1}, {TK_NAME, "__status", -1}, {TK_AND, NULL, -1},
        {TK_NAME, "type", -1}, {'(',NULL,-1}, {TK_NAME, "__exp", -1}, {')',NULL,-1}, {TK_NE, NULL, -1}, {TK_STRING, "table", -1},
        {TK_THEN, NULL, -1},
        {TK_NAME, "__exp", -1}, {'=',NULL,-1}, {TK_NAME, "newError", -1}, {'(', NULL, -1},
        {TK_NAME, "CLuaError", -1},{',', NULL, -1}, {TK_NAME, "__exp", -1}, {')', NULL, -1}, {TK_END,NULL,-1},

        {TK_IF, NULL, -1},{'(', NULL, -1}, {'(', NULL, -1}, {TK_NOT, NULL, -1}, {TK_NAME, "__status", -1}, {')', NULL, -1},
        {TK_AND, NULL, -1}, {'(', NULL, -1}, {TK_NAME, "__exp", -1}, {':', NULL, -1}, {TK_NAME, "is", -1}, {'(', NULL, -1},{TK_NAME, NULL, 11},{')', NULL, -1},
        {TK_EQ,NULL,-1}, {TK_INT,NULL,1},{')', NULL, -1},{')', NULL, -1},{TK_THEN, NULL, -1},
        {TK_LOCAL,NULL,-1}, {TK_NAME,NULL,10},{'=',NULL,-1}, {TK_NAME, "__exp", -1},
        {-1, NULL,-1}};

//============================================================================================================================ #catch_next
struct flt_pattern FILTER_CATCH_NEXT_PATTERN[] ={{'#',NULL,-1}, {TK_NAME,"catch_next",-1},{'(', NULL, -1},
        {TK_NAME, NULL, 10},{':', NULL, -1},{TK_NAME, NULL, 11},
        {')', NULL, -1},{-1, NULL,-1}};
/* INSERT
elseif ((not __status) and (__exp:is(CLunaxError) == 1)) then local err = __exp
*/
struct flt_pattern FILTER_CATCH_NEXT_PATTERN_N[] ={
        {TK_ELSEIF, NULL, -1},{'(', NULL, -1}, {'(', NULL, -1}, {TK_NOT, NULL, -1}, {TK_NAME, "__status", -1}, {')', NULL, -1},
        {TK_AND, NULL, -1}, {'(', NULL, -1}, {TK_NAME, "__exp", -1}, {':', NULL, -1}, {TK_NAME, "is", -1}, {'(', NULL, -1},{TK_NAME, NULL, 11},{')', NULL, -1},
        {TK_EQ,NULL,-1}, {TK_INT,NULL,1},{')', NULL, -1},{')', NULL, -1},{TK_THEN, NULL, -1},
        {TK_LOCAL,NULL,-1}, {TK_NAME,NULL,10},{'=',NULL,-1}, {TK_NAME, "__exp", -1},
        {-1, NULL,-1}};

//============================================================================================================================ #end
struct flt_pattern FILTER_TRYCATCH_END_PATTERN[] ={{'#',NULL,-1}, {TK_END,NULL,-1}, {-1, NULL,-1}};

/* INSERT
elseif not __status then error(__exp.msg)
elseif type(__exp) == "table" and __exp.code == 99 then
else return __exp, __p1,__p2,__p3,__p4,__p5 end;
end
*/
struct flt_pattern FILTER_TRYCATCH_END_PATTERN_N[] ={
        {TK_ELSEIF,NULL,-1},{TK_NOT, NULL, -1}, {TK_NAME, "__status", -1}, {TK_THEN,NULL,-1},
        {TK_NAME, "error", -1}, {'(', NULL, -1}, {TK_NAME,"__exp",-1}, {'.',NULL,-1}, {TK_NAME,"msg",-1}, {')', NULL, -1},
        {TK_ELSEIF,NULL,-1}, {TK_NAME, "type", -1}, {'(',NULL,-1}, {TK_NAME, "__exp", -1}, {')',NULL,-1}, {TK_EQ, NULL, -1}, {TK_STRING, "table", -1},
        {TK_AND, NULL, -1}, {TK_NAME,"__exp", -1}, {'.',NULL,-1}, {TK_NAME,"code", -1}, {TK_EQ,NULL,-1},{ TK_INT,NULL,99},{TK_THEN,NULL,-1},
        {TK_ELSE,NULL,-1}, {TK_RETURN,NULL,-1},
        {TK_NAME,"__exp",-1}, {',',NULL,-1},
        {TK_NAME,"__p1",-1}, {',',NULL,-1} ,{TK_NAME,"__p2",-1}, {',',NULL,-1},
        {TK_NAME,"__p3",-1}, {',',NULL,-1} ,{TK_NAME,"__p4",-1}, {',',NULL,-1},{TK_NAME,"__p5",-1},
        {TK_END,NULL,-1},{TK_END,NULL,-1},{-1, NULL,-1}};


static void filter_param_add( int t, char* val)
{
    struct filter_param* param;
    if (FILTER_PARAM == NULL)
    {
        FILTER_PARAM =  malloc(sizeof(struct filter_param));
        FILTER_PARAM->next = NULL;
        param = FILTER_PARAM;
    }
    else
    {
        param = FILTER_PARAM;
        while( param->next )
        {
            param = param->next;
        }
        param->next = malloc(sizeof(struct filter_param));
        param = param->next;
        param->next = NULL;
    }
    param->t = t;
    param->value = strdup(val);
}
static char* filter_param_lookup( int t)
{
    struct filter_param* param;
    param = FILTER_PARAM;
    while(param)
    {
        if (param->t ==t) break;
        param = param->next;
    }
    if (param == NULL) return NULL;
    return param->value;
}

static void filter_param_freeall()
{
    struct filter_param* param;
    struct filter_param* param_pre;
    param = FILTER_PARAM;
    param_pre = NULL;
    while(param)
    {
        param_pre = param;
        param = param->next;
        free(param_pre->value);
        free(param_pre);
    }
}


static void filter_leaf_push( struct filter_node* node, int t, SemInfo *seminfo)
{
    struct filter_leaf* last_leaf;
    struct filter_leaf* leaf = malloc(sizeof(struct filter_leaf));

    leaf->next = NULL;
    leaf->t = t;

    leaf->seminfo= malloc(sizeof(SemInfo));
    memcpy (leaf->seminfo,seminfo, sizeof(SemInfo));
    last_leaf = node->leafs;
    if (last_leaf)
    {
        while (last_leaf->next)
        {
            last_leaf = last_leaf->next;
        }
        last_leaf->next = leaf;
    }
    else
    {
        node->leafs = leaf;
    }
}
static struct filter_leaf* filter_leaf_pull( struct filter_node* node)
{
    if (node->leafs == NULL) return NULL;
    struct filter_leaf* leaf = node->leafs;
    node->leafs = leaf->next;
    return leaf;
}
static void filter_leaf_free( struct filter_leaf* leaf)
{
    free(leaf->seminfo);
    free(leaf);
}

static struct filter_node* filter_node_new()
{
    struct filter_node*  node = malloc(sizeof(struct filter_node));
    node->leafs = NULL;
    node->next_node = NULL;
    return node;
}
static void filter_node_add()
{
    struct filter_node* to_node = FILTER_OUT;
    struct filter_node* from_node  = FILTER_IN;
    if (to_node == NULL)
    {
        FILTER_OUT = from_node;
        return;
    }
    while(to_node->next_node)
    {
        to_node = to_node->next_node;
    }
    to_node->next_node = from_node;
}

static void filter_node_free(struct filter_node* node)
{
    struct filter_leaf* leaf;
    while( node)
    {
        while (node->leafs)
        {
            leaf = node->leafs;
            node->leafs = node->leafs->next;
            filter_leaf_free(leaf);
        }
        node = node->next_node;
    }
}


static int llex_token(LexState *ls, SemInfo *seminfo)
{
    int t;
    struct filter_node* node;
    struct filter_node* pre_node;
    struct filter_leaf* leaf;
    while (1)
    {
        node = FILTER_OUT;
        pre_node = NULL;
        if (node)
        {
            while (node->next_node)
            {
                pre_node = node;
                node = node->next_node;
            }
            leaf = filter_leaf_pull(node);
            if (leaf)
            {
                memcpy(seminfo, leaf->seminfo, sizeof(SemInfo));
                t = leaf->t;
                filter_leaf_free(leaf);
                return t;
            }
            else
            {
                if (pre_node)
                {
                    pre_node->next_node = NULL;
                }
                else
                {
                    FILTER_OUT = NULL;
                }
                free(node);
            }
        }
        else
        {
            return llex(ls, seminfo);
        }
    }
}
static int check_pattern_node(int t, SemInfo *seminfo, struct flt_pattern* flt)
{
    bool match = false;
    if (t == flt->t)
    {
        if (flt->sem_srt)
        {
            if (strcmp(getstr(seminfo->ts), flt->sem_srt) == 0)
            {
                //printf("===> match2\n");
                match = true;
            }
            else
            {
                match = false;
            }
        }
        else
        {
            if (flt->sem_int != -1)
            {
                filter_param_add(flt->sem_int, getstr(seminfo->ts));
                //printf("===> match3\n");
                match = true;
            }
            else
            {
                //printf("===> match1\n");
                match = true;
            }
        }
    }
    else
    {
        match = false;
    }
    return match;
}

static void fill_node(LexState *ls, struct filter_node* node, struct flt_pattern* flt)
{
    int i=0;
    SemInfo seminfo;
    while (flt[i].t != -1)
    {
        if (flt[i].sem_srt)
        {
            seminfo.ts = luaX_newstring(ls,flt[i].sem_srt,strlen(flt[i].sem_srt));
        }
        else
        {
            if (flt[i].sem_int != -1)
            {
                if (flt[i].t == TK_NAME)
                {
                    char* str = filter_param_lookup(flt[i].sem_int);
                    seminfo.ts = luaX_newstring(ls,str,strlen(str));
                }
                else
                {
                    if (flt[i].t == TK_INT)
                    {
                        lua_Integer i1;
                        TValue obj;
                        i1 = l_castU2S(flt[i].sem_int);
                        setivalue(&obj, i1);
                        seminfo.i = ivalue(&obj);
                    }
                }
            }
        }
        filter_leaf_push(node,flt[i].t,&seminfo);
        i++;
    }
}
static int match_pattern (LexState *ls,int t,SemInfo *seminfo, struct flt_pattern* check_patt, struct flt_pattern* cng_patt)
{
    bool match = false;
    int i;
    int ti;
    SemInfo si;
    FILTER_PARAM = NULL;
    memcpy(&si,seminfo,sizeof(SemInfo));
    ti = t;
    FILTER_IN = filter_node_new();
    i=0;
    while (check_patt[i].t != -1)
    {
        match = check_pattern_node(ti,&si,&check_patt[i]);
        if (!match) break;
        filter_leaf_push(FILTER_IN,ti,&si);
        ti = llex_token(ls,&si);
        i++;
    }
    if (match)
    {
        filter_node_free(FILTER_IN);
        FILTER_IN = filter_node_new();
        fill_node(ls, FILTER_IN, cng_patt);
    }
    filter_leaf_push(FILTER_IN,ti,&si);
    filter_param_freeall();
    return 0;
}

static int filter_inner(LexState *ls, SemInfo *seminfo)
{
    int t;
    t = llex_token(ls,seminfo);
    if (t<FIRST_RESERVED)
    {
        if (t == '#')
        {
            match_pattern(ls,t,seminfo,FILTER_TRY_PATTERN,FILTER_TRY_PATTERN_N);
            filter_node_add();
            t = llex_token(ls,seminfo);
            match_pattern(ls,t,seminfo,FILTER_CATCH_PATTERN,FILTER_CATCH_PATTERN_N);
            filter_node_add();
            t = llex_token(ls,seminfo);
            match_pattern(ls,t,seminfo,FILTER_CATCH_NEXT_PATTERN,FILTER_CATCH_NEXT_PATTERN_N);
            filter_node_add();
            t = llex_token(ls,seminfo);
            match_pattern(ls,t,seminfo,FILTER_TRYCATCH_END_PATTERN,FILTER_TRYCATCH_END_PATTERN_N);
            filter_node_add();
            t = llex_token(ls,seminfo);

        }
    }
    if (t == TK_STRING)
    {
    }
    if (t == TK_NAME)
    {
        if (strcmp(getstr(seminfo->ts), "__FILE__") == 0)
        {
            t = TK_STRING;
            seminfo->ts = ls->source;
        }
        else if (strcmp(getstr(seminfo->ts), "__LINE__") == 0)
        {
            t = TK_INT;
            seminfo->i = ls->linenumber;
        }
    }
    return t;
}
#else
//function FILTER(line,token,text,value)
//local t=text
//if t=="<file>" or t=="<eof>" then return end
//if t=="<string>" then value=string.format("%q",value) end
//if t=="@" then
//    emit("self.")
//else
//    emit(value)
//end
//end


//In
// L  ....
//Out
//L | empty
/*
Scripts should define a global function FILTER to process the token stream.
    function FILTER(line,token,text,value) ... end
The arguments are:
- the line number where the token appears
- the token as a number
- the token as text
- the value of names, numbers, and strings; for other tokens,
  the value is the same as the text.
*/
int filter_try(lua_State* L)
{
    L = L;
    return 0;
}
static int lua_filter(lua_State* L)
{
    if (lua_type(L,-1) == LUA_TSTRING)
    {
       printf("%d %d %s %s  \n", lua_tointeger(L,-4),lua_tointeger(L,-3),lua_tostring(L,-2),lua_tostring(L,-1));
    }
    else
    {
        printf("%d %d %s %s  \n", lua_tointeger(L,-4),lua_tointeger(L,-3),lua_tostring(L,-2), "some type");
    }
    lua_pop(L,4);
    return 0;
}

static int filter_inner(LexState *X, SemInfo *seminfo)
{
 lua_State *L=X->L;
 lua_getglobal(L,"FILTER");
 // denied filter for luaxprj.lua
 if (lua_type(L,-1) == LUA_TNIL)
 {
     lua_pop(L,1);
     return -1;
 }
 printf("===========> \n");
 lua_pushinteger(L,0);
 lua_pushinteger(L,-1);
 lua_pushstring(L,"<file>");
 lua_pushstring(L,getstr(X->source));
 lua_filter(L);
 //lua_call(L,4,0);
 for (;;)
 {
  int t=llex(X,seminfo);
  //lua_getglobal(L,FILTER);
  lua_pushinteger(L,X->linenumber);
  lua_pushinteger(L,t);
  if (t<FIRST_RESERVED)
  {
   char s[2]= {t,0};
   lua_pushstring(L,s);
  }
  else
   lua_pushstring(L,luaX_tokens[t-FIRST_RESERVED]);
  switch (t)
  {
    case TK_STRING:
    case TK_NAME:
     lua_pushstring(L,getstr(seminfo->ts));
     break;
    case TK_INT:
    case TK_FLT:
     lua_pushstring(L,X->buff->buffer);
     break;
    default:
     lua_pushvalue(L,-1);
     break;
  }
  //lua_call(L,4,0);
  lua_filter(L);
  if (t==TK_EOS) return t;
 }
}
#endif

static int nexttoken(LexState *X, SemInfo *seminfo)
{
    int res = filter_inner(X,seminfo);
    if (res == -1)
    {
        return llex(X,seminfo);
    }
    return res;
}

#define llex nexttoken
