# Lunax 

**Release:** version **0.1.0**

**Lunax** is a Lua/C framework for creating multitask applications.  
In Lunux context the multitasks application means like application which build as set of parallel(and/or pseudo - parallel) **tasks**.  
These **tasks** cooperate between themselves using special **Lunax IPC** technology based on shared memory paradigm.  
The **Lunux IPC** does possible an exchange of data between tasks more fastly due to not using serialization and additional caching of data.

## Documentation
[Lunax Wiki](https://gitlab.com/sergepv/lunax/wikis/home)  
[Lunax presentation](https://gitlab.com/sergepv/lunax/tree/master/doc/ppt/Lunax.pptx)

## How does it work?
**Lunax** is written using these main techniques:

    Lua, a high-level programming language that is easy to learn.
    POSIX API  for creating shared memory region and maped it in processes.
    Luaunit - lua library for testing. Used for creation of tests for project.

In system is created special shared memory region, any **task** can create table(**sharedtable**) inside this region. Other **tasks** can attach to the **sharedtable** and read information from fields of it.  
For any **sharedtable** could be created an **event** which will fire for updating this table. Inside **task**, these **events** could be caught and it gives ability to organize event-oriented architecture in application.  

For simple example to create two tasks for exchange of information.
```lua
 -- Task 1
 lunax=require("lunax");                                 -- attach lunax.so library 
 lunax.init();                                           -- check existing of shared memory region and creating it if it is needed
 lunax.setAlias("task1");                                -- bind name to set of sharedtables related this  task 
 ln = lunax.createT("luna1");                            -- create luan1 sharedtable inside shared memory region
 lunax.copyV(lunax.attr.update(),ln[0], "Hello world!"); -- copy in luna1 sharedtable value in field [0]
 lunax.fetch();                                          -- start event observer                    
```

```lua
 -- Task 2
 lunax=require("lunax");                                 -- attach lunax.so library 
 lunax.init();                                           -- check existing of shared memory region and creating it if it is needed
 luna1 = lunax.attach("task1","luna1");                  -- attach to luna1 sharedtable for task1 set
 print(luna1[0])                                         -- print "Hello world!"
 lunax.fetch();                                          -- start event observer                    
```
For testing these tasks is necessary to make following steps:
1. Build **Lunax** framework (see below)
2. Move to **build** directory with **luax** and **lunax.so** files
3. Create task1.lua(with Task1) and task2.lua(with Task2)
4. Run: **./luax task1.lua &**
5. Run: **./luax task2.lua &**
6. Output console : "Hello world!"
7. For finshed both tasks run: **pkill -2 luax**

## How do I get started?
**Lunax** framework compiles for any modern Linux i386/x86-64 distributions by GCC 8.2.0 and more.  
Setting up a Lunax development environment:  
* git clone https://gitlab.com/sergepv/lunax.git
* cd **lunax**
* make clean
* make all
* After building **Lunax** modules will be stored in **lunax/build** directory:

**Lunax** framework compiles into folowing modules:
 1. **Luax**  - special dialect **Lua** which is full compatible with **Lua v.5.3.5**
 2. **lunax.so** - **Lunax** framework library.

For run tests of **Lunax** framework:
* cd **lunax**
* make test_lua
* make tests


