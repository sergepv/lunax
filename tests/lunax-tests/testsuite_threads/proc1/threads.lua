function thread1()
  function thread1_task()
    l1.setAlias("thr1");
    table1 = l1.createT("table1");
    l1.copyV(l1.attr.merge(), table1.rec, {1,"Thread",true});
    l1.event.isHandlers(l1.attr.wait(),table1.rec,1);
    l1.copyV(l1.attr.update(), table1.rec[2], "Thread1");
  end
  l1 = require("lunax");
  l1.init();
  l1.addTask(thread1_task);
  l1.tfetch();
end;

function thread2()
  function thread2_task()
    l2.setAlias("thr2");
    table1 = l2.createT("table1");
    l2.copyV(l2.attr.merge(), table1.rec, {1,"Thread",true});
    l2.event.isHandlers(l2.attr.wait(),table1.rec,1);
    l2.copyV(l2.attr.update(), table1.rec[2], "Thread2");
  end
  l2 = require("lunax");
  l2.init();
  l2.addTask(thread2_task);
  l2.tfetch();
end;

function event1(x) 
  TEST_event1 = x[2];
end

function event2(x) 
  TEST_event2 = x[2];
end

function main()
  thr1 = lunax.app.addThread(thread1);
  thr2 = lunax.app.addThread(thread2);
  t1 = lunax.attach(lunax.attr.wait(),"thr1","table1");
  t2 = lunax.attach(lunax.attr.wait(),"thr2","table1");
  lunax.event.setHandler(lunax.attr.wait(),t1.rec,event1);
  lunax.event.setHandler(lunax.attr.wait(),t2.rec,event2);
  lunax.wait(1000);
  lunax.event.unsetHandler(t1.rec); 
  lunax.event.unsetHandler(t2.rec); 
  lunax.app.cancelThread(thr1);
  lunax.app.cancelThread(thr2);
end;

lu = require('luaunit');
testsuite_Threads = {};

lunax = require("lunax");
lunax.init();

function testsuite_Threads:test0SpawnThreads()
  TEST_event1 = 0;
  TEST_event2 = 0;
  lunax.addTask(main);
  lunax.tfetch();
  lu.assertEquals(TEST_event1,"Thread1");
  lu.assertEquals(TEST_event2,"Thread2");
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");

