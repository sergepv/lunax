-- main function for thread1
function thr2()

  function thread2_task()
    l2.setAlias("thr2");
    table1 = l2.createT("table1");
    l2.copyV(l2.attr.merge(), table1.rec, {1,"Thread",true});
    l2.event.isHandlers(l2.attr.wait(),table1.rec,1);
    l2.copyV(l2.attr.update(), table1.rec[2], "Thread2");
  end
  
  
  l2 = require("lunax");
  l2.init();
  l2.addTask(thread2_task);
  l2.tfetch();
end;

thread2 = {}
thread2.thread2 = thr2;
return thread2
