-- main function for thread1
function thr1()

  function thread1_task()
    l1.setAlias("thr1");
    table1 = l1.createT("table1");
    l1.copyV(l1.attr.merge(), table1.rec, {1,"Thread",true});
    l1.event.isHandlers(l1.attr.wait(),table1.rec,1);
    l1.wait(2000);
    l1.copyV(l1.attr.update(), table1.rec[2], "Thread1");
  end
   
  l1 = require("lunax");
  l1.init();
  l1.addTask(thread1_task);
  l1.tfetch();
end;

thread1 = {}
thread1.thread1 = thr1;
return thread1
