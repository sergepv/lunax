-- handler for ["thr1","table1"].rec
function event1(x) 
  TEST_event1 = x[2];
end

-- handler for ["thr2","table1"].rec
function event2(x) 
  TEST_event2 = x[2];
end

-- handler for ["thr3","table1"].rec
function event3(x) 
  TEST_event3 = x[2];
end
function mainTask()
  thr1 = lunax.app.addThread(thr1.thread1);
  thr2 = lunax.app.addThread(thr2.thread2);
  t1 = lunax.attach(lunax.attr.wait(),"thr1","table1");
  t2 = lunax.attach(lunax.attr.wait(),"thr2","table1");
  t3 = lunax.attach(lunax.attr.wait(),"thr3","table1");
  lunax.event.setHandler(lunax.attr.wait(),t1.rec,event1);
  lunax.event.setHandler(lunax.attr.wait(),t2.rec,event2);
  lunax.event.setHandler(lunax.attr.wait(),t3.rec,event3);
  lunax.wait(3000);
  lunax.event.unsetHandler(t1.rec); 
  lunax.event.unsetHandler(t2.rec); 
  lunax.event.unsetHandler(t3.rec); 
  lunax.app.cancelThread(thr1);
  lunax.app.cancelThread(thr2);
end;

--[[ 
    ==================================================================
                               main()
    ==================================================================
--]]

lu = require('luaunit');
testsuite_Tasks = {};

lunax = require("lunax");
lunax.init();

thr1 = require("thrs.thread1");
thr2 = require("thrs.thread2");

function testsuite_Tasks:test0_SpawnThrsAndProc()
  print("  Check a work together threads and process (about 3 sec).") 
  TEST_event1 = 0;
  TEST_event2 = 0;
  TEST_event3 = 0;
  lunax.addTask(mainTask);
  lunax.tfetch();
  lu.assertEquals(TEST_event1,"Thread1");
  lu.assertEquals(TEST_event2,"Thread2");
  lu.assertEquals(TEST_event3,"Process1");
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml")
