-- process 1

function process1_task()
  p1.setAlias("thr3");
  table1 = p1.createT("table1");
  p1.copyV(p1.attr.merge(), table1.rec, {1,"Process",true});
  p1.event.isHandlers(p1.attr.wait(),table1.rec,1);
  t1 = p1.attach(p1.attr.wait(),"thr1","table1");
  while (t1.rec[2] ~= "Thread1") do
    p1.wait(100);
  end  
  p1.copyV(p1.attr.update(), table1.rec[2], "Process1");
end
  
p1 = require("lunax");
p1.init();
p1.addTask(process1_task);
p1.tfetch();

