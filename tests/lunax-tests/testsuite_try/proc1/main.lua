 testsuite_try = {};

 lu = require('luaunit');

 lunax=require("lunax");
 lunax.init()
 
function testsuite_try:test0CommonTryCatch()
  #try
      userError("Test0")
  #catch(err: CUserError)
      lu.assertStrContains(tostring(err),"ERROR: Test0")
  #end
end

function testsuite_try:test1NestedTryCatch()
  TEST1 = 0
  #try  
      #try
          userError("Test1")
      #catch(err: CUserError)
          lu.assertStrContains(tostring(err),"ERROR: Test1")
      #end
  #catch (err: CError)
      TEST1 = 1
  #end
  lu.assertEquals(TEST1, 0);
end

function testsuite_try:test2ReturnFromCatch()
  function test2_inner()  
    #try
        userError("Test2")
        do return 34 end
    #catch(err: CUserError)
        lu.assertStrContains(tostring(err),"ERROR: Test2")
        return 42
    #end
    return 14
  end
  TEST2 = 0;
  TEST2 = test2_inner();
  lu.assertEquals(TEST2, 42);
end

function testsuite_try:test3ReturnFromTry()
  function test3_inner()  
    #try
        do return 34 end
    #catch(err: CUserError)
        lu.assertStrContains(tostring(err),"ERROR: Test3")
        return 42
    #end
    return 14
  end
  function test3_inner1()  
    #try
    #catch(err: CUserError)
        lu.assertStrContains(tostring(err),"ERROR: Test3")
        return 42
    #end
    return 14
  end
  TEST3 = 0;
  TEST3 = test3_inner();
  lu.assertEquals(TEST3, 34);
  TEST3 = 0;
  TEST3 = test3_inner1();
  lu.assertEquals(TEST3, 14);
end

function testsuite_try:test4ReturnTableFromTry()
  function test4_inner()  
    #try
        do return {34} end
    #catch(err: CUserError)
        lu.assertStrContains(tostring(err),"ERROR: Test4")
        return 42
    #end
    return 14
  end
  TEST4 = 0;
  TEST4 = test4_inner();
  lu.assertEquals(TEST4[1], 34);
end

function task5()
  #try  
      #try
          lunax.wait(1000);
          userError(CLunaxError, "Test5")
     #catch(err: CLunaxError)
          lu.assertStrContains(tostring(err),"ERROR: Test5")
          TEST5 = 10
    #end
  #catch (err: CError)
      print(tostring(err))
      TEST5 = 1
  #end  
  end

function testsuite_try:test5TryCatchinTask()
  TEST5 = 0
  lunax.addTask(task5)
  lunax.tfetch()
  lu.assertEquals(TEST5, 10);
end

function testsuite_try:test6CascadeTry()
  TEST6 = 0
    #try  
      #try
          lunax.wait(1000);
          userError(CLunaxError, "Test6")
     #catch(err: CLunaxError)
          TEST6 = 11
    #end
  #catch (err: CError)
    lu.assertStrContains(tostring(err),"attempt to yield from outside a coroutine")
      TEST6 = 1
  #end
  lu.assertEquals(TEST6, 1);
end

function testsuite_try:test7CaseTry()
  #try
    TEST7 = 0
    userError(CLunaxError, "Test6")
    TEST7 = 10 
  #catch (err:CUserError)
    TEST7 = 1
  #catch_next (err:CLunaxError)
    TEST7 = 2
  #catch_next (err:CError)
    TEST7 = 3
  #end
  lu.assertEquals(TEST7, 2);
  end

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");