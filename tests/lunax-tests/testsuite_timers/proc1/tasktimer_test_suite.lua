function task_func0()
  --print("dddd ===> ");
  TEST_task = 1;
 end

function task_func1()
  lunax.timer.del(tm);
  TEST_task = 1;
 end
 
 function task_func3()
  lunax.timer.del(tm);
  lunax.timer.del(tm1);
 end
 
function task_func_stoptimer(tmr)
  lunax.timer.del(tmr);
 end

function task_func4()
  lunax.wait(2000);
  TEST_task = 1;
 end

testsuite_Timers = {};

lu = require('luaunit');

lunax=require("lunax");
lunax.init();

function testsuite_Timers:test0AddTask()
  TEST_task = 0;
  lunax.addTask(task_func0);
  lunax.tfetch();
  lu.assertEquals(TEST_task,1);
end;

function testsuite_Timers:test1addTimer()
  TEST_task = 0;
  tm = lunax.timer.add(5000,task_func1);
  lunax.timer.start(tm);
  lunax.tfetch();
  lu.assertEquals(TEST_task,1);
end;

function testsuite_Timers:test2delTimer()
  TEST_task = 0;
  tm = lunax.timer.add(5000,task_func0);
  lunax.timer.start(tm);
  lunax.addTask(task_func_stoptimer,tm);
  lunax.tfetch();
  lu.assertEquals(TEST_task,0);
end;

function testsuite_Timers:test3stopTimer()
  TEST_task = 0;
  tm = lunax.timer.add(1000,task_func0);
  lunax.timer.start(tm);
  tm1 = lunax.timer.add(5000,task_func3);
  lunax.timer.start(tm1);
  lunax.timer.stop(tm);
  lunax.tfetch();
  lu.assertEquals(TEST_task,0);
end;

function testsuite_Timers:test4delTask()
  TEST_task = 0;
  ts = lunax.addTask(task_func4);
  lunax.delTask(ts);
  lunax.tfetch();
  lu.assertEquals(TEST_task,0);
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");
