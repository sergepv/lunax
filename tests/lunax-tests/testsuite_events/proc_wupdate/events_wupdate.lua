function task1()
 lunax.copyV(lunax.attr.merge(),ln1[0], 12);
 lunax.copyV(lunax.attr.merge(),ln1["table1"], {"1",{"r", 34,"test table"},3});
 lunax.event.isHandlers(lunax.attr.wait(),ln1.table1,1);
 lunax.wait(1000);
 lunax.copyV(lunax.attr.update(),ln1.table1[2],{"r", 64,"test table"});
end;

function task2()
 lunax.copyV(lunax.attr.merge(),ln2["table1"], {"1",{"r", 34,"test table"},3});
 lunax.event.isHandlers(lunax.attr.wait(),ln2.table1,1);
 lunax.wait(1000);
 lunax.copyV(lunax.attr.merge(),ln2["table1"], {"1",{"r", 34,"test table"},3});
 lunax.copyV(lunax.attr.update(),ln2.table1[2],{"r", 164,"test table"});
end;


function task3()
  function task3_1()
    lunax.wait(10000);
    lunax.copyV(lunax.attr.merge(),ln3["table1"], {"1",{"r", 34,"test table"},3});
  end
  lunax.addTask(task3_1);
  lunax.event.isHandlers(lunax.attr.wait(),ln3.table1,1);
  lunax.copyV(lunax.attr.update(),ln3.table1[2],{"r", 174,"test table"});
end;

 lunax=require("lunax");
 lunax.init("serv_wupdate");
 ln1 = lunax.createT("wupdate_test1");
 ln2 = lunax.createT("wupdate_test2");
 ln3 = lunax.createT("wupdate_test3");

 lunax.addTask(task1);
 lunax.addTask(task2);
 lunax.addTask(task3);
 lunax.tfetch();
 
 