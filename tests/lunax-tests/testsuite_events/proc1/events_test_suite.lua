function update(x)
  lunax.event.unsetHandler(tb.table1);
  TEST_event = x[2][2]
 end;

 function update1(x)
  TEST_event1 = x[2][2]
  lunax.wait(10000);
  lunax.event.unsetHandler(tb1.table1);
 end;

 function update2(x)
  TEST_event2 = x[2][2]
  lunax.event.unsetHandler(tb2.table1);
 end;


 function init()
 tb = lunax.attach(lunax.attr.wait(),"serv1","event_test");
 lunax.event.setHandler(lunax.attr.wait(),tb.table1,update);
 end;

 function init1()
 tb1 = lunax.attach(lunax.attr.wait(),"serv1","event_test1");
 lunax.event.setHandler(lunax.attr.wait(),tb1.table1,update1);
 end;

 function init2()
 tb2 = lunax.attach(lunax.attr.wait(),"serv1","event_test2");
 lunax.event.setHandler(lunax.attr.wait(),tb2.table1,update2);
 end;


 function wupdate1()
   lunax.wait(1000);
   local tb = lunax.attach(lunax.attr.wait(),"serv_wupdate","wupdate_test1");
   lunax.isUpdate(lunax.attr.wait(), tb.table1);
   TEST_event3 = tb.table1[2][2];
end;

 function wupdate2()
   local tb = lunax.attach(lunax.attr.wait(),"serv_wupdate","wupdate_test2");
   lunax.isUpdate(lunax.attr.wait(), tb.table1);
   TEST_event4 = tb.table1[2][2];
end;

 function wupdate3()
   local tb = lunax.attach(lunax.attr.wait(),"serv_wupdate","wupdate_test3");
   lunax.isUpdate(lunax.attr.wait(), tb.table1);
   TEST_event5 = tb.table1[2][2];
end;

 function complex6()
   local tb = lunax.attach(lunax.attr.wait(),"serv1","event_test6");
   lunax.isUpdate(lunax.attr.wait(), tb.table1);
   lunax.isUpdate(lunax.attr.wait(), tb.table2[3][2]);
   TEST_event6 = tb.table2[3][2][2];
end;

function timeout7()
 l1 = lunax.attach(lunax.attr.wait(1000),"nonexist","timeout_test");
 if (l1 == nil) then TEST_event7_1 = 142; end;

 r = lunax.event.isHandlers(lunax.attr.wait(1500),timeout7_table.table1[1],1);
 if (r == 0) then TEST_event7_2 = 242; end;

 r = lunax.event.setHandler(lunax.attr.wait(2000),timeout7_table.table1,1);
 if (r == 0) then TEST_event7_3 = 342; end;

 r = lunax.isUpdate(lunax.attr.wait(1000),timeout7_table.table1)
  if (r == 0) then TEST_event7_4 = 442; end;
end;

 lu = require('luaunit');

 testsuite_Events = {};

 lunax=require("lunax");
 lunax.init("ev_test");

function testsuite_Events:test0AddHandler()
  TEST_event = 0;
  lunax.addTask(init);
  lunax.tfetch();
  lu.assertEquals(TEST_event,44);
end;

function testsuite_Events:test1MultiCalls()
  TEST_event1 = 0;
  lunax.addTask(init1);
  lunax.tfetch();
  lu.assertEquals(TEST_event1,54);
end;

function testsuite_Events:test2waitEvents()
  TEST_event2 = 0;
  lunax.addTask(init2);
  lunax.tfetch();
  lu.assertEquals(TEST_event2,65);
end;

function testsuite_Events:test3waitUpdate1()
  TEST_event3 = 0;
  lunax.addTask(wupdate1);
  lunax.tfetch();
  lu.assertEquals(TEST_event3,64);
end;

function testsuite_Events:test4waitUpdate2()
  TEST_event4 = 0;
  lunax.addTask(wupdate2);
  lunax.tfetch();
  lu.assertEquals(TEST_event4,164);
end;

function testsuite_Events:test5waitUpdate3_nonfield()
  TEST_event5 = 0;
  lunax.addTask(wupdate3);
  lunax.tfetch();
  lu.assertEquals(TEST_event5,174);
end;

function testsuite_Events:test6complexwaitEvent()
  TEST_event6 = 0;
  lunax.addTask(complex6);
  lunax.tfetch();
  lu.assertEquals(TEST_event6,642);
end;

function testsuite_Events:test7timeoutEvent()
  TEST_event7_1 = 0;
  TEST_event7_2 = 0;
  TEST_event7_3 = 0;
  TEST_event7_4 = 0;
  timeout7_table = lunax.createT("timeout_test");
  lunax.addTask(timeout7);
  lunax.tfetch();
  lu.assertEquals(TEST_event7_1,142);
  lu.assertEquals(TEST_event7_2,242);
  lu.assertEquals(TEST_event7_3,342);
  lu.assertEquals(TEST_event7_4,442);
end;

 return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml")
