 function update2(x)
   TEST_task11 = x[0];
   lunax.event.unsetHandler(t2);
end

function update3(x)
  TEST_task12 = x[0];
  lunax.event.unsetHandler(t3);
 end
 

 function task1()
   t2 = lunax.attach(lunax.attr.wait(),"ts_delete2","ts_table");
   t3 = lunax.attach(lunax.attr.wait(),"ts_delete3","ts_table");
   lunax.event.setHandler(lunax.attr.wait(),t2,update2);
   lunax.event.setHandler(lunax.attr.wait(),t3,update3);
   --print("proc1 -> ready");
   
 lunax.copyV(lunax.attr.update(),ln[0],1024);
 lunax.copyV(lunax.attr.update(),ln[3],{"test proc3",56,"fff"});
 lunax.wait(2000)
 lunax.copyV(lunax.attr.create(),ln[3],nil);
   
 end
 function task2()
   lunax.event.setHandler(lunax.attr.wait(),t2,update2);
   lunax.deleteT("ts_table");
   
 end
 
 
 testsuite_2stepsDeletion = {};

 lu = require('luaunit');

 lunax=require("lunax");
 lunax.init("ts_delete1");

 
 function testsuite_2stepsDeletion:test0ReadDeletion()
    ln = lunax.createT("ts_table");
    lunax.addTask(task1);
    TEST_task11 = 0;
    TEST_task12 = 0;
    lunax.tfetch();
    lu.assertEquals(TEST_task11,10222);
    lu.assertEquals(TEST_task12,333);
end;


 function testsuite_2stepsDeletion:test1ReadDeletion()
    TEST_task11 = 0;
    TEST_task12 = 0;
    lunax.addTask(task2);
    lunax.tfetch();
    lu.assertEquals(TEST_task11,444);
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");
