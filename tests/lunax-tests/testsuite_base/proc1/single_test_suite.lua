testsuite_Base_0_Single = {};

function testsuite_Base_0_Single:test0CreateSharedTable()
 luna1,status = lunax.createT("luna1");
 lu.assertIsTable(luna1);
end

function SetValue(sht)
 sht[1] = 9;
end

function testsuite_Base_0_Single:test1ErrorSetValue()
    lu.assertErrorMsgContains
    ('Error 3: Shared Table. Operation over lunax only',
    SetValue,luna1);
end

function testsuite_Base_0_Single:test2CopyValue()
    lunax.copyV(lunax.attr.merge(),luna1["val1"], 12);
    lu.assertEquals(luna1["val1"],12);
    lunax.copyV(lunax.attr.merge(),luna1["val2"], "test");
    lu.assertStrContains(luna1["val2"],"test");
    lunax.copyV(lunax.attr.merge(),luna1["val3"], false);
    lu.assertEquals(luna1["val3"],false);
    lunax.copyV(lunax.attr.merge(),luna1.ff, {"1",{"r","test table"},3});
    lu.assertStrContains(luna1["ff"][2][2],"test table");
    lunax.copyV(lunax.attr.merge(),luna1.ff[2], {"r","test table2"});
    lu.assertStrContains(luna1["ff"][2][2],"test table2");
    lunax.copyV(lunax.attr.merge(),luna1.ff[2][2], "test table3");
    lu.assertStrContains(luna1["ff"][2][2],"test table3");    
end

function testsuite_Base_0_Single:test3CopyValueEx()
  luna2 = lunax.createT("luna2");
  luna3 = lunax.createT("luna3");
  lunax.copyV(lunax.attr.merge(),luna2.tt,120)
  lunax.copyV(lunax.attr.merge(),luna2.tt1,"string")
  lunax.copyV(lunax.attr.merge(),luna3.arr,{g =120,g1 = luna2.tt+30})
  lunax.copyV(lunax.attr.merge(),luna2.arr,{g =120,g1 = luna2.tt+40})

  lu.assertEquals(luna3.arr.g1,150);
  lu.assertEquals(luna2.arr.g1,160);
  
end

return testsuite_Base_0_Single;

