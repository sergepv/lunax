
 function sleep(n)
  os.execute("sleep " .. tonumber(n))
 end
 
testsuite_Base_1_Multi = {};


function testsuite_Base_1_Multi:test0AttachToSharedTable()
 sleep(1);
 tt = lunax.ls.ls();
 print(lu.prettystr(tt));
for k, v in pairs(tt) do
  io.write("Server "..k..".");
  for key, value in pairs(v) do
    io.write(" ["..key .."]:".. value);
  end
  io.write("\n");
end
 tb = lunax.attach("serv1","serv1_luna1");
 tb2 = lunax.attach("serv2","serv2_luna1");
 lu.assertIsTable(tb);
 lu.assertIsTable(tb2);
end

function testsuite_Base_1_Multi:testGetValue_serv1()
 print(lu.prettystr(tb));
    lu.assertEquals(tb["val1"],12);
    lu.assertStrContains(tb["val2"],"test");
    lu.assertEquals(tb["val3"],false);
    lu.assertStrContains(tb["ff"][2][2],"test table");
end

function testsuite_Base_1_Multi:testGetValue_serv2()
 print(lu.prettystr(tb2));
    lu.assertEquals(tb2["val1"],12);
    lu.assertStrContains(tb2["val2"],"test");
    lu.assertEquals(tb2["val3"],false);
    lu.assertStrContains(tb2["ff"][2][2],"test table");
end

return testsuite_Base_1_Multi;


