
lu = require('luaunit');

lunax=require("lunax");
lunax.init();
lunax.setAlias("main")
ch = lunax.createT("luna0");
require("single_test_suite")
require("multi_test_suite")
res = lu.LuaUnit.run("-v","-o","junit","-n","tests.xml")
lunax.copyV(lunax.attr.merge(),ch[0],42);
return res

