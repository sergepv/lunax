function sleep(n)
 os.execute("sleep " .. tonumber(n))
end

function wait()
  while (EVEXT_CNT < 100) do
    lunax.wait(0);
  end
  -- print("free handl .."..tostring(tb))
    lunax.event.unsetHandler(tb);
end;
--[[ ============================================================== TEST 1 ]]
function update(x)
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
end;

function init()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e1");
  lunax.event.setHandler(lunax.attr.wait(),tb,update);
end;
--[[ ============================================================== TEST 2 ]]

function update_task2(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task2()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e2");
  lunax.event.setHandler(lunax.attr.wait(),tb,update_task2);
  sleep(5);
end;

--[[ ============================================================== TEST 3 ]]
function update_task3(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
  if EVEXT_CNT == 35 then
    lunax.copyV(lunax.attr.update(),e3_db[0], 1);
  end

end;

function init_task3()
  -- attach to e3
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e3");
  lunax.event.setHandler(lunax.attr.wait(),tb,update_task3);

  -- wait attach to e3_db
  lunax.event.isHandlers(lunax.attr.wait(),e3_db,1);

  lunax.copyV(lunax.attr.update(),e3_db[0], 1);
  sleep(5);
end;

--[[ ============================================================== TEST 4 ]]

function wait4()
  while (EVEXT_CNT < 100) do
    lunax.wait(0);
  end
  -- print("free handl .."..tostring(tb["task4"][4]))
    lunax.event.unsetHandler(tb["task4"][4]);
end;

function update_task4(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task4()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e4");
  lunax.event.setHandler(lunax.attr.wait(),tb["task4"][4],update_task4);
  sleep(5);
end;

--[[ ============================================================== TEST 5 ]]

function wait5()
  while (EVEXT_CNT < 50) do
    lunax.wait(0);
  end
    --print("wait free handl .."..tostring(tb["task5"][4]))
    lunax.wait(1000);
    lunax.event.unsetHandler(tb["task5"][4]);
end;

function update_task5(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task5()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e5");
  lunax.event.setHandler(lunax.attr.wait(),tb["task5"][4],update_task5);
  sleep(5);
end;

--[[ ============================================================== TEST 6 ]]

function wait6()
  while (EVEXT_CNT < 50) do
    lunax.wait(0);
  end
    --print("wait free handl .."..tostring(tb["task6"][4]))
    lunax.wait(1000);
    lunax.event.unsetHandler(tb["task6"][4]);
end;

function update_task6(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task6()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e6");
  lunax.event.setHandler(lunax.attr.wait(),tb["task6"][4],update_task6);
  sleep(5);
end;

--[[ ============================================================== TEST 7 ]]

function wait7()
  while (EVEXT_CNT < 1) do
    lunax.wait(0);
  end
    --print("wait free handl .."..tostring(tb["task7"][4]))
    lunax.wait(1000);
    lunax.event.unsetHandler(tb["task7"][4]);
end;

function update_task7(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task7()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e7");
  lunax.event.setHandler(lunax.attr.wait(),tb["task7"][4],update_task7);
  sleep(5);
end;

--[[ ============================================================== TEST 8 ]]

function wait8()
  while (EVEXT_CNT < 30) do
    lunax.wait(0);
  end
    --print("wait free handl .."..tostring(tb["task8"][4]))
    lunax.wait(1000);
    lunax.event.unsetHandler(tb["task8"][4]);
end;

function update_task8(x)
  --print("ggg ===> ".. EVEXT_CNT .. " > "  .. x[0]);
  EVEXT[EVEXT_CNT] = x[0];
  EVEXT_CNT = EVEXT_CNT + 1;
end;

function init_task8()
  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext","e8");
  lunax.event.setHandler(lunax.attr.wait(),tb["task8"][4],update_task8);
  sleep(5);
end;

--[[ ============================================================== MAIN ]]
testsuite_events_ext = {};
lu = require('luaunit');

lunax=require("lunax");
lunax.init("ts_events_ext_dp");

EVEXT_CNT = 0;
EVEXT = {};

function testsuite_events_ext:test0SyncEvent()
  lunax.addTask(wait);
  lunax.addTask(init);
  lunax.tfetch();
  local i;
  for i= 0,99 do
    lu.assertEquals(EVEXT[i], i);
  end
end;


function testsuite_events_ext:test1SyncPipeEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait);
  lunax.addTask(init_task2);
  lunax.tfetch();
  local i;
  for i= 0,99 do
    lu.assertEquals(EVEXT[i], 42+i);
  end
end;

function testsuite_events_ext:test2SyncPipePackEvent()
  EVEXT_CNT = 0;
  e3_db = lunax.createT("e3_db");
  lunax.addTask(wait);
  lunax.addTask(init_task3);
  lunax.tfetch();
  local i;
  for i= 0,99 do
    lu.assertEquals(EVEXT[i], i);
  end
end;

function testsuite_events_ext:test4SyncPipeInerTableEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait4);
  lunax.addTask(init_task4);
  lunax.tfetch();
  local i;
  for i= 0,99 do
    lu.assertEquals(EVEXT[i], 142+i);
  end
end;

function testsuite_events_ext:test5SyncPipeMaxValueEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait5);
  lunax.addTask(init_task5);
  lunax.tfetch();
  local i;
  lu.assertEquals(EVEXT_CNT, 50);
  for i= 0,EVEXT_CNT-1 do
    lu.assertEquals(EVEXT[i], 242+i);
  end
end;

function testsuite_events_ext:test6ASyncPipeMaxValueEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait6);
  lunax.addTask(init_task6);
  lunax.tfetch();
  local i;
  lu.assertEquals(EVEXT_CNT, 50);
  for i= 0,EVEXT_CNT-1 do
    lu.assertEquals(EVEXT[i], 342+50+i);
  end
end;

function testsuite_events_ext:test7ChangeStratetgyEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait7);
  lunax.addTask(init_task7);
  lunax.tfetch();
  local i;
  lu.assertEquals(EVEXT_CNT, 1);
  lu.assertEquals(EVEXT[0], 442+49);
end;

function testsuite_events_ext:test8ChangeStratetgyExtEvent()
  EVEXT_CNT = 0;
  lunax.addTask(wait8);
  lunax.addTask(init_task8);
  lunax.tfetch();
  local i;
  lu.assertEquals(EVEXT_CNT, 30);
  for i= 0,EVEXT_CNT-1 do
    lu.assertEquals(EVEXT[i], 542+20+i);
  end
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");
