function task0()
  lunax.event.isHandlers(lunax.attr.wait(),e1,1);
  cnt =0
  while cnt< 100 do
    local st =lunax.copyV(lunax.attr.update(),e1[0], cnt);
    cnt = cnt +1;
    --lunax.wait(1000);
  end;
end
--[[ ============================================================== TEST 1 ]]
function task1()
  lunax.event.isHandlers(lunax.attr.wait(),e1,1);
  lunax.event.setStrategy(e1, lunax.event.EV_SYNC_SYMPLE_UPDATE);
  cnt =0
  while cnt< 100 do
    while (lunax.copyV(lunax.attr.update(),e1[0], cnt) == 1) do
      lunax.wait(0)
    end
    cnt = cnt +1;
  end;
end

--[[ ============================================================== TEST 2 ]]
function task2()
  lunax.event.isHandlers(lunax.attr.wait(),e2,1);
  lunax.event.setStrategy(e2,lunax.event.EV_SYNC_PIPE_UPDATE);
  for i=0,99 do
    lunax.copyV(lunax.attr.update(),e2[0], 42+i);
  end
end

--[[ ============================================================== TEST 3 ]]
function update_task3_db (x)

  for i=0,49 do
    lunax.copyV(lunax.attr.update(),e3[0], TASK3_INT);
    TASK3_INT = TASK3_INT+1;
  end
  if TASK3_INT == 100 then
    lunax.event.unsetHandler(tb);
  end
end

function task3()
  TASK3_INT = 0;
    -- wait attach to e3
  lunax.event.isHandlers(lunax.attr.wait(),e3,1);
    -- attach to e3_db

  tb = lunax.attach(lunax.attr.wait(),"ts_events_ext_dp","e3_db");
  lunax.event.setHandler(lunax.attr.wait(),tb,update_task3_db);

  lunax.event.setStrategy(e3,lunax.event.EV_SYNC_PIPE_UPDATE);
end

--[[ ============================================================== TEST 4 ]]
function task4()
  lunax.copyV(lunax.attr.update(),e4["task4"],{})
  lunax.copyV(lunax.attr.update(),e4["task4"][4],{"hello"})
  lunax.event.isHandlers(lunax.attr.wait(),e4["task4"][4],1);
  lunax.event.setStrategy(e4["task4"][4],lunax.event.EV_SYNC_PIPE_UPDATE);
  for i=0,99 do
    lunax.copyV(lunax.attr.update(),e4["task4"][4][0], 142+i);
  end
end

--[[ ============================================================== TEST 5 ]]
function task5()
  lunax.copyV(lunax.attr.update(),e5["task5"],{})
  lunax.copyV(lunax.attr.update(),e5["task5"][4],{"hello"})
  lunax.event.isHandlers(lunax.attr.wait(),e5["task5"][4],1);
  lunax.event.setStrategy(e5["task5"][4],lunax.event.EV_SYNC_PIPE_UPDATE,50);
  for i=0,99 do
    if (lunax.copyV(lunax.attr.update(),e5["task5"][4][0], 242+i) == 1) then
      --print ("vvvv ".. i)
      break;
    end
  end
end

--[[ ============================================================== TEST 6 ]]
function task6()
  lunax.copyV(lunax.attr.update(),e6["task6"],{})
  lunax.copyV(lunax.attr.update(),e6["task6"][4],{"hello"})
  lunax.event.isHandlers(lunax.attr.wait(),e6["task6"][4],1);
  lunax.event.setStrategy(e6["task6"][4],lunax.event.EV_ASYNC_PIPE_UPDATE,50);
  for i=0,99 do
    if (lunax.copyV(lunax.attr.update(),e6["task6"][4][0], 342+i) == 1) then
      --print ("vvvv ".. i)
      break;
    end
  end
end

--[[ ============================================================== TEST 7 ]]
function task7()
  lunax.copyV(lunax.attr.update(),e7["task7"],{})
  lunax.copyV(lunax.attr.update(),e7["task7"][4],{"hello"})
  lunax.event.isHandlers(lunax.attr.wait(),e7["task7"][4],1);
  lunax.event.setStrategy(e7["task7"][4],lunax.event.EV_SYNC_PIPE_UPDATE,50);
  for i=0,99 do
    if (lunax.copyV(lunax.attr.update(),e7["task7"][4][0], 442+i) == 1) then
      --print ("vvvv ".. i)
      break;
    end
  end
  lunax.event.setStrategy(e7["task7"][4],lunax.event.EV_SYNC_SYMPLE_UPDATE);
end

--[[ ============================================================== TEST 8 ]]
function task8()
  lunax.copyV(lunax.attr.update(),e8["task8"],{})
  lunax.copyV(lunax.attr.update(),e8["task8"][4],{"hello"})
  lunax.event.isHandlers(lunax.attr.wait(),e8["task8"][4],1);
  lunax.event.setStrategy(e8["task8"][4],lunax.event.EV_SYNC_PIPE_UPDATE,50);
  for i=0,99 do
    if (lunax.copyV(lunax.attr.update(),e8["task8"][4][0], 542+i) == 1) then
      --print ("vvvv ".. i)
      break;
    end
  end
  lunax.event.setStrategy(e8["task8"][4],lunax.event.EV_SYNC_PIPE_UPDATE,30);
end

--[[ ============================================================== MAIN ]]
lunax=require("lunax");
lunax.init("ts_events_ext");
e1 = lunax.createT("e1");
e2 = lunax.createT("e2");
e3 = lunax.createT("e3");
e4 = lunax.createT("e4");
e5 = lunax.createT("e5");
e6 = lunax.createT("e6");
e7 = lunax.createT("e7");
e8 = lunax.createT("e8");
lunax.addTask(task1);
lunax.addTask(task2);
lunax.addTask(task3);
lunax.addTask(task4);
lunax.addTask(task5);
lunax.addTask(task6);
lunax.addTask(task7);
lunax.addTask(task8);
lunax.tfetch();
