--[[ ============================================================== TEST 1 ]]
function task1()
    lunax.copyV(lunax.attr.merge(),db.ready,{});
    lunax.copyV(lunax.attr.merge(),db.dev,{});
    lunax.copyV(lunax.attr.merge(),db.dev[1],"name1");
    lunax.copyV(lunax.attr.merge(),db.dev[2],"name2");
    lunax.copyV(lunax.attr.merge(),db.dev["str"],"str_name");
    lunax.event.setHandler(lunax.attr.wait(),db.ready,function(x) lunax.event.unsetHandler(x); end);
    lunax.copyV(lunax.attr.update(),db.ready[1], 42);

    lunax.event.isHandlers(lunax.attr.wait(20),db.dev,1);
    lunax.event.setStrategy(db.dev, lunax.event.EV_SYNC_PIPE_UPDATE, 10);
    lunax.copyV(lunax.attr.update(),db.dev[1], "test1");
    lunax.copyV(lunax.attr.update(),db.dev[1], "test2");
    lunax.copyV(lunax.attr.update(),db.dev[1], "test3");
  end

--[[ ============================================================== MAIN ]]
lunax=require("lunax");
lunax.init("pair_test");
db = lunax.createT("pair_db");
lunax.addTask(task1);
lunax.tfetch();