function sleep(n)
    os.execute("sleep " .. tonumber(n))
end
  
function update(x)
    -- print("update ".. tb.dev[1])
    if (x[1] == "test3") then
        lunax.event.unsetHandler(tb.dev);
    end
end;
  
function init()
    tb = lunax.attach(lunax.attr.wait(),"pair_test","pair_db");
    lunax.event.isHandlers(lunax.attr.wait(),tb.ready,1);
    -- [[ DB is ready ]]
    for k,v in pairs(tb.dev) do
        if (k == 1) then lu.assertEquals(v,"name1"); end
        if (k == 2) then lu.assertEquals(v,"name2"); end
        if (k == "str") then lu.assertEquals(v,"str_name"); end
        -- print("---wait->" .. v);
    end

    lunax.event.setHandler(lunax.attr.wait(),tb.dev,update);
    -- lunax.wait(2000);
    sleep(5);
end;
   
lu = require('luaunit');

testsuite_Pairs = {};

lunax=require("lunax");
lunax.init();

function testsuite_Pairs:test0AddHandler()
    lunax.addTask(init);
    lunax.tfetch();
end

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");