function sleep(n)
    os.execute("sleep " .. tonumber(n))
end

function init()
    tb = lunax.attach(lunax.attr.wait(),"pair_test","pair_db");
    lunax.event.isHandlers(lunax.attr.wait(),tb.ready,1);
    sleep(1);
    for k,v in pairs(tb.dev) do
        if (k == 1) then lu.assertEquals(v,"test3"); end
        if (k == 2) then lu.assertEquals(v,"name2"); end
        if (k == "str") then lu.assertEquals(v,"str_name"); end
        -- print("---->" .. v);
    end

    for k,v in ipairs(tb.dev) do
        lu.assertNotEquals(k,"str");
        if (k == 1) then lu.assertEquals(v,"test3"); end
        if (k == 2) then lu.assertEquals(v,"name2"); end
        -- print("-- 222 -->" .. v);
    end
end;

lu = require('luaunit');

testsuite_Pairs = {};

lunax=require("lunax");
lunax.init();

function testsuite_Pairs:test0AddHandler()
 TEST_event = 0;
 lunax.addTask(init);
 lunax.tfetch();
end;

return lu.LuaUnit.run("-v","-o","junit","-n","tests.xml");