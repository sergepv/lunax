local t ={}
t.pid2pid_kv_name = function (k,v)
    local key_name = "{tidnumber:int}";
    local val_name = "{pid2thr_table}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

t.pid2thr_kv_name = function (k,v)
    local key_name = "{pid_num:int}";
    local val_name = "{bool}";
    if (k == 0) then key_name = "{0:const}" val_name = "{real_pid_number:int}" end
    if (k == 1) then key_name = "{1:const}" val_name = "{real:thread:int}" end

    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

return t;
