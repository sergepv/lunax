local t ={}
t.shared_kv_name = function (k,v)
    local key_name = "{user_key}";
    local val_name = "{user_value}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

t.shared_ref_kv_name = function (k,v)
    local key_name = "{light: int}";
    local val_name = "{shared_tabe}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

t.shared_events_kv_name = function (k,v)
    local key_name = "{tidnumber: int}";
    local val_name = "{table_events_lights}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

t.shared_events_lights_kv_name = function (k,v)
    local key_name = "{light: int}";
    local val_name = "{event_state}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end
return t;
