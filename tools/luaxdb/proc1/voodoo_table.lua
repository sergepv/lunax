local t ={}
t.voodoo_kv_name = function (k,v)
    local key_name = "{stack:int}";
    local val_name = "{sh table}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

return t;
