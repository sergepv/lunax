function string:split(sep)
   local sep, fields = sep or ":", {}
   local pattern = string.format("([^%s]+)", sep)
   self:gsub(pattern, function(c) fields[#fields+1] = c end)
   return fields
end

function pairsMain (t)
  function f(a,b)
    if (type(a) == "string") then
       if (a == "__LIGHT") then a =-1 else a = 0 end  
    end 
    if (type(b) == "string") then
       if (b == "__LIGHT") then b =-1 else  b = 0 end  
    end     
    return a < b
  end
  local a= {}
  for n in pairs(t) do a[#a+1] = n end
  table.sort(a,f)
  local i = 0
  return function()
    i = i+1 
    return a[i],t[a[i]]
  end
end

function pairsTable (t)
  local function f(a,b)
    local a1 = 0
    local b1 = 0
    if (type(t[a]) == "userdata") then a1 = 1 end 
    if (type(t[b]) == "userdata") then b1 = 1 end
    
    if (type(t[a]) == "table") then a1 = 100 end 
    if (type(t[b]) == "table") then b1 = 100 end
    if (a1 == 0) and (type(a) == "string") then
       if (a == "__LIGHT") then a1 = -100   
       elseif (a == "__ETYPE") then a1 = -99 
       else a1 = -98 end         
    end 
    if (b1 == 0) and (type(b) == "string") then
       if (b == "__LIGHT") then b1 = -100
       elseif (b == "__ETYPE") then b1 = -99
       else b1 = -98 end  
    end 
    if (a1 == 0) then a1 = a end
    if (b1 == 0) then b1 = b end    
    return a1 < b1
  end
  local a= {}
  for n in pairs(t) do a[#a+1] = n end
  table.sort(a,f)
  local i = 0
  return function()
    i = i+1 
    return a[i],t[a[i]]
  end
end


function light ()
local k_name = "{id:const}";
local v_name = "{light:int}";
return k_name, v_name
end 


function event_type (val)
local k_name = "{event:const}";
local v_name = "{unknown:int}";
if (val == 8) then v_name = "{EV_ASYNC_SYMPLE_UPDATE}"; end
if (val == 9) then v_name = "{EV_SYNC_SYMPLE_UPDATE}"; end
return k_name, v_name
end 

--[[
Function prints table.
Params:
tb - table;
name - name of table;
tab - count of space symbols for indent
kv_name - function for determination of type keys and values
color - color of table
deep -table with params for build-in tables:
{
   param1 - nil,
   param2 - name,
   param3 - tab,
   param3 - kv_name,
   param3 - color,
   param3 - deep,
}   
--]]
function print_table(tb, name, tab, kv_name, color, deep)
  io.write("\n")
  local frm
  if (color == nil) then frm = string.format("\x1b[0m%%%d.%ds",tab,tab)
  else frm = string.format("\x1b[%d;1m%%%d.%ds",color,tab,tab)
  end
  io.write(string.format(frm.."{------------------------------- %s --------------------------\n"," ",name));
  for k,v in pairsTable(tb) do
    key_name, val_name = kv_name(k,v)
    local key_ft = string.format("%-24.24s [%16.16s]",key_name,tostring(k))
    local val_ft = string.format("%-16.16s %-24.24s",tostring(v), val_name)
        
    io.write (string.format(frm.."%43.43s = %-41.41s\n"," ",key_ft,val_ft));
    if (type(v) == "table") and (type(deep) ~= "number") then
       if (deep ~= nil) then       
         print_table (v, deep[2], tab+deep[3],deep[4],deep[5],deep[6])
       else
         print_table (v, "next",tab+10, kv_name, color+1, nill)
       end  
    end
  end
  if (color == nil) then io.write(string.format(frm.."%s"," ","}\n"))
  else
  io.write(string.format(frm.."%s"," ","}\n\x1b[0m"))
  end
end


print ("start");

print ("arg "..__LUNAX_IOTABLE.args);

args = __LUNAX_IOTABLE.args;

local args_table = args:split(" ");
print(type(args_table[2]))
print(args_table[2])
local deep = 0;

lunax = require("lunax");
lunax.init("__debug");

lu = require('luaunit');


if (args_table[1]:find("-main") ~= nil) then
     local tb = lunax.ls.main();
     local main_table = require('main_table');     
     if (args_table[2] == "0") then deep = 0 end
     if (args_table[2] == "1") then deep ={nil,"pid",30,main_table.pid_table_kv_name,32,nil} end
     if (args_table[2] == "2") then deep ={nil,"pid",20,main_table.pid_table_kv_name,32,{nil,"share table",20,main_table.shared_table_kv_name,33,nil}} end       
     
     print_table(tb,"main table",5,main_table.main_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1]:find("-pid2pid") ~= nil) then
     local pid2pid = require('pid2pid_table');
     local tb = lunax.ls.pid2pid();
     if (args_table[2] == "0") then deep = 0 end
     if (args_table[2] == "1") then deep ={nil,"pid2thr table",10,pid2pid.pid2thr_kv_name,32,nil} end
     print_table(tb,"pid2pid table",5,pid2pid.pid2pid_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1]:find("-voodoo") ~= nil) then
     local voodoo = require('voodoo_table');
     local main_table = require('main_table');   
     local tb = lunax.ls.voodoo();
     if (args_table[2] == "0") then deep = 0 end
     if (args_table[2] == "1") then deep ={nil,"share table",20,main_table.shared_table_kv_name,33,nil} end
     print_table(tb,"voodoo table",5,voodoo.voodoo_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1] == "-sh") then
     local kvn = require('shared_table');
     local tb = lunax.ls.sh(args_table[2],args_table[3]);
     deep = nil;
     if (args_table[4] == "0") then deep = 0 end
     if (args_table[4] == "1") then deep ={nil,"iter1",20,kvn.shared_kv_name,32,0} end
     if (args_table[4] == "2") then deep ={nil,"iter1",20,kvn.shared_kv_name,32,{nil,"iter2",20,kvn.shared_kv_name,33,0}} end       
     
     print_table(tb,"shared table",5,kvn.shared_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1] == "-shref") then
     local kvn = require('shared_table'); 
     local tb = lunax.ls.shref(args_table[2],args_table[3]);
     deep = nil;     
     print_table(tb,"shared table",5,kvn.shared_ref_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1] == "-shevents") then
     local kvn = require('shared_table');
     local tb = lunax.ls.shevents(args_table[2],args_table[3]);
     deep ={nil,"iter1",20,kvn.shared_events_lights_kv_name,32,0}   
     print_table(tb,"shared table",5,kvn.shared_events_kv_name,31,deep)
     io.write("\n");
end

if (args_table[1] == "-dbg") then
     lunax.createT("__dbg"); 
     io.write(" __dbg created \n");
end

if (args_table[1] == "-help") or (args_table[1] == "-?") then
     io.write(string.format("%-15.15s [%s] [%s] [%s] : %s\n","-sh","pid_name","table name", "deep", "show shared table" ))
end

lunax.fetch(1);
