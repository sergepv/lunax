local t ={}
t.main_kv_name = function (k,v)
    local key_name = "{tidnumber:int}";
    local val_name = "{pid_table}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
    end
    return key_name, val_name
end

t.pid_table_kv_name = function (k,v)
    local key_name = "{name:string}";
    local val_name = "{sh table}";
    
    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
      if (k == "n") then key_name = "{n:const}" val_name = "{int}" end
      if (k == "name") then key_name = "{name:const}" val_name = "{string}" end
    end
    return key_name, val_name
end

t.shared_table_kv_name = function (k,v)
    local key_name = "{pid_num:int}";
    local val_name = "{int}";
    if (k == 1) then key_name = "{LSTATE_KEY:const}" val_name = "{pointer}" end
    if (k == 2) then key_name = "{SEM_KEY:const}" val_name = "{pointer}" end

    if (type(k) == "string") then
      if (k == "__ETYPE") then key_name, val_name = event_type(v) end
      if (k == "__LIGHT") then key_name, val_name = light(v) end
      if (k == "owner") then key_name = "{const}"; val_name = "{pid_num:int}" end;
    end
    return key_name, val_name
end

return t;
