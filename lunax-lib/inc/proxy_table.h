/*
** proxy_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNAX_CODE_INC_PROXY_TABLE_H_
#define LUNAX_CODE_INC_PROXY_TABLE_H_

#define NONEXIST_DEEP_KEY 0
#define NONEXIST_TABLE_KEY 1
#define NONEXIST_KEYS_KEY 2
#define NONEXIST_IDENT_KEY "nonexist"


int call_libfuncs_create (lua_State* L);
int call_libfuncs_merge (lua_State* L);
int call_libfuncs_update (lua_State* L);

int call_libfuncs_check (lua_State* L);
int call_libfuncs_wait (lua_State* L);
void set_read_check_attr(lua_State* L);
void clear_read_check_attr(lua_State* L);

int is_exist_table (lua_State *L, int objindex);
int resolve_nonexist_table(lua_State* L);

int check_proxy_table (lua_State* L, int pid, const char* name);
int create_proxy_table(lua_State* L, lua_State* lstate, sem_t* lsem, pid_t pid, const char* name);
int copy_value_by_proxy_table (lua_State* L);
int set_event_strategy_by_proxy_table (lua_State* L);
int get_event_strategy_by_proxy_table (lua_State* L);
int get_event_strategy_by_proxy_table_inner (lua_State* L);
int set_event_handler_by_proxy_table (lua_State* L);
int unset_event_handler_by_proxy_table (lua_State* L);
int update_event_by_proxy_table_inner (lua_State* L, events_type strategy, int startetgy_size);
#endif /* LUNAX_CODE_INC_PROXY_TABLE_H_ */
