/*
** deepcopy.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNAX_CODE_INC_DEEPCOPY_H_
#define LUNAX_CODE_INC_DEEPCOPY_H_

#include "lua.h"
#include "lauxlib.h"

int xmerge (lua_State *lstate);

int xcopy1(lua_State *L, lua_State *T, int n);
int xcopy(lua_State *L, lua_State *T, int t);
int xcopy2_deep(lua_State *L, lua_State *T, int n,int deep);
int xcopy_deep(lua_State *L, lua_State *T, int t, int deep);


#endif /* LUNAX_CODE_INC_DEEPCOPY_H_ */
