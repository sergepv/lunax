/*
** mem_main.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/
#ifndef LUNAX_CODE_INC_MEM_MAIN_H_
#define LUNAX_CODE_INC_MEM_MAIN_H_

int delete_main_table();
int delete_from_main_table(lua_State* L ,pid_t pid, const char* name);
void* get_base_addr();
lua_State* get_main_table();
uint32_t get_inc_main_counter();
sem_t* get_main_sem ();
int init_mem_main(void* addr);
int attach_to_mem_main(void* addr);
int show_main_table( lua_State* L);
int is_table (lua_State* L, int pid, const char* name);
int add_pid_to_pid2pid(unsigned long long thr);
POINTER_INT* lunax_threads_join (void);
int delete_pid2pid_table();
int delete_pid2thr_tables( int tid);
#endif /* LUNAX_CODE_INC_MEM_MAIN_H_ */
