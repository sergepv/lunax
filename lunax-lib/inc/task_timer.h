/*
** task_timer.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef INC_TASK_TIMER_H_
#define INC_TASK_TIMER_H_

#include "lunax.h"

int create_threads_table(lua_State* L);
int add_task_in_proccess (lua_State* L);
int del_task_in_proccess (lua_State* L);

int check_task_in_threads_table(lua_State* L);
int add_task_in_threads_table (lua_State* L);
int add_event_task_in_threads_table (lua_State* L);
int del_task_in_threads_table (lua_State* L);
int fetch_by_threads_table (lua_State* L);

int get_ms (long int* sec, long int* msec);
int create_timers_table(lua_State* L);
int add_timer_in_proccess (lua_State* L);
int del_timer_in_proccess (lua_State* L);
int start_timer_in_proccess (lua_State* L);
int stop_timer_in_proccess (lua_State* L);
int fetch_by_timers_table (lua_State* L);
#endif /* INC_TASK_TIMER_H_ */
