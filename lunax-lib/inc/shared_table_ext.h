/*
** share_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef INC_SHARED_TABLE_EXT_H_
#define INC_SHARED_TABLE_EXT_H_

#include "lunax.h"

uint32_t create_shared_table_inner(lua_State* lstate);
#ifndef  SH_SIMPLE
int sync_diff_index(lua_State* lstate);
int sync_shared_table_diff_inner(lua_State* L);
int sync_shared_table_diff_inner_inner(lua_State* lstate);
int repack_shared_table_diff(lua_State* lstate, int strategy_size);
#endif

#endif /* INC_SHARED_TABLE_EXT_H_ */
