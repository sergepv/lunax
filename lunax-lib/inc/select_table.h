/*
** select_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNAX_CODE_INC_SELECT_TABLE_H_
#define LUNAX_CODE_INC_SELECT_TABLE_H_

#include "lunax.h"

int create_select_table(lua_State* L);
int get_proxy(lua_State* L, lua_State* lstate, sem_t* lsem, pid_t pid, const char* name);
int del_proxy(lua_State* L);
int null_for_proxy_tables(lua_State* L, lua_State* lstate);
int null_for_proxy_table(lua_State* L, lua_State* lstate, uint32_t light);

#endif /* LUNAX_CODE_INC_SELECT_TABLE_H_ */
