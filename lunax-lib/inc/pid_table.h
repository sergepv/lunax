/*
** main_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNAX_CODE_INC_PID_TABLE_H_
#define LUNAX_CODE_INC_PID_TABLE_H_

#include "lua.h"

int create_pid_table(lua_State* LMAIN, int pid, const char* name);
int set_alias_pid_table (lua_State* LMAIN, const char* name);
int delete_pid_table_inner(lua_State* LMAIN);
int delete_pid_table(int pid);

int find_pid_table(lua_State* LMAIN);

void clear_tid_in_sh_table(lua_State* LMAIN);
void inc_tid_in_sh_table(lua_State* LMAIN);
void find_and_inc_tid_in_sh_table();
void dec_tid_in_sh_table(lua_State* LMAIN);
int try_delete_sh_table(lua_State* LMAIN);
int force_delete_sh_table(lua_State* LMAIN);

#endif /* LUNAX_CODE_INC_PID_TABLE_H_ */
