/*
** events_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef INC_EVENTS_TABLE_H_
#define INC_EVENTS_TABLE_H_

#include "lunax.h"

int create_events_table(lua_State* L);
unsigned int add_handler (lua_State* L);
int del_handler (lua_State* L);
int check_events_by_events_table (lua_State* L);
int check_update_table_by_events_table(lua_State* L);
int clear_event_state_by_events_table(lua_State* L);
int fetch_by_events_table (lua_State* L);

#endif /* INC_EVENTS_TABLE_H_ */
