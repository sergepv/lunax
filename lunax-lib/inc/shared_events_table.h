/*
** shared_events_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef INC_SHARED_EVENTS_TABLE_C_
#define INC_SHARED_EVENTS_TABLE_C_

#include "lunax.h"


int reg_event_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid);
int unreg_event_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid);
int update_event_in_shared_events(lua_State* L, lua_State* lstate, events_type startetgy, int startetgy_size);
int check_event_state_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid);
int clear_event_state_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid);
int check_events_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate);
int set_maxdiff_in_shared_events(lua_State* lstate, events_type startetgy, int newmaxdiff);

#endif /* INC_SHARED_EVENTS_TABLE_C_ */
