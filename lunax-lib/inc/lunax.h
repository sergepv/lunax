/*
** lunax.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNX_CODE_INC_LUNAX_H_
#define LUNX_CODE_INC_LUNAX_H_

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
//#include <stat.h>
#include <setjmp.h>

#include <unistd.h>
#include <string.h>
#include <semaphore.h>

#include "lua.h"
#include "lauxlib.h"

typedef unsigned int uint32_t;
//typedef unsigned long long uint64_t;

#define NAME_SHARED_MEM "/lunax_shared_mem"
#define NAME_SEM_MAIN "/lunax_sem_main"


#define mtable "mtable"
#define pid2pid "pid2pid"
#define voodoo "voodoo"
#define shtable "sh"
#define shtable_ref "shref"
#define shtable_events "events"
#define PMPA_DETECT 0x11436d7

#define ERRORCODE_CLunaxError 300
#define ERRORCODE_CRmShtableError 301

#define TRANSACTION_WRITE_STOP   0
#define TRANSACTION_WRITE_CREATE 1
#define TRANSACTION_WRITE_MERGE  2
#define TRANSACTION_WRITE_UPDATE 3

#define TRANSACTION_WRITE_STATUS_NONE   0
#define TRANSACTION_WRITE_STATUS_FIRST  1
#define TRANSACTION_WRITE_STATUS_NEXT   2
#define TRANSACTION_WRITE_STATUS_READ   3
#define TRANSACTION_WRITE_STATUS_CHECK  4

//attr coe
#define ATTR_NONE  0
#define ATTR_CHECK 1
#define ATTR_WAIT  2


#define SEM_WAIT(X) while (sem_wait(X)){;}

#if defined(__i386__)
extern uint32_t G_LUNAX_ADDR;
#define POINTER_INT uint32_t
#define ARCH_32BIT
#elif defined(__x86_64__)
extern uint64_t G_LUNAX_ADDR;
#define POINTER_INT uint64_t
#define ARCH_64BIT
#elif defined(__arm__)
extern uint32_t G_LUNAX_ADDR;
#define POINTER_INT uint32_t
#define ARCH_32BIT
#else
#error "Unknown platform architecture."
#endif


//#define LUNAX_SINGLE
#ifndef LUNAX_SINGLE
#define LUNAX_RE_CAST(X) (void*)((char*)(X) - G_LUNAX_ADDR)
#define LUNAX_CAST(X) (void*)((char*)(X) + G_LUNAX_ADDR)
#else
#define LUNAX_RE_CAST(X) X
#define LUNAX_CAST(X) X
#endif

#ifdef DEBUG
#define dbg(...) do {printf("[%d][%s:%d] ",gettid(),__FUNCTION__,__LINE__);printf( __VA_ARGS__);printf("\n");} while(0)

#define DBG_STACK(X,Y) do {printf("[%d][%s]: ",gettid(),Y);printf("[%s:%d]  STACK(%s[%d]): %s - %s - %s - %s -%s\n",\
                     __FUNCTION__,__LINE__,#X,lua_gettop(X),\
                     lua_typename(X, lua_type(X, -5)),\
                     lua_typename(X, lua_type(X, -4)),\
                     lua_typename(X, lua_type(X, -3)),\
                     lua_typename(X, lua_type(X, -2)),\
                     lua_typename(X, lua_type(X, -1)));}while(0)

#else
#define dbg(...)
#define DBG_STACK(X,Y)
#endif

typedef enum t_wait_state
{
    W_TIMEOUT = 0,
    W_SET = 1,
    W_UNSET = 2,

} waitstate;

typedef enum t_error_type
{
	E_OK = 0,
	E_ERROR,
	E_BAD_ARG,
	E_ACCESS_DENIED,
	E_SHTABLE_ALREADY_EXIST,
} eerror;

typedef enum t_events_type
{
	EV_ASYNC_SYMPLE_UPDATE = 8,
	EV_SYNC_SYMPLE_UPDATE = 9,
    EV_SYNC_PIPE_UPDATE = 10,
    EV_ASYNC_PIPE_UPDATE = 11,


} events_type;

typedef enum t_events_state
{
    SHDIFF_EMPTY = -1,
    SHDIFF_EXIST = 0,

} events_state;

#define SIZE_SHMEMORY 2800000


int create_get_metatable (lua_State* L, lua_State* state, sem_t* sem, int pid, const char* name);
int get_value4index (lua_State* L);
int create_table (lua_State* L);
int delete_table (lua_State* L);
int copy_value (lua_State* L);
int attach (lua_State* L);
int detach (lua_State* L);
int set_alias (lua_State* L);
int get_alias (lua_State* L);
int fetch (lua_State* L);
int add_task (lua_State* L);
int del_task (lua_State* L);
int wait_timer (lua_State* L);
int is_update_table(lua_State* L);
int lunax_timer_add(lua_State* L);
int lunax_timer_del(lua_State* L);
int lunax_timer_start(lua_State* L);
int lunax_timer_stop(lua_State* L);
int tfetch (lua_State* L);
int dfetch (lua_State* L);
int dbg_show_shtable (lua_State* L);

void settid(lua_State* L, uint32_t tid);
uint32_t gettid(void);

#endif /* LUNX_CODE_INC_LUNAX_H_ */
