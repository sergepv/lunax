/*
** shared_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef LUNAX_CODE_INC_SHARED_TABLE_H_
#define LUNAX_CODE_INC_SHARED_TABLE_H_

#include "lunax.h"

typedef enum T_SH_TABLE_STATE
{
    SH_LIGHT_NONEXIST,
    SH_LIGHT_EXIST,
    SH_LIGHT_DELETED_LEAF,
    SH_LIGHT_DELETED_ROOT,

} SH_TABLE_STATE;

uint32_t genlight();
uint32_t getlight(lua_State *from);
SH_TABLE_STATE light_to_table (lua_State *from, lua_State *to);
uint32_t  table_to_light (lua_State *from, lua_State *to);
lua_State* get_lstate(lua_State* LMAIN);
sem_t* get_lsem(lua_State* LMAIN);
int get_share_table (lua_State* L,lua_State* lstate, sem_t* lsem);
int getvalue_share_table_for_write(lua_State *L, lua_State* lstate, sem_t* lsem, int first);

int check_share_table(lua_State* L, int pid, const char* name);
int create_share_table (lua_State* L,int pid, const char* name);

int real_delete_share_table_inner(lua_State* LMAIN);
int delete_share_table_inner (lua_State* LMAIN);
int delete_shtable_inner(lua_State* LMAIN, const char* name);

SH_TABLE_STATE getvalue_share_table(lua_State *L, lua_State* lstate, sem_t* lsem);
SH_TABLE_STATE getvalue_share_table_nosem(lua_State *L, lua_State* lstate);

SH_TABLE_STATE next_share_table(lua_State *L, lua_State* lstate, sem_t* lsem, uint8_t iflag);

int setvalue_share_table_clean(lua_State *L, sem_t* sem);
int setvalue_share_table_create(lua_State *L, sem_t* sem);
int setvalue_share_table_merge(lua_State *L, sem_t* sem);
int setstrategy_share_table(lua_State *L, lua_State* lstate, sem_t* lsem, int strategy, int strategy_size);
int getstrategy_share_table(lua_State *L, lua_State* lstate, sem_t* lsem);
int getstrategy_share_table_inner(lua_State *L, lua_State* lstate);

int dbg_show_shared_table (lua_State* L);

#endif /* LUNAX_CODE_INC_SHARED_TABLE_H_ */
