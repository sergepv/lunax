/*
** ls.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#ifndef INC_LS_H_
#define INC_LS_H_

#include "lunax.h"

int ls(lua_State* L);
int ls_get_main_table(lua_State* L);
int ls_get_pid2pid_table(lua_State* L);
int ls_get_voodoo_table(lua_State* L);
int ls_get_shared_table(lua_State* L);
int ls_get_shared_table_ref(lua_State* L);
int ls_get_shared_table_events(lua_State* L);
void debug_monitor (lua_State* L);


#endif /* INC_LS_H_ */
