/*
** shared_events_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "shared_events_table.h"
#include "shared_table.h"
#include "deepcopy.h"

//In
// L | light
//Out
//L | empty
int reg_event_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate,
        int pid)
{
    SEM_WAIT(lsem);
    xcopy1(L, lstate, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | light
    lua_getglobal(lstate, shtable_events);
    //lstate | light; shtable_events;
    lua_pushinteger(lstate, pid);
    //lstate | light; shtable_events;pid;
    lua_pushvalue(lstate, -1);
    //lstate | light; shtable_events;pid;pid;
    lua_gettable(lstate, -3);
    //lstate | light; shtable_events;pid;vl
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        //lstate | light; shtable_events;pid;table_events_lights
        lua_pushvalue(lstate, -4);
        //lstate | light; shtable_events;pid;table_events_lights;light
        lua_gettable(lstate, -2);
        if (lua_type(lstate, -1) == LUA_TNIL)
        {
            lua_pop(lstate, 1);
            //lstate | light; shtable_events;pid;table_events_lights;
            lua_pushvalue(lstate, -4);
            //lstate | light; shtable_events;pid;table_events_lights;light;
            lua_pushinteger(lstate, SHDIFF_EMPTY);
            lua_settable(lstate, -3);
        }
        //lstate | light; shtable_events;pid;table_events_lights;
        lua_pop(lstate, 4);
    }
    else
    {
        lua_pop(lstate, 1);
        //lstate | light; shtable_events;pid;
        lua_newtable(lstate);
        //lstate | light; shtable_events;pid;new_table_events_lights
        {
            lua_pushvalue(lstate, -4);
            //lstate | light; shtable_events;pid;new_table_events_lights; ligth
            lua_pushinteger(lstate, SHDIFF_EMPTY);
            lua_settable(lstate, -3);
        }
        //lstate | light; shtable_events;pid;new_table_events_lights
        lua_settable(lstate, -3);
        //lstate | light; shtable_events;
        lua_pop(lstate, 2);
    }
    //lstate | empty;
    sem_post(lsem);
    return 0;
}

//In
// L | light
//Out
//L | empty
int unreg_event_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate,
        int pid)
{
    SEM_WAIT(lsem);
    xcopy1(L, lstate, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | light
    lua_getglobal(lstate, shtable_events);
    //lstate | light; shtable_events;
    lua_pushinteger(lstate, pid);
    //lstate | light; shtable_events;pid;
    lua_gettable(lstate, -2);
    //lstate | light; shtable_events;pid;vl
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        //lstate | light; shtable_events;pid;table_events_lights
        lua_pushvalue(lstate, -4);
        //lstate | light; shtable_events;pid;table_events_lights;light
        lua_gettable(lstate, -2);
        if (lua_type(lstate, -1) != LUA_TNIL)
        {
            lua_pop(lstate, 1);
            //lstate | light; shtable_events;pid;table_events_lights;
            lua_pushvalue(lstate, -4);
            //lstate | light; shtable_events;pid;table_events_lights;light;
            lua_pushnil(lstate);
            lua_settable(lstate, -3);
        }
        //lstate | light; shtable_events;pid;table_events_lights;
        lua_pop(lstate, 4);
    }
    //lstate | empty;
    sem_post(lsem);
    return 0;

}

//In
//lstate | shared_table;
//Out
//lstate | shared_table
int set_maxdiff_in_shared_events(lua_State* lstate, events_type startetgy, int newmaxdiff)
{
    events_state levent_state;
    uint32_t light;

    lua_getfield(lstate, -1, "__LIGHT");
    light = lua_tointeger(lstate, -1);
    lua_pop(lstate, 1);
    lua_getglobal(lstate, shtable_events);

    //lstate | shared_table; shtable_events
    lua_pushnil(lstate); /* first key */
    while (lua_next(lstate, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
        //lstate | shared_table; shtable_events; pid; shared_table_events
        lua_pushnil(lstate);
        while (lua_next(lstate, -2) != 0)
        {
            //lstate | shared_table; shtable_events; pid; shared_table_events;light;event state
            if (light == lua_tointeger(lstate, -2))
            {
                levent_state = lua_tointeger(lstate, -1);
                switch (startetgy)
                {
                case EV_SYNC_PIPE_UPDATE:
                case EV_ASYNC_PIPE_UPDATE:
                    if (newmaxdiff < levent_state)
                    {
                        levent_state = newmaxdiff;
                        //lstate | shared_table; shtable_events; pid; shared_table_events;light;event state
                        lua_pop(lstate,1);
                        lua_pushinteger(lstate,levent_state);
                        lua_settable(lstate,-3);
                        //lstate | shared_table; shtable_events; pid; shared_table_events;
                        lua_pushinteger(lstate,0);
                        lua_pushinteger(lstate,0);
                        //lstate | shared_table; shtable_events; pid; shared_table_events;0;0;

                    }
                    break;
                default:
                    break;
                }
                lua_pop(lstate, 2);
                //lstate | shared_table; shtable_events; pid;shared_table_events;
                break;

            }
            lua_pop(lstate, 1);
        }
        //lstate | shared_table; shtable_events; pid; shared_table_events;
        lua_pop(lstate, 1);
    }
    //lstate | shared_table; shtable_events;
    lua_pop(lstate,1);
    return 0;

}

//In
//lstate | shtable_events;
//Out
//lstate | shtable_events;
static int get_maxdiff_in_shared_events(lua_State* lstate, uint32_t light, events_type startetgy)
{
    int max_diff =-1;
    events_state levent_state;

    //lstate | shtable_events
    lua_pushnil(lstate); /* first key */
    while (lua_next(lstate, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
        //lstate |  shtable_events; pid; shared_table_events
        lua_pushnil(lstate);
        while (lua_next(lstate, -2) != 0)
        {
            //lstate | shtable_events; pid; shared_table_events;light;event state
            if (light == lua_tointeger(lstate, -2))
            {
                levent_state = lua_tointeger(lstate, -1);
                switch (startetgy)
                {
                case EV_SYNC_PIPE_UPDATE:
                case EV_ASYNC_PIPE_UPDATE:
                    if (max_diff < levent_state)
                    {
                        max_diff = levent_state;
                    }
                    break;
                default:
                    break;
                }
                lua_pop(lstate, 2);
                //lstate | shtable_events; pid;shared_table_events;
                break;

            }
            lua_pop(lstate, 1);
        }
        //lstate | shtable_events; pid; shared_table_events;
        lua_pop(lstate, 1);
    }
    //lstate | shtable_events;
    return max_diff;

}
//In
//lstate | shtable_events;
//Out
//lstate | empty;
static int pooling_in_shared_events(lua_State* lstate, uint32_t light, events_type startetgy, int maxdiff_limit)
{
    int max_diff =-1;
    events_state levent_state;

    //lstate | shtable_events
    lua_pushnil(lstate); /* first key */
    while (lua_next(lstate, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
        //lstate |  shtable_events; pid; shared_table_events
        lua_pushnil(lstate);
        while (lua_next(lstate, -2) != 0)
        {
            //lstate | shtable_events; pid; shared_table_events;light;event state
            if (light == lua_tointeger(lstate, -2))
            {
                levent_state = lua_tointeger(lstate, -1);
                switch (startetgy)
                {
                case EV_SYNC_SYMPLE_UPDATE:
                    if (levent_state > SHDIFF_EMPTY)
                    {
                        //lstate | shtable_events; pid; shared_table_events;light;event state
                        lua_pop(lstate, 5);
                        return 1;
                    }
                    levent_state = SHDIFF_EMPTY + 1;
                    //lstate | shtable_events; pid; shared_table_events;light;event state
                    lua_pop(lstate, 1);
                    lua_pushvalue(lstate, -1);
                    //lstate | shtable_events; pid; shared_table_events;light;light;
                    lua_pushinteger(lstate, levent_state);
                    //lstate | shtable_events; pid; shared_table_events;light;light;(levent_state)
                    lua_settable(lstate, -4);
                    //lstate | shtable_events; pid; shared_table_events;light;
                    lua_pop(lstate, 1);
                    break;
                case EV_ASYNC_SYMPLE_UPDATE:
                    levent_state = SHDIFF_EMPTY + 1;
                    //lstate | shtable_events; pid; shared_table_events;light;event state
                    lua_pop(lstate, 1);
                    lua_pushvalue(lstate, -1);
                    //lstate | shtable_events; pid; shared_table_events;light;light;
                    lua_pushinteger(lstate, levent_state);
                    //lstate | shtable_events; pid; shared_table_events;light;light;(levent_state)
                    lua_settable(lstate, -4);
                    //lstate | shtable_events; pid; shared_table_events;light;
                    lua_pop(lstate, 1);
                    break;
                case EV_SYNC_PIPE_UPDATE:
                    levent_state++;
                    if (max_diff < levent_state)
                    {
                        max_diff = levent_state;
                    }
                    //lstate | shtable_events; pid; shared_table_events;light;levent_state
                    lua_pop(lstate, 1);
                    lua_pushvalue(lstate, -1);
                    //lstate | shtable_events; pid; shared_table_events;light;light;
                    lua_pushinteger(lstate, levent_state);
                    //lstate | shtable_events; pid; shared_table_events;light;light;(levent_state)
                    lua_settable(lstate, -4);
                    //lstate | shtable_events; pid; shared_table_events;light;
                    lua_pop(lstate, 1);
                    break;
                case EV_ASYNC_PIPE_UPDATE:
                    if (maxdiff_limit != levent_state)
                    {
                        levent_state++;
                    }
                    if (max_diff < levent_state)
                    {
                        max_diff = levent_state;
                    }
                    //lstate | shtable_events; pid; shared_table_events;light;levent_state
                    lua_pop(lstate, 1);
                    lua_pushvalue(lstate, -1);
                    //lstate | shtable_events; pid; shared_table_events;light;light;
                    lua_pushinteger(lstate, levent_state);
                    //lstate | shtable_events; pid; shared_table_events;light;light;(levent_state)
                    lua_settable(lstate, -4);
                    //lstate | shtable_events; pid; shared_table_events;light;
                    lua_pop(lstate, 1);
                    break;

                }
                //lstate | shtable_events; pid;shared_table_events;
                break;

            }
            lua_pop(lstate, 1);
        }
        //lstate | shtable_events; pid; shared_table_events;
        lua_pop(lstate, 1);
    }
    //lstate | shtable_events;

    //lstate | shtable_events;
    lua_pop(lstate, 1);

    //lstate | empty;
    if (max_diff != -1)
    {
        lua_getglobal(lstate,shtable_ref);
        lua_pushinteger(lstate, light);
        lua_gettable(lstate,-2);
        //lstate | shared_table_ref; shared_table
        lua_getmetatable(lstate,-1);
        //lstate | shared_table_ref;shared_table;  meta_shared_table
        lua_pushinteger(lstate,max_diff);
        lua_setfield(lstate,-2,"__MAX");
        lua_pop(lstate,3);
    }
    return 0;

}

//In
// L | light
//Out
//L | empty
int update_event_in_shared_events(lua_State* L, lua_State* lstate, events_type startetgy, int startetgy_size)
{
    int max_diff =-1;
    uint32_t light;
    int maxdiff_limit = -2;

    light = lua_tointeger(L, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | empty
    lua_getglobal(lstate, shtable_events);
    //lstate | shtable_events;
    max_diff = get_maxdiff_in_shared_events(lstate,light,startetgy);
    //lstate | shtable_events;
    if (max_diff != -1)
    {
       if ((startetgy == EV_SYNC_PIPE_UPDATE) && (startetgy_size != -1))
       {
           if (max_diff >= startetgy_size - 1)
           {
               return 1;
           }
       }
       if ((startetgy == EV_ASYNC_PIPE_UPDATE) && (startetgy_size != -1))
       {
           if (max_diff >= startetgy_size - 1)
           {
               maxdiff_limit = max_diff;
           }
       }
    }
    return pooling_in_shared_events(lstate,light,startetgy,maxdiff_limit);
}

//In
// L | light
//Out
//L | empty
int clear_event_state_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid)
{
    int res = 0;
    events_state levent_state;

    SEM_WAIT(lsem);
    xcopy1(L, lstate, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | light
    lua_getglobal(lstate, shtable_events);
    //lstate | light; shtable_table_events;
    lua_pushinteger(lstate, pid);
    //lstate | light; shtable_table_events;pid;
    lua_gettable(lstate, -2);
    //lstate | light; shtable_events;vl
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        //lstate | light; shtable_table_events; table_events_lights
        lua_pushvalue(lstate, -3);
        //lstate | light; shtable_table_events; table_events_lights;light;
        lua_gettable(lstate, -2);
        //lstate | light; shtable_table_events; table_events_lights;event_state
        if (lua_type(lstate, -1) == LUA_TNUMBER)
        {
            levent_state = lua_tointeger(lstate, -1);
            if (levent_state > SHDIFF_EMPTY)
            {
                levent_state--;
                res = 1;
                lua_pop(lstate, 1);
                //lstate | light; shtable_table_events; table_events_lights;
                lua_pushvalue(lstate, -3);
                //lstate | light; shtable_table_events; table_events_lights; light
                lua_pushinteger(lstate, levent_state);
                //lstate | light; shtable_table_events; table_events_lights; light; (levent_state)
                lua_settable(lstate, -3);
                //lstate | light; shtable_table_events;table_events_lights;
            }
            else
            {
                lua_pop(lstate, 1);
            }
        }
        else
        {
            lua_pop(lstate, 1);
        }
        //lstate | light; shtable_table_events; table_events_lights;
    }
    lua_pop(lstate, 3);
    //lstate | empty;
    sem_post(lsem);
    return res;
}
//In
// L | light
//Out
//L | empty
int check_event_state_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate, int pid)
{
    int res = 0;
    events_state levent_state;

    SEM_WAIT(lsem);
    xcopy1(L, lstate, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | light
    lua_getglobal(lstate, shtable_events);
    //lstate | light; shtable_table_events;
    lua_pushinteger(lstate, pid);
    //lstate | light; shtable_table_events;pid;
    lua_gettable(lstate, -2);
    //lstate | light; shtable_events;vl
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        //lstate | light; shtable_table_events; table_events_lights
        lua_pushvalue(lstate, -3);
        //lstate | light; shtable_table_events; table_events_lights;light;
        lua_gettable(lstate, -2);
        //lstate | light; shtable_table_events; table_events_lights;event_state
        if (lua_type(lstate, -1) == LUA_TNUMBER)
        {
            levent_state = lua_tointeger(lstate, -1);
            if (levent_state > SHDIFF_EMPTY)
            {
                res = 1;
                lua_pop(lstate, 1);
                //lstate | light; shtable_table_events; table_events_lights;
            }
            else
            {
                lua_pop(lstate, 1);
            }
        }
        else
        {
            lua_pop(lstate, 1);
        }
        //lstate | light; shtable_table_events; table_events_lights;
    }
    lua_pop(lstate, 3);
    //lstate | empty;
    sem_post(lsem);
    return res;
}

//In
// L | light
//Out
//L | empty
int check_events_in_shared_events(lua_State* L, sem_t* lsem, lua_State* lstate)
{
    int res = 0;

    SEM_WAIT(lsem);
    xcopy1(L, lstate, -1);
    lua_pop(L, 1);
    // L | empty
    //lstate | light
    lua_getglobal(lstate, shtable_events);
    //lstate | light; shtable_events;
    lua_pushnil(lstate); /* first key */
    while (lua_next(lstate, -2) != 0)
    {
        //lstate | light; shtable_events;key(pid); value(table_events_lights)
        lua_pushvalue(lstate, -4);
        //lstate | light; shtable_events;key(pid); value(table_events_lights);light;
        lua_gettable(lstate, -2);
        //lstate | light; shtable_events; key(pid); value(table_events_lights);event_state
        if (lua_type(lstate, -1) == LUA_TNUMBER)
        {
            res++;
        }
        lua_pop(lstate, 2);
    }
    //lstate | light; shtable_events;
    lua_pop(lstate, 2);
    //lstate | empty;
    sem_post(lsem);
    return res;
}
