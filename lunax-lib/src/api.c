/*
**  api.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/
#include <pid_table.h>
#include "lua.h"
#include "lunax.h"
#include "mem_main.h"
#include "proxy_table.h"
#include "shared_table.h"
#include "select_table.h"
#include "events_table.h"
#include "deepcopy.h"
#include "events_table.h"
#include "task_timer.h"

#include "app.h"
#include <pthread.h>
#include "ls.h"

extern int EXIT;
//In
// L  safe ; param1 ; param2
//Out
//L | status
int copy_value(lua_State* L)
{
    int res;
    res = copy_value_by_proxy_table(L);
    lua_pushinteger(L, res);
    return 1;
}
//In
// L | $shared_table_name;
// Out
// L | proxy table; res;
int create_table(lua_State* L)
{
    int res = E_SHTABLE_ALREADY_EXIST;
    pid_t pid;
    const char* name;
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        //luaL_error(L,"Error: BAD_ARG\n");
        lua_pop(L, 1);
        lua_pushnil(L);
        lua_pushnumber(L, E_BAD_ARG);
        return 2;
    }
    name = lua_tostring(L, -1);
    lua_pop(L, 1);
    pid = gettid();

    if (!is_table(L, pid, name))
    {
        create_select_table(L);
        create_share_table(L, pid, name);
        res = E_OK;
    }
    // L | luserdata_lstate; luserdata_lsem; light
    get_proxy(L, LUNAX_CAST(lua_touserdata(L, -3)),
            LUNAX_CAST(lua_touserdata(L, -2)), pid, name);
    //L | luserdata_lstate; luserdata_lsem; proxy_table;
    lua_remove(L, -2);
    // L | luserdata_lstate; proxy_table
    lua_remove(L, -2);
    // L |  proxy_table
    lua_pushnumber(L, res);
    //L | proxy table; res;
    return 2;
}

//In
//L | table_name
//Out
//L | res
int delete_table(lua_State* L)
{
    pid_t pid;
    const char* name;
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        lua_pop(L, 1);
        lua_pushnumber(L, E_BAD_ARG);
        return 1;
    }
    name = lua_tostring(L, -1);
    lua_pop(L, 1);
    pid = gettid();
    if (!is_table(L, pid, name))
    {
        lua_pushnumber(L, E_BAD_ARG);
        return 1;
    }
    // L | luserdata_lstate; luserdata_lsem; light
    lua_remove(L, -2);
    lua_remove(L, -2);
    // L | light
   lua_pop(L,1); //serge_p ... del_proxy(L);
    // L | empty
    delete_from_main_table(L, pid, name);
    lua_pushnumber(L, E_OK);
    return 1;
}

//In
// L | pid_name | table_name
//Out
//L | proxy_table
int attach_inner(lua_State* L)
{
    int err;
    const char* name;
    int pid;
    lua_State* lstate;
    sem_t* lsem;

    name = lua_tostring(L, -1);
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        luaL_error(L, "Error: BAD_ARG\n");
        return 1;
    }
    lua_pop(L, 1);
    // L | pid_name;
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    xcopy1(L, LMAIN, lua_gettop(L));
    // L | pid_name;
    // LMAIN | pid_name;
    lua_pop(L, 1);

    err = find_pid_table(LMAIN);
    if (err)
    {
        //L | empty
        //LMAIN | empty
        sem_post(semMAIN);
        lua_pushnil(L);
        return 1;
    }
    // LMAIN | pid_table; pid_number;
    pid = lua_tonumber(LMAIN, -1);
    lua_pop(LMAIN, 1);
    // LMAIN | pid_table;
    lua_getfield(LMAIN, -1, name);
    if (lua_type(LMAIN, -1) != LUA_TTABLE)
    {
        //L | empty
        // LMAIN | pid_table; nil
        lua_pop(LMAIN, 2);
        sem_post(semMAIN);
        lua_pushnil(L);
        return 1;
    }
    // LMAIN | pid_table; shtable

    lua_remove(LMAIN, -2);
    lua_pushvalue(LMAIN, -1);
    lua_pushvalue(LMAIN, -1);
    // LMAIN | shtable; shtable; shtable
    lstate = get_lstate(LMAIN);
    lsem = get_lsem(LMAIN);
    // LMAIN | shtable;
    lua_pushinteger(LMAIN, gettid());
    lua_gettable(LMAIN, -2);
    // LMAIN | shtable;value
    if (lua_type(LMAIN, -1) != LUA_TBOOLEAN)
    {
        // LMAIN | shtable; value
        create_select_table(L);
        lua_pushinteger(LMAIN, gettid());
        lua_pushboolean(LMAIN, 1);
        lua_settable(LMAIN, -4);
        // LMAIN | shtable; value
    }
    lua_pop(LMAIN, 2);
    // LMAIN | empty
    sem_post(semMAIN);
    SEM_WAIT(lsem);

    lua_getglobal(lstate, shtable);

    table_to_light(lstate, L);
    sem_post(lsem);
    //L | light
    //lstate | empty
    get_proxy(L, lstate, lsem, pid, name);

    //L |  proxy_table;
    return 1;
}

//In
// L | pid_name ; table_name;  ms; sec; timeout
//Out
//L | proxy_table
static int attach_wait_co(lua_State *L, int status, lua_KContext ctx)
{
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,5);
            lua_pushnil(L);
            return 1;
        }
    }

    // L | pid_name ; table_name;  ms; sec; timeout
    lua_pushvalue(L, -5);
    lua_pushvalue(L, -5);
    // L | pid_name ; table_name; ms; sec; timeout; pid_name ; table_name
    attach_inner(L);
    // L | pid_name ; table_name ;  ms; sec; timeout; value(proxy_table/nil)
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_pop(L, 1);
        // L | pid_name ; table_name;  ms; sec; timeout
        return lua_yieldk(L, 5, 0, &attach_wait_co);
    }
    else
    {
        lua_remove(L, -2);
        lua_remove(L, -2);
        lua_remove(L, -2);
        lua_remove(L, -2);
        lua_remove(L, -2);
    }
    // L | value(proxy_table)
    return 1;
}

//In
// L | OPTIONAL(attr); pid_name ; table_name
//Out
//L | proxy_table
int attach(lua_State* L)
{
    long int sec = 0;
    long int ms = 0;
    long int timeout = 0;

    int attr = ATTR_NONE;
    int cnt = lua_gettop(L);
    if (cnt == 3)
    {
        if (lua_type(L,-3) == LUA_TNUMBER)
        {
            attr = lua_tointeger(L, -3);
            lua_remove(L, -3);
        }
        else
        {
            // L | attr_table;  pid_name ; table_name;
            lua_pushinteger(L,0);
            lua_gettable(L,-4);
            // L | attr_table;  pid_name ; table_name attr
            attr = lua_tointeger(L, -1);
            lua_pop(L,1);
            // L | attr_table;  pid_name ; table_name
            lua_pushinteger(L,1);
            lua_gettable(L,-4);
            timeout = lua_tointeger(L, -1);
            get_ms(&sec, &ms);
            lua_pop(L,1);
            lua_remove(L,-3);

        }
    }
    // L | pid_name ; table_name
    switch (attr)
    {
    case ATTR_NONE:
        return attach_inner(L);
        break;
    case ATTR_CHECK:
        return attach_inner(L);
        break;
    case ATTR_WAIT:
        if (lua_pushthread(L) == 1)
        {
            lua_pop(L, 2);
            luaL_error(L, "Error: ATTR: Wait can be used in Task only. \n");
            return 0;
        }
        else
        {
            lua_pop(L, 1);
            lua_pushinteger(L,ms);
            lua_pushinteger(L,sec);
            lua_pushinteger(L,timeout);
            // L | pid_name ; table_name;  ms; sec; timeout
            return attach_wait_co(L, 0, 0);
        }
        break;
    default:
        break;
    }
    luaL_error(L, "Error: Incorrect attribute\n");
    return 0;
}

//In
// L | proxy_table
//Out
//L | empty
int detach(lua_State* L)
{
    lua_State* lstate;
    lua_State* LMAIN;
    sem_t* semMAIN;

    if (lua_type(L, -1) != LUA_TTABLE)
    {
        luaL_error(L, "Error: BAD_ARG\n");
        return 1;
    }
    lua_getmetatable(L,-1);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        luaL_error(L, "Error: BAD_ARG\n");
        return 1;
    }
    // L | proxy_table; metatable
    lua_getfield(L,-1,"state");
    // L | proxy_table; metatable; state;
    lstate = lua_touserdata(L,-1);
    lua_pop(L,3);
    LMAIN = get_main_table();
    semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_getglobal (LMAIN,voodoo);
    lua_pushlightuserdata(LMAIN,lstate);
    lua_gettable(LMAIN,-2);
    // LMAIN | vooodoo; sh_table;
     lua_pushvalue(LMAIN,-1);
     dbg ("DETACH for lstate:%p",lstate);
     //LMAIN | voodoo ; sh_table ; sh_table;
     clear_tid_in_sh_table(LMAIN);
     //LMAIN | voodoo ; sh_table ;
     try_delete_sh_table(LMAIN);
     //LMAIN | voodoo ;
     lua_pop(LMAIN,1);
     sem_post(semMAIN);
     null_for_proxy_tables(L, lstate);
     // LMAIN | empty
     return 0;
}
//In
//L | string
//Out
//L | empty
int set_alias(lua_State* L)
{
    const char* name;
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        luaL_error(L, "Error: BAD_ARG\n");
        return 0;
    }
    name = lua_tostring(L, -1);
    lua_pop(L, 1);
    //L | empty
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_pushstring(LMAIN, name);
    if (find_pid_table(LMAIN))
    {
        //LMAIN | empty
        set_alias_pid_table(LMAIN, name);
    }
    else
    {
        // LMAIN | pid_table; pid_number;
        lua_pop(L,2);
        luaL_error(L, "Error: PID name is already existed\n");
    }
    sem_post(semMAIN);

    return 0;
}
//In
//L | empty
//Out
//L | string
int get_alias(lua_State* L)
{
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_getglobal(LMAIN, mtable);
    lua_pushnumber(LMAIN, gettid());
    lua_gettable(LMAIN, -2);
    //LMAIN | mtable; pid_table
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        lua_getfield(LMAIN, -1, "name");
        //LMAIN | mtable; pid_table ; name
    }
    else
    {
        //LMAIN | mtable; pid_table; nil
        lua_pop(LMAIN, 3);
        luaL_error(L, "Error: Unknown table %d\n", gettid());
        return 0;
    }
    xcopy1(LMAIN, L, lua_gettop(LMAIN));
    lua_pop(LMAIN, 3);

    sem_post(semMAIN);
    return 1;
}

//In
// L  optional(int);
//Out
//L | empty
int fetch(lua_State* L)
{
    int iter = 0;
    int citer = 0;
    int tid;
    POINTER_INT* thrs;
    int cnt = 0;

    if (lua_gettop(L))
    {
        if (lua_type(L,-1) == LUA_TNUMBER)
        {
            iter = lua_tointeger(L,-1);
        }
        lua_pop(L,1);
    }
    while (1)
    {

        fetch_by_events_table(L);
        fetch_by_threads_table(L);
        fetch_by_timers_table(L);
        pthread_testcancel();
        if (iter != 0)
        {
            if (citer == iter) EXIT =1 ;
            citer++;
        }

        if (EXIT)
        {
            dbg("========exit: %d=========>",EXIT);
            thrs = lunax_threads_join();
            if (thrs)
            {
                while (thrs[cnt])
                {
                    pthread_join(thrs[cnt], NULL);
                    cnt++;
                }
                free(thrs);
            }
            lua_getglobal(L,"ATID");
            tid = lua_tointeger(L,-1);
            lua_pop(L,1);
            delete_pid_table(tid);
            delete_pid2thr_tables(tid);

            //exit(EXIT_SUCCESS);
            break;
        }
    }
    return 0;
}

//In
// L  empty;
//Out
//L | empty
int tfetch(lua_State* L)
{
    int res;
    POINTER_INT* thrs;
    int cnt = 0;

    while (1)
    {
        res = 0;
        res += fetch_by_events_table(L);
        res += fetch_by_threads_table(L);
        res += fetch_by_timers_table(L);
        pthread_testcancel();
        if (EXIT)
        {
            dbg("========exit: %d=========>",EXIT);
            thrs = lunax_threads_join();
            if (thrs)
            {
                while (thrs[cnt])
                {
                    pthread_join(thrs[cnt], NULL);
                    cnt++;
                }
                free(thrs);
            }
            //exit(EXIT_SUCCESS);
            break;
        }
        if (res == 3)
        {
            thrs = lunax_threads_join();
            if (thrs)
            {
                while (thrs[cnt])
                {
                    pthread_join(thrs[cnt], NULL);
                    cnt++;
                }
                free(thrs);
            }
            break;
        }
    }
    return res;
}

//In
// L  empty;
//Out
//L | empty
int dfetch(lua_State* L)
{
    int iter = 0;
    int citer = 0;
    int tid;
    POINTER_INT* thrs;
    int cnt = 0;

    if (lua_gettop(L))
    {
        if (lua_type(L,-1) == LUA_TNUMBER)
        {
            iter = lua_tointeger(L,-1);
        }
        lua_pop(L,1);
    }
    while (1)
    {
        fetch_by_events_table(L);
        fetch_by_threads_table(L);
        fetch_by_timers_table(L);
        debug_monitor(L);
        pthread_testcancel();
        if (iter != 0)
        {
            if (citer == iter) EXIT =1 ;
            citer++;
        }
        if (EXIT)
        {
            dbg("========exit: %d=========>\n",EXIT);
            thrs = lunax_threads_join();
            if (thrs)
            {
                while (thrs[cnt])
                {
                    pthread_join(thrs[cnt], NULL);
                    cnt++;
                }
                free(thrs);
            }
            lua_getglobal(L,"ATID");
            tid = lua_tointeger(L,-1);
            lua_pop(L,1);
            delete_pid_table(tid);
            delete_pid2thr_tables(tid);

            //exit(EXIT_SUCCESS);
            break;
        }
    }
    return 0;
}
//In
// L  function; params ....
//Out
//L | task handler
int add_task(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt == 0) || (lua_type(L, 1) != LUA_TFUNCTION))
    {
        luaL_error(L, "Error: Absent function. \n");
        return 0;
    }
    return add_task_in_proccess(L);
}

//In
// L  task handler
//Out
//L | empty
int del_task(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt != 1) || (lua_type(L, cnt) != LUA_TNUMBER))
    {
        luaL_error(L, "Error: Task handler is wrong. \n");
        return 0;
    }
    return del_task_in_proccess(L);
}

//In
// L | delay;
//Out
//L | empty
static int wait_timer_one_shot_co(lua_State *L, int status, lua_KContext ctx)
{
    long int delay;
    // L | delay;

    delay = lua_tointeger(L, -1);
    if (delay == 0)
    {
        // L | delay;
        lua_pop(L, 1);
        lua_pushinteger(L, 1);
        return lua_yieldk(L, 1, 0, &wait_timer_one_shot_co);
    }
    else
    {
        lua_pop(L, 1);
    }
    // L | empty
    return 0;
}
//In
// L | delay; start_sc; start_ms;
//Out
//L | empty
static int wait_timer_co(lua_State *L, int status, lua_KContext ctx)
{
    long int sec;
    long int ms;

    long int start_sec;
    long int start_ms;
    long int delay;

    get_ms(&sec, &ms);

    // L | delay; start_sc; start_ms

    delay = lua_tointeger(L, -3);
    start_sec = lua_tointeger(L, -2);
    start_ms = lua_tointeger(L, -1);

    ms = (sec - start_sec) * 1000 + (ms - start_ms);

    if (ms < delay)
    {
        // L | delay;  start_sc; start_ms
        return lua_yieldk(L, 3, 0, &wait_timer_co);
    }
    else
    {
        lua_pop(L, 3);
    }
    // L | empty
    return 0;
}
//In
// L  delay;
//Out
//L | empty
int wait_timer(lua_State* L)
{
    long int sec;
    long int ms;

    if (lua_tointeger(L,-1) != 0)
    {
        get_ms(&sec, &ms);
        lua_pushinteger(L, sec);
        lua_pushinteger(L, ms);
        // L  delay; start_sc; start_ms
        return wait_timer_co(L, 0, 0);
    }
    else
    {
        // L  delay;
        return wait_timer_one_shot_co(L, 0, 0);
    }
}

//In
// L | proxy_table; ehid; ms; sec; timeout;
//Out
//L | wait_status
static int wait_update_table_co(lua_State *L, int status, lua_KContext ctx)
{
    int res;
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,5);
            lua_pushinteger(L, W_TIMEOUT);
            return 1;
        }
    }

    lua_pushvalue(L, -5);
    lua_pushvalue(L, -5);
    // L | proxy_table; ehid;ms; sec; timeout; proxy_table; ehid;
    res = check_update_table_by_events_table(L);

    if (res == 0)
    {
        // L | proxy_table; ehid; ms; sec; timeout;
        return lua_yieldk(L, 5, 0, &wait_update_table_co);
    }
    // L | proxy_table; ehid; ms; sec; timeout;
    lua_pop(L,3);
    // L |  proxy_table; ehid;
    del_handler(L);

    lua_pushinteger(L, W_SET);
    return 1;
}

//In
// L | nonexist_table; ms; sec; timeout
//Out
//L | proxy_table
static int wait_update_table_wait_proxy_co(lua_State *L, int status, lua_KContext ctx)
{
    unsigned int ehid;
    int st;
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,4);
            lua_pushinteger(L, W_TIMEOUT);
            return 1;
        }
    }

    set_read_check_attr(L);

    // L | nonexist_table ; ms; sec; timeout;
    lua_pushvalue(L,-4);
    // L | nonexist_table ; ms; sec; timeout; nonexist_table
    st = resolve_nonexist_table(L);
    if (st == 0)
    {
        // L | nonexist_table ; ms; sec; timeout; nonexist_table ;
        lua_pop(L,1);
        return lua_yieldk(L, 4, 0, &wait_update_table_wait_proxy_co);
    }
    // L | nonexist_table ; ms; sec; timeout; proxy_table;
    lua_pushvalue(L, -1);
    lua_pushinteger(L, 0);
    // L |  nonexist_table ; ms; sec; timeout; proxy_table; proxy_table; 0
    ehid = add_handler(L);
    lua_pushinteger(L, ehid);
    // L |  nonexist_table ; ms; sec; timeout; proxy_table; ehid
    lua_remove (L,-6);
    // L |  ms; sec; timeout; proxy_table; ehid
    lua_insert(L,-5);
    // L |  ehid; ms; sec; timeout; proxy_table;
    lua_insert(L,-5);
    // L |  proxy_table; ehid; ms; sec; timeout;
    return wait_update_table_co(L, 0, 0);
}

//In
// L | OPTIONAL(attr); proxy_table;
//Out
//L | status
int is_update_table(lua_State* L)
{
    unsigned int ehid;

    int status;
    int attr = ATTR_NONE;
    long int sec = 0;
    long int ms = 0;
    long int timeout = 0;

    int cnt = lua_gettop(L);
    if (cnt == 2)
    {
        if (lua_type(L, -2) == LUA_TNUMBER)
        {
            attr = lua_tointeger(L, -2);
            lua_remove(L, -2);
        }
        else
        {
            // L | attr_table; proxy_table;
            lua_pushinteger(L, 0);
            lua_gettable(L, -3);
            // L | attr_table; proxy_table; attr
            attr = lua_tointeger(L, -1);
            lua_pop(L, 1);
            // L | attr_table; proxy_table;;
            lua_pushinteger(L, 1);
            lua_gettable(L, -3);
            timeout = lua_tointeger(L, -1);
            get_ms(&sec, &ms);
            lua_pop(L, 1);
            lua_remove(L, -2);
        }
    }

    switch (attr)
    {
    case ATTR_NONE:
        lua_pop(L, 1);
        lua_pushinteger(L, W_UNSET);
        break;
    case ATTR_CHECK:
        lua_pop(L, 1);
        lua_pushinteger(L, W_UNSET);
        break;
    case ATTR_WAIT:

        // L |  proxy_table;
        if (lua_type(L, -1) != LUA_TTABLE)
        {
            lua_pop(L, 1);
            luaL_error(L, "Error: Incorrect type. \n");
            return 0;
        }
        // L | proxy_table;
        if (lua_pushthread(L) == 1)
        {
            lua_pop(L, 1);
            luaL_error(L, "Error: Wait function can be used in Task only. \n");
            return 0;
        }
        else
        {
            // L | proxy_table; thread
            lua_pop(L, 1);
            // L | proxy_table;
            status = is_exist_table(L, -1);
            if (status == 0)
            {
                // L | nonexist_table;
                lua_pushinteger(L, ms);
                lua_pushinteger(L, sec);
                lua_pushinteger(L, timeout);
                // L | nonexist_table; ms; sec; timeout
                return wait_update_table_wait_proxy_co(L, 0, 0);
            }
            // L | proxy_table;
            lua_pushvalue(L, -1);
            lua_pushinteger(L, 0);
            // L |  proxy_table; proxy_table; 0
            ehid = add_handler(L);
            lua_pushinteger(L, ehid);
            // L |  proxy_table; ehid
            lua_pushinteger(L, ms);
            lua_pushinteger(L, sec);
            lua_pushinteger(L, timeout);
            // L |  proxy_table; ehid; ms; sec; timeout
            return wait_update_table_co(L, 0, 0);
        }
        break;
    default:
        break;
    }
    return 1;
}

//In
// L  delay, function; params ....
//Out
//L | timer_handler
int lunax_timer_add(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt < 2) || (lua_type(L, 1) != LUA_TNUMBER)
            || (lua_type(L, 2) != LUA_TFUNCTION))
    {
        luaL_error(L, "Error: Incorrect parameters. \n");
        return 0;
    }
    return add_timer_in_proccess(L);
}

//In
// L  timer_handler;
//Out
//L | empty
int lunax_timer_del(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt < 1) || (lua_type(L, 1) != LUA_TNUMBER))
    {
        luaL_error(L, "Error: Incorrect parameters. \n");
        return 0;
    }
    return del_timer_in_proccess(L);
}

//In
// L  timer_handler;
//Out
//L | empty
int lunax_timer_start(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt < 1) || (lua_type(L, 1) != LUA_TNUMBER))
    {
        luaL_error(L, "Error: Incorrect parameters. \n");
        return 0;
    }
    return start_timer_in_proccess(L);
}

//In
// L  timer_handler;
//Out
//L | empty
int lunax_timer_stop(lua_State* L)
{
    int cnt;

    cnt = lua_gettop(L);
    if ((cnt < 1) || (lua_type(L, 1) != LUA_TNUMBER))
    {
        luaL_error(L, "Error: Incorrect parameters. \n");
        return 0;
    }
    return stop_timer_in_proccess(L);
}

int dbg_show_shtable(lua_State* L)
{
    dbg_show_shared_table(L);
    return 0;
}

