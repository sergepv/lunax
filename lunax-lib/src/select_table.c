/*
** select_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "select_table.h"
#include "proxy_table.h"
#include "shared_table.h"
#include "pid_table.h"

static char SELECT;

//In
// any
//Out
// any
int create_select_table(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_newtable(L);
        {
            lua_newtable(L);
            {
                lua_pushstring(L, "v");
                lua_setfield(L, -2, "__mode");
            }
            lua_setmetatable(L, -2);
        }
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    }
    lua_pop(L, 1);
    return 0;
}

//In
// L |  light
//Out
//L | proxy table
int get_proxy(lua_State* L, lua_State* lstate, sem_t* lsem, pid_t pid,
        const char* name)
{
    int res = 0;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    lua_pushvalue(L, -2);
    // L |  light; select_table;light
    lua_gettable(L, -2);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        // L |  light; select_table; nil
        lua_pop(L, 1);
        lua_pushvalue(L, -2);
        // L |  light; select_table; light;
        create_proxy_table(L, lstate, lsem, pid, name);
        // L |  light; select_table;light;proxy_table;
        lua_pushvalue(L, -1);
        // L |  light; select_table;light;proxy_table;proxy_table;
        lua_insert(L, -3);
        // L |  light; select_table;proxy_table;light;proxy_table;
        lua_settable(L, -4);
        // L |  light; select_table;proxy_table;
        find_and_inc_tid_in_sh_table(lstate);
    }
    // L |  light; select_table;proxy_table;
    lua_remove(L, -2);
    lua_remove(L, -2);
    // L |  proxy_table;
    return res;
}

//In
// L |  light
//Out
//L |  empty
int del_proxy(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    lua_pushvalue(L, -2);
    // L | light; select; light
    lua_pushnil(L);
    // L |  light; select; light; nil
    lua_settable(L, -3);
    // L |  light; select;
    lua_pop(L, 2);
    return 0;
}

//In
// L  empty
//Out
//L | empty

int null_for_proxy_tables(lua_State* L, lua_State* lstate)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    //L | select;
    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        //L | select; key(light); value(proxy_table)
        lua_getmetatable(L,-1);
        //L | select; key(light); value(proxy_table); metatable;
        lua_getfield(L,-1,"state");
        //L | select; key(light); value(proxy_table); metatable; lstate;
        if (LUNAX_RE_CAST(lstate) == lua_touserdata(L,-1))
        {
            dbg("shtable erase in proxy table (lstate: %p)\n",lua_touserdata(L,-1));
            lua_pop(L,1);
            //L | select; key(light); value(proxy_table); metatable;
            lua_pushnil(L);
            lua_setfield(L,-2,"shtable");
            //L | select; key(light); value(proxy_table); metatable;
            lua_pop(L,2);
        }
        else
        {
            lua_pop(L,3);
        }
        //L | select; key(light);
    }
    lua_pop(L,1);
    return 0;
}

//In
// L  empty
//Out
//L | empty

int null_for_proxy_table(lua_State* L, lua_State* lstate, uint32_t light)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SELECT);
    //L | select;
    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        //L | select; key(light); value(proxy_table)
        lua_getmetatable(L,-1);
        //L | select; key(light); value(proxy_table); metatable;
        lua_getfield(L,-1,"shtable");
        //L | select; key(light); value(proxy_table); metatable; light;
        if (light == lua_tointeger(L,-1))
        {
            lua_pop(L,1);
            //L | select; key(light); value(proxy_table); metatable;
            lua_pushnil(L);
            lua_setfield(L,-2,"shtable");
            //L | select; key(light); value(proxy_table); metatable;
            lua_pop(L,3);
            break;
        }
        else
        {
            lua_pop(L,3);
        }
        //L | select; key(light);
    }
    lua_pop(L,1);
    return 0;
}

