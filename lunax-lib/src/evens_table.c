/*
** evens_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "events_table.h"
#include "proxy_table.h"
#include "shared_table.h"
#include "shared_events_table.h"
#include "task_timer.h"

static char EVENTS;

//In
// L| handle_table;
//Out
// L| handle_table;
static int calc_new_ehid (lua_State* L)
{
    unsigned int ehid = 0;
    lua_pushinteger(L,0xFFFFFFFF);
    lua_gettable(L,-2);
    // L| handle_table; max_value;
    do
    {
        ehid = (unsigned int)lua_tointeger(L,-1)+1;
        if (ehid == 0xFFFFFFFF) ehid = 1;
        lua_pop(L,1);
        lua_pushinteger(L,ehid);
        // L| handle_table;  ehid
        lua_gettable(L,-2);
    }
    while (lua_type(L,-1) != LUA_TNIL);
    // L| handle_table;  ehid
    lua_pushinteger(L,0xFFFFFFFF);
    lua_pushvalue(L,-2);
    // L| handle_table;  ehid; 0xFFFFFFFF; ehid;
    lua_settable(L,-4);
    // L| handle_table;  ehid;
    lua_pop(L,1);
    return ehid;
}

//In
// any
//Out
// any
int create_events_table(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_newtable(L);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    }
    else
    {
        lua_pop(L, 1);
    }
    return 0;
}

//In
// L |  proxy_table, handler
//Out
//L | empty
unsigned int add_handler(lua_State* L)
{
    lua_State* lstate;
    sem_t* lsem;
    unsigned int ehid;

    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    // L |  proxy_table; handler; events_table;
    lua_pushvalue(L, -3);
    // L |  proxy_table; handler; events_table;proxy_table;
    lua_gettable(L,-2);
    if (lua_type(L,-1) == LUA_TNIL)
    {
        ehid = 1;
        // L |  proxy_table; handler; events_table;nil
        lua_pop(L,1);
        lua_pushvalue(L, -3);
        // L |  proxy_table; handler; events_table;proxy_table;
        lua_newtable(L);
        {
            // L |  proxy_table; handler; events_table;proxy_table; new handle_table
            if (lua_type(L,-4) == LUA_TFUNCTION)
            {
                lua_pushinteger(L,ehid);
                // L |  proxy_table; handler; events_table;proxy_table; new handle_table;1
                lua_pushvalue(L, -5);
                lua_settable(L, -3);
            }
            else
            {
                lua_pushinteger(L,ehid);
                lua_pushinteger(L,SHDIFF_EMPTY);
                lua_settable(L, -3);
            }
            // L |  proxy_table; handler; events_table;proxy_table; new handle_table;
            lua_pushinteger(L,0xFFFFFFFF);
            lua_pushinteger(L,ehid);
            lua_settable(L, -3);
        }
        // L |  proxy_table; handler; events_table;proxy_table; new handle_table;
        lua_settable(L, -3);

        // L |  proxy_table; handler; events_table;
        lua_pop(L, 2);
        // L |  proxy_table;
        lua_getmetatable(L, -1);
        // L |  proxy_table; meta_proxy_table
        lua_getfield(L, -1, "sem");
        lsem = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -1, "state");
        lstate = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -1, "shtable");
        // L |  proxy_table; meta_proxy_table; light
        reg_event_in_shared_events(L, lsem, lstate, gettid());
        // L |  proxy_table; meta_proxy_table;
        lua_pop(L, 2);
    }
    else
    {
        // L |  proxy_table; handler; events_table; handle_table
        ehid = calc_new_ehid(L);
        if (lua_type(L,-3) == LUA_TFUNCTION)
        {
            lua_pushinteger(L, ehid);
            // L |  proxy_table; handler; events_table;proxy_table; handle_table;ehid
            lua_pushvalue(L, -5);
            lua_settable(L, -3);
        }
        else
        {
            lua_pushinteger(L,ehid);
            lua_pushinteger(L,SHDIFF_EMPTY);
            lua_settable(L, -3);
        }
        // L |  proxy_table; handler; events_table;proxy_table;handle_table;
        lua_pushinteger(L,0xFFFFFFFF);
        lua_pushinteger(L,ehid);
        lua_settable(L, -3);

        lua_pop(L,5);
    }

    return ehid;
}

//In
// L |  proxy_table; optional(ehid)
//Out
//L | empty
int del_handler(lua_State* L)
{
    lua_State* lstate;
    sem_t* lsem;
    unsigned int ehid = 0;
    int count =0;

    if (lua_gettop(L) == 2)
    {
        ehid = lua_tointeger(L,-1);
        lua_pop(L,1);
    }
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    // L |  proxy_table; events_table;
    lua_pushvalue(L, -2);
    // L |  proxy_table; events_table; proxy_table;
    lua_pushvalue(L, -1);
    // L |  proxy_table; events_table; proxy_table; proxy_table;
    if (ehid)
    {
        lua_gettable(L,-3);
        if (lua_type(L,-1) == LUA_TTABLE)
        {
            // L |  proxy_table; events_table; proxy_table; handle_table;
            lua_pushinteger(L,ehid);
            lua_pushnil(L);
            lua_settable(L, -3);
            lua_pushnil(L);
            while (lua_next(L,-2)!=0)
            {
                // L |  proxy_table; events_table; proxy_table; handle_table; key; value
                if ((unsigned int)lua_tointeger(L,-2) != 0xFFFFFFFF)
                {
                    count++;
                }
                lua_pop(L,1);
            }
            lua_pop(L,1);
        }
        else
        {
            lua_pop(L,1);
        }
    }
    else
    {
        lua_gettable(L,-3);
        if (lua_type(L,-1) == LUA_TTABLE)
        {
            // L |  proxy_table; events_table; proxy_table; handle_table;
            lua_pushnil(L);
            while (lua_next(L,-2)!=0)
            {
                // L |  proxy_table; events_table; proxy_table; handle_table; key(ehid); value(function/event_state);
                if (lua_type(L,-1) == LUA_TFUNCTION)
                {
                    lua_pushvalue(L,-2);
                    lua_pushnil(L);
                    // L |  proxy_table; events_table; proxy_table; handle_table; key(ehid); value(function/event_state);key(ehid); nil;

                    lua_settable(L, -5);
                }
                else
                {
                    if ((unsigned int)lua_tointeger(L,-2) != 0xFFFFFFFF)
                    {
                        count++;
                    }
                }
                lua_pop(L,1);

            }
            lua_pop(L,1);
            // L |  proxy_table; events_table; proxy_table;
        }
    }
    // L |  proxy_table; events_table; proxy_table;
    if (count == 0)
    {
        lua_pushnil(L);
        // L |  proxy_table; events_table; proxy_table;nil
        lua_settable(L, -3);
        // L |  proxy_table; events_table;
        lua_pop(L, 1);
        // L |  proxy_table;
        lua_getmetatable(L, -1);
        // L |  proxy_table; meta_proxy_table
        lua_getfield(L, -1, "sem");
        lsem = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -1, "state");
        lstate = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -1, "shtable");
        // L |  proxy_table; meta_proxy_table; light
        unreg_event_in_shared_events(L, lsem, lstate, gettid());
        // L |  proxy_table; meta_proxy_table;
        lua_pop(L, 2);
    }
    else
    {
        lua_pop(L,3);
    }
    return 0;
}

//In
// L  proxy_table
//Out
//L | empty
int clear_event_state_by_events_table(lua_State* L)
{
    int res;
    lua_State* lstate;
    sem_t* lsem;

    // L |  proxy_table;
    lua_getmetatable(L, -1);
    // L |  proxy_table; meta_proxy_table
    lua_getfield(L, -1, "sem");
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "state");
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "shtable");
    // L |  proxy_table; meta_proxy_table; light
    res = clear_event_state_in_shared_events(L, lsem, lstate, gettid());
    // L |  proxy_table; meta_proxy_table;
    lua_pop(L, 2);
    return res;
}

//In
// L  proxy_table
//Out
//L | empty
int check_event_state_by_events_table(lua_State* L)
{
    int res;
    lua_State* lstate;
    sem_t* lsem;

    // L |  proxy_table;
    lua_getmetatable(L, -1);
    // L |  proxy_table; meta_proxy_table
    lua_getfield(L, -1, "sem");
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "state");
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "shtable");
    // L |  proxy_table; meta_proxy_table; light
    res = check_event_state_in_shared_events(L, lsem, lstate, gettid());
    // L |  proxy_table; meta_proxy_table;
    lua_pop(L, 2);
    return res;
}

//In
// L  proxy_table
//Out
//L | empty
int check_events_by_events_table(lua_State* L)
{
    int res;
    lua_State* lstate;
    sem_t* lsem;

    // L |  proxy_table;
    lua_getmetatable(L, -1);
    // L |  proxy_table; meta_proxy_table
    lua_getfield(L, -1, "sem");
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "state");
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "shtable");
    // L |  proxy_table; meta_proxy_table; light
    res = check_events_in_shared_events(L, lsem, lstate);
    // L |  proxy_table; meta_proxy_table;
    lua_pop(L, 2);
    return res;
}

//In
// L  proxy_table ; ehid
//Out
//L | empty
int check_update_table_by_events_table(lua_State* L)
{
    int res = 0;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    // L |   proxy_table ; ehid; events_table;
    lua_pushvalue(L,-3);
    // L |   proxy_table ; ehid; events_table; proxy_table
    lua_gettable(L,-2);
    // L |   proxy_table ; ehid; events_table; handle_table
    lua_pushvalue(L,-3);
    lua_gettable(L,-2);
    // L |   proxy_table ; ehid; events_table; handle_table; state_event
    if (lua_tointeger(L,-1) == SHDIFF_EXIST)
    {
        res = 1;
    }
    lua_pop(L,5);

    return res;
}

//In
// L  empty
//Out
//L | empty
int fetch_by_events_table(lua_State* L)
{
    int result = 0;
    int run = 0;
    int res;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &EVENTS);
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        result = 1;
        //L | events_table
        lua_pushnil(L);
        while (lua_next(L, -2) != 0)
        {
            result = 0;
            //L | events_table; key(proxy_table); value(handler_table)
            lua_pushvalue(L, -2);
            //L | events_table; key(proxy_table); value(handler_table); key(proxy_table)
            run = check_event_state_by_events_table(L);
            //L | events_table; key(proxy_table); value(handler_table);
            if (run)
            {
                //L | events_table; key(proxy_table); value(handler_table);
                lua_pushnil(L);
                while (lua_next(L,-2)!=0)
                {
                    //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function/event_state)
                    if (lua_type(L,-1) != LUA_TFUNCTION )
                    {
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(event_state)
                        lua_pop(L,1);
                        if (lua_tointeger(L,-1) != 0xFFFFFFFF)
                        {
                            lua_pushvalue(L, -1);
                            //L | events_table; key(proxy_table); value(handler_table); key(ehid); key(ehid);
                            lua_pushinteger(L, SHDIFF_EXIST);
                            lua_settable(L, -4);
                            //L | events_table; key(proxy_table); value(handler_table); key(ehid);
                        }
                    }
                    else
                    {
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function)
                        lua_State* L1 = lua_newthread(L);
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                        lua_pushvalue(L, -2);
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;value(function);
                        lua_pushvalue(L, -6);
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;value(function);key(proxy_table);
                        lua_xmove(L, L1, 2);
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                        lua_pushvalue(L, -5);
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;key(proxy_table);
                        if (check_task_in_threads_table(L) == 0)
                        {
                            //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                            //L1 | function(value); key(proxy_table);
                            res = lua_resume(L1, L, 1);
                            if (res == LUA_YIELD)
                            {
                                //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                                lua_pushvalue(L, -5);
                                //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;key(proxy_table);
                                add_event_task_in_threads_table(L);
                                lua_pop(L, 1);
                            }
                            else
                            {
                                //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                                lua_pushvalue(L,-5);
                                clear_event_state_by_events_table(L);
                                lua_pop(L, 2);
                            }
                        }
                        else
                        {
                            //L | events_table; key(proxy_table); value(handler_table); key(ehid); value(function);thread;
                            lua_pop(L, 2);
                        }
                        //L | events_table; key(proxy_table); value(handler_table); key(ehid);
                    }
                }
                //L | events_table; key(proxy_table); value(handler_table);
                lua_pop(L,1);
                //L | events_table; key(proxy_table);
            }
            else
            {
                lua_pop(L, 1);
            }
        }
        //L | events_table
        lua_pop(L, 1);
    }
    else
    {
        dbg ("Error!!!. EVENTS TABLE absents.");
        lua_pop(L, 1);
        result = -1;
    }
    return result;
}

