/*
** lunax.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <fcntl.h>
#include <dlfcn.h>

#include <stdarg.h>
#include <errno.h>

#include "lunax.h"
#include "lua.h"
#include "shared_table.h"
#include "proxy_table.h"

#include <sys/types.h>
#include <sys/mman.h>

#include "mem_main.h"
#include <signal.h>

#include "task_timer.h"
#include "events_table.h"
#include "app.h"
#include "ls.h"
#include "event.h"
#include "pid_table.h"


#include "lauxlib.h"

#define TOENUM(X)  {lua_pushinteger (L,X);lua_setfield (L,-2,#X);}

char ABASE;
char ANAME_SHARED_MEM[256];
char ANAME_SEM_MAIN[256];
__thread uint32_t ATID;

static int init(lua_State* L);
static int test(lua_State* L);
static int errnum2string(lua_State* L);
static int luagetpid(lua_State* L);

void settid(lua_State* L, uint32_t tid)
{
    ATID = tid;
    lua_pushinteger(L, tid);
    lua_setglobal(L, "ATID");
}

uint32_t gettid(void)
{
    return ATID;
}
static const struct luaL_Reg lunax[] =
{
{ "init", init },
{ "test", test },
{ "errno", errnum2string },
{ "getpid", luagetpid },
{ "createT", create_table },
{ "deleteT", delete_table },
{ "attach", attach },
{ "detach", detach },
{ "copyV", copy_value },
{ "setAlias", set_alias },
{ "getAlias", get_alias },
{ "fetch", fetch },
{ "addTask", add_task },
{ "delTask", del_task },
{ "wait", wait_timer },
{ "isUpdate", is_update_table },
// system API
{ "tfetch", tfetch },
{ "dfetch", dfetch },
{ "dbg_shtable", dbg_show_shtable },
{ NULL, NULL } };

void lunax_exit(void)
{
    sem_t* sem_main;
    int sem_main_val;
    int fd, flags;
    dbg("That was all, folks");
    flags = O_RDWR | O_CREAT | O_EXCL;
    fd = shm_open(ANAME_SHARED_MEM, flags, 0777);
    if (fd == -1)
    {
        if (errno == EEXIST)
        {
            sem_main = sem_open(ANAME_SEM_MAIN, O_CREAT, 0777, 0);
            if (sem_main != SEM_FAILED)
            {
                if (!sem_trywait(sem_main))
                {
                    sem_getvalue(sem_main, &sem_main_val);
                    dbg("---close--> sem_main_val:%d", sem_main_val);
                    if (!sem_main_val)
                    {
                        delete_pid2pid_table();
                        delete_main_table();
                        dbg("delete sh memory: %d", errno);
                        shm_unlink(ANAME_SHARED_MEM);
                        sem_close(sem_main);
                        sem_unlink(ANAME_SEM_MAIN);
                        //sem_unlink("/lunax_sem_alloc");
                    }
                    else
                    {
                        sem_close(sem_main);
                    }
                }
            }
        }
        else
        {
            dbg("err :%d", errno);
        }

    }
}

int EXIT = 0;
void term_handler(int i)
{
    printf("Terminating\n");
    EXIT = 1;
    //exit(EXIT_SUCCESS);
}
//In
//L |  string(pid_table name | not)
// Out
//L | empty
static int init(lua_State* L)
{
    sem_t* sem_main;
    int sem_main_val;
    int fd;
    void* addr;
    unsigned long long thr = 0;
    int ret = E_OK;
    char * pid_name = NULL;

    int i;
    char* name_shared_mem;
    char* name_sem_main;
    const char* extra_name = NULL;
    int len;

    if (lua_type(L,-1) == LUA_TSTRING)
    {
        pid_name = (char*)lua_tostring(L,-1);
    }
    struct sigaction sa;
    sigset_t newset;
    sigemptyset(&newset);
    sigaddset(&newset, SIGHUP);
    sigprocmask(SIG_BLOCK, &newset, 0);
    sa.sa_handler = term_handler;
    sigaction(SIGINT, &sa, 0);

    i = atexit(lunax_exit);
    if (i != 0)
    {
        fprintf(stderr, "cannot set exit function\n");
        exit(EXIT_FAILURE);
    }

    lua_getglobal(L, "__LUNAX_IOTABLE");
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        //L | table
        lua_getfield(L, -1, "thr");
        if (lua_type(L, -1) != LUA_TNIL)
        {
            thr = (unsigned long long)lua_tointeger(L, -1);
        }
        lua_pop(L, 1);
        lua_getfield(L, -1, "name");
        //L | table; name
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            extra_name = lua_tostring(L, -1);
            len = strlen(NAME_SHARED_MEM) + strlen(extra_name) + 1;
            name_shared_mem = malloc(len);
            name_shared_mem[0] = 0;
            strcat(name_shared_mem, NAME_SHARED_MEM);
            strcat(name_shared_mem, extra_name);

            len = strlen(NAME_SEM_MAIN) + strlen(extra_name) + 1;
            name_sem_main = malloc(len);
            name_sem_main[0] = 0;
            strcat(name_sem_main, NAME_SEM_MAIN);
            strcat(name_sem_main, extra_name);
        }
        else
        {
            name_shared_mem = strdup(NAME_SHARED_MEM);
            name_sem_main = strdup(NAME_SEM_MAIN);
        }
        lua_pop(L, 2);
    }
    else
    {
        name_shared_mem = strdup(NAME_SHARED_MEM);
        name_sem_main = strdup(NAME_SEM_MAIN);
    }
    lua_pop(L, 1);

    sem_main = sem_open(name_sem_main, O_CREAT | O_EXCL, 0777, 0);
    if (sem_main == SEM_FAILED)
    {
        sem_main = sem_open(name_sem_main, O_CREAT, 0777, 0);
        if (sem_main == SEM_FAILED)
        {
            free(name_shared_mem);
            free(name_sem_main);
            luaL_error(L, "Semaphore fail :%d\n", errno);
            return 1;
        }

        do
        {
            sem_getvalue(sem_main, &sem_main_val);
        } while (!sem_main_val);
    }

    if (G_LUNAX_ADDR == 0)
    {
        fd = shm_open(name_shared_mem, O_RDWR | O_CREAT, 0777);
        if (fd == -1)
        {
            free(name_shared_mem);
            free(name_sem_main);
            luaL_error(L, "Shared memory fail :%d\n", errno);
            return 1;
        }
        ftruncate(fd, SIZE_SHMEMORY);
        addr = mmap(NULL, SIZE_SHMEMORY, PROT_READ | PROT_WRITE, MAP_SHARED, fd,
                0);
        if (addr == MAP_FAILED)
        {
            free(name_shared_mem);
            free(name_sem_main);
            luaL_error(L, "Mmap fail :%d\n", errno);
            return 1;
        }
        dbg("=================> heap:%p", addr);

        sem_getvalue(sem_main, &sem_main_val);
        dbg("-----> sem_main_val:%d",sem_main_val);
        if (!sem_main_val)
        {
            dbg("---init shared area and create main table -->");
            ret = init_mem_main(addr);
            if (ret != E_OK)
            {
                free(name_shared_mem);
                free(name_sem_main);
                luaL_error(L, "Create main table fail :%d", ret);
                return 1;
            }
        }
        else
        {
            dbg("=======remap==========> to %p", addr);
            attach_to_mem_main(addr);
        }
    }
    sem_post(sem_main);
    sem_getvalue(sem_main, &sem_main_val);
    dbg("---111--> sem_main_val:%d",sem_main_val);
    close(fd);
    lua_pushlightuserdata(L, addr);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &ABASE);

    //  registry tid in pid2pid table;
    settid(L, get_inc_main_counter());
    add_pid_to_pid2pid(thr);

    create_threads_table(L);
    create_events_table(L);
    create_timers_table(L);
    clear_read_check_attr(L);

    strcpy(ANAME_SHARED_MEM, name_shared_mem);
    strcpy(ANAME_SEM_MAIN, name_sem_main);
    free(name_shared_mem);
    free(name_sem_main);

    if (pid_name)
    {
        lua_State* LMAIN = get_main_table();
        sem_t* semMAIN = get_main_sem();
        SEM_WAIT(semMAIN);
        lua_pushstring(LMAIN, pid_name);
        if (find_pid_table(LMAIN))
        {
            //LMAIN | empty
            set_alias_pid_table(LMAIN, pid_name);
        }
        else
        {
            // LMAIN | pid_table; pid_number;
            lua_pop(L,2);
            luaL_error(L, "Error: PID name is already existed\n");
        }
        sem_post(semMAIN);
        lua_pop(L,1);
    }
// FOR LUAX only .
//Error classes registration :
// CLunaxError : CError
// CRmShtableError: CLunaxError

    lua_pushstring(L,"CLunaxError");
    lua_getglobal(L,"CError");
    reg_error(L);

    lua_getglobal(L, "CLunaxError");
    lua_pushinteger(L,ERRORCODE_CLunaxError);
    lua_setfield(L,-2,"code");
    lua_pushstring(L,"Lunax. Common error.");
    lua_setfield(L,-2,"msg");

    //L |  CLunaxError
    lua_pushstring(L,"CRmShtableError");
    lua_insert(L,-2);
    reg_error(L);

    lua_getglobal(L, "CRmShtableError");
    lua_pushinteger(L,ERRORCODE_CRmShtableError);
    lua_setfield(L,-2,"code");
    lua_pushstring(L,"Lunax. Shared table was deleted.");
    lua_setfield(L,-2,"msg");
    lua_pop(L,1);

    return 0;
}

static int errnum2string(lua_State* L)
{
    int err = 0;
    if (lua_type(L, -1) != LUA_TNUMBER)
    {
        luaL_error(L, "Error: BAD_ARG\n");
        return 0;
    }

    err = lua_tonumber(L, -1);
    lua_pop(L, 1);
    switch (err)
    {
    case E_OK:
        lua_pushstring(L, "OK.");
        break;

    case E_ERROR:
        lua_pushstring(L, "Error 1: Undefined error.");
        break;

    case E_BAD_ARG:
        lua_pushstring(L, "Error 2: Bad argument.");
        break;

    case E_ACCESS_DENIED:
        lua_pushstring(L, "Error 3: Shared Table. Operation over lunax only.");
        break;

    case E_SHTABLE_ALREADY_EXIST:
        lua_pushstring(L, "Error 4: Shared Table already_exist.");
        break;

    default:
        luaL_error(L, "Undefined code:%d\n", err);
    }
    return 1;
}

static int test(lua_State* L)
{

    if (EXIT)
    {
        dbg("========exit: %d=========>",EXIT);
        exit(EXIT_SUCCESS);
    }
    //lua_State* LMAIN = get_main_table();
    //lua_getglobal (LMAIN,mtable);

    //delete_main_table();
    lua_pushinteger(L, 1);
    return 1;
}


static int luagetpid(lua_State* L)
{
    lua_getglobal(L, "ATID");
    return 1;
}

LUALIB_API int luaopen_lunax(lua_State *L)
{

    lua_getglobal(L, "islunax");
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_pop(L, 1);
        luaL_error(L,
                " The lunax library is able to use with luax (special lua implementation).\n At present work with standart lua is not supported\n");
        return 1;
    }
    lua_pop(L, 1);

    //L | string; string;
    luaL_newlib(L, lunax);
    //L | lunax_table

    lua_newtable(L);
    {

        lua_pushcfunction(L, &set_event_strategy);
        lua_setfield(L, -2, "setStrategy");
        lua_pushcfunction(L, &get_event_strategy);
        lua_setfield(L, -2, "getStrategy");
        lua_pushcfunction(L, &set_event_handler);
        lua_setfield(L, -2, "setHandler");
        lua_pushcfunction(L, &unset_event_handler);
        lua_setfield(L, -2, "unsetHandler");
        lua_pushcfunction(L, &is_event_handlers);
        lua_setfield(L, -2, "isHandlers");
        TOENUM(EV_ASYNC_SYMPLE_UPDATE);
        TOENUM(EV_SYNC_SYMPLE_UPDATE);
        TOENUM(EV_SYNC_PIPE_UPDATE);
        TOENUM(EV_ASYNC_PIPE_UPDATE);

    }
    lua_setfield(L, -2, "event");

    lua_newtable(L);
    {
        lua_pushcfunction(L, &ls);
        lua_setfield(L, -2, "ls");
        lua_pushcfunction(L, &ls_get_main_table);
        lua_setfield(L, -2, "main");
        lua_pushcfunction(L, &ls_get_pid2pid_table);
        lua_setfield(L, -2, "pid2pid");
        lua_pushcfunction(L, &ls_get_voodoo_table);
        lua_setfield(L, -2, "voodoo");
        lua_pushcfunction(L, &ls_get_shared_table);
        lua_setfield(L, -2, "sh");
        lua_pushcfunction(L, &ls_get_shared_table_ref);
        lua_setfield(L, -2, "shref");
        lua_pushcfunction(L, &ls_get_shared_table_events);
        lua_setfield(L, -2, "shevents");
    }
    lua_setfield(L, -2, "ls");

    lua_newtable(L);
    {
        lua_pushcfunction(L, &app_get_procs);
        lua_setfield(L, -2, "getPIDs");
        lua_pushcfunction(L, &app_create_thread);
        lua_setfield(L, -2, "addThread");
        lua_pushcfunction(L, &app_cancel_thread);
        lua_setfield(L, -2, "cancelThread");
    }
    lua_setfield(L, -2, "app");

    lua_newtable(L);
    {
        lua_pushcfunction(L, &call_libfuncs_check);
        lua_setfield(L, -2, "check");
        lua_pushcfunction(L, &call_libfuncs_wait);
        lua_setfield(L, -2, "wait");
        lua_pushcfunction(L, &call_libfuncs_update);
        lua_setfield(L, -2, "update");
        lua_pushcfunction(L, &call_libfuncs_merge);
        lua_setfield(L, -2, "merge");
        lua_pushcfunction(L, &call_libfuncs_create);
        lua_setfield(L, -2, "create");
    }
    lua_setfield(L, -2, "attr");

    lua_newtable(L);
    {
        lua_pushcfunction(L, &lunax_timer_add);
        lua_setfield(L, -2, "add");
        lua_pushcfunction(L, &lunax_timer_del);
        lua_setfield(L, -2, "del");
        lua_pushcfunction(L, &lunax_timer_start);
        lua_setfield(L, -2, "start");
        lua_pushcfunction(L, &lunax_timer_stop);
        lua_setfield(L, -2, "stop");
    }
    lua_setfield(L, -2, "timer");

    lua_newtable(L);
    {
        TOENUM(E_OK);
        TOENUM(E_ERROR);
        TOENUM(E_BAD_ARG);
        TOENUM(E_ACCESS_DENIED);
        TOENUM(E_SHTABLE_ALREADY_EXIST);
    }
    lua_setfield(L, -2, "err");

//
    //L |  lunax_table ;
    return 1;
}
