/*
** task_timer.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "task_timer.h"
#include "mem_main.h"

#include <time.h>
#include <math.h>

#include "events_table.h"

static char THREADS;
static char TIMERS;
//In
// any
//Out
// any
int create_threads_table(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_newtable(L);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    }
    else
    {
        lua_pop(L, 1);
    }
    return 0;
}

//In
// L |  function; params ....
//Out
// L | task handler
int add_task_in_proccess(lua_State* L)
{
    int cnt;
    uint32_t task_handler = 0;

    cnt = lua_gettop(L);

    //L | function; params....
    lua_State* L1 = lua_newthread(L);
    //L | function; params.... ; thread;

    lua_insert(L, -(cnt + 1));
    //L | thread;function; params.... ;
    lua_xmove(L, L1, cnt);
    //L | thread;
    //L1 | function; params....;
    task_handler = get_inc_main_counter();
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    //L | thread; THREADS_TABLE;
    lua_pushvalue(L, -2);
    lua_newtable(L);
    {
        lua_pushinteger(L, 0);
        lua_pushinteger(L, task_handler);
        lua_settable(L, -3);
    }
    //L | thread; THREADS_TABLE; threads; thread_table;
    lua_settable(L, -3);
    //L | thread; THREADS_TABLE;
    lua_pop(L, 2);
    lua_pushinteger(L, task_handler);
    // L | task handler
    return 1;
}

//In
// L | task_handler
//Out
// L | empty
int del_task_in_proccess(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    // L | task handler; THREADS_TABLE;
    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        // L | task_handler; THREADS_TABLE; key(thread); value(thread_table)
        lua_pushvalue(L, 0);
        lua_gettable(L, -2);
        // L | task handler; THREADS_TABLE; key(thread); value(thread_table); task_handler
        if (lua_tointeger(L,-5) == lua_tointeger(L, -1))
        {
            lua_pop(L, 2);
            lua_pushnil(L);
            // L | task handler; THREADS_TABLE; key(thread); nil
            lua_settable(L, -3);
            // L | task handler; THREADS_TABLE;
            break;
        }
        else
        {
            lua_pop(L, 2);
            // L | task handler; THREADS_TABLE; key(thread);
        }
    }
    // L | task handler; THREADS_TABLE;
    lua_pop(L, 2);
    return 0;
}

//In
// L |  proxy_table;
//Out
// L | empty
int check_task_in_threads_table(lua_State* L)
{
    int res = 0;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    // L | task handler; THREADS_TABLE;
    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        // L |  proxy_table; THREADS_TABLE; key(thread); value(thread_table);
        lua_pushvalue(L, -4);
        // L |  proxy_table; THREADS_TABLE; key(thread); value(thread_table); proxy_table;
        lua_gettable(L, -2);
        if (lua_type(L, -1) != LUA_TNIL)
        {
            lua_pop(L, 3);
            res = 1;
            break;
        }
        else
        {
            lua_pop(L, 2);
        }
        // L |  proxy_table; THREADS_TABLE; key(thread);
    }
    // L | thread; THREADS_TABLE;
    lua_pop(L, 2);
    return res;
}

//In
// L |  thread
//Out
// L | empty
int add_task_in_threads_table(lua_State* L)
{
    uint32_t task_handler = 0;

    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    // L | thread; THREADS_TABLE;
    lua_pushvalue(L, -2);
    // L | thread; THREADS_TABLE; thread;
    task_handler = get_inc_main_counter();
    lua_newtable(L);
    {
        lua_pushinteger(L, 0);
        lua_pushinteger(L, task_handler);
        lua_settable(L, -3);
    }
    // L | thread; THREADS_TABLE; thread; thread_table;
    lua_settable(L, -3);
    // L | thread; THREADS_TABLE;
    lua_pop(L, 2);
    return 0;
}

//In
// L |  thread; proxy_table
//Out
// L | empty
int add_event_task_in_threads_table(lua_State* L)
{
    uint32_t task_handler = 0;

    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    // L | thread; proxy_table; THREADS_TABLE;
    lua_pushvalue(L, -3);
    // L | thread; proxy_table; THREADS_TABLE; thread;
    task_handler = get_inc_main_counter();
    lua_newtable(L);
    {
        lua_pushinteger(L, 0);
        lua_pushinteger(L, task_handler);
        lua_settable(L, -3);
        // L | thread; proxy_table; THREADS_TABLE; thread; thread_table;
        lua_pushvalue(L, -4);
        lua_pushinteger(L, 1);
        lua_settable(L, -3);
        // L | thread; proxy_table; THREADS_TABLE; thread; thread_table;
        lua_pushinteger(L, 1);
        lua_pushvalue(L, -5);
        lua_settable(L, -3);
    }
    // L | thread; proxy_table; THREADS_TABLE; thread; thread_table;
    lua_settable(L, -3);
    // L | thread; proxy_table; THREADS_TABLE;
    lua_pop(L, 3);
    return 0;
}
//In
// L |  thread
//Out
// L | empty
int del_task_in_threads_table(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    // L | thread; THREADS_TABLE;
    lua_pushvalue(L, -2);
    // L | thread; THREADS_TABLE; thread;
    lua_pushnil(L);
    // L | thread; THREADS_TABLE; thread; nil;
    lua_settable(L, -3);
    // L | thread; THREADS_TABLE;
    lua_pop(L, 2);
    return 0;
}

//In
// L  empty
//Out
//L | empty
int fetch_by_threads_table(lua_State* L)
{
    int result = 0;
    int res;
    lua_State* L1;
    int nargs;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &THREADS);
    result = 1;
    // L | THREADS_TABLE;
    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        result = 0;
        // L | THREADS_TABLE; key(thread); value(thread_table)
        L1 = lua_tothread(L, -2);
        nargs = lua_gettop(L1) - 1;
        res = lua_resume(L1, L, nargs);
        if (res == LUA_YIELD)
        {
            // L | THREADS_TABLE; key(thread); value(thread_table)
            lua_pop(L, 1);
        }
        else
        {
            if (res != LUA_OK)
            {
                luaL_error(L,lua_tostring(L1,-1));
            }
            // L | THREADS_TABLE; key(thread); value(thread_table)
            lua_pushinteger(L, 1); // index for proxy_table in thread_table
            lua_gettable(L, -2);
            // L | THREADS_TABLE; key(thread); value(thread_table); proxy_table or nill
            if (lua_type(L,-1) == LUA_TTABLE)
            {
                // L | THREADS_TABLE; key(thread); value(thread_table); proxy_table
                clear_event_state_by_events_table(L);
            }
            else
            {
                lua_pop(L,1);
            }

            lua_pop(L, 1);
            lua_pushvalue(L, -1);
            lua_pushnil(L);
            // L | THREADS_TABLE; key(thread); key(thread);nil;
            lua_settable(L, -4);
            // L | THREADS_TABLE; key(thread);
        }
    }
    // L | THREADS_TABLE;
    lua_pop(L, 1);
    return result;
}

int get_ms(long int* sec, long int* msec)
{
    long int ms; // Milliseconds
    time_t s;  // Seconds
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);
    s = spec.tv_sec;
    ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
    if (ms > 999)
    {
        s++;
        ms = 0;
    }
    *msec = ms;
    *sec = s;
    return 0;
}

//In
// any
//Out
// any
int create_timers_table(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_newtable(L);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    }
    else
    {
        lua_pop(L, 1);
    }
    return 0;
}

//In
// L |  delay; function; params ....
//Out
// L | task handler
int add_timer_in_proccess(lua_State* L)
{
    int cnt;
    uint32_t timer_handler = 0;

    cnt = lua_gettop(L);

    //L | delay; function; params....
    lua_State* L1 = lua_newthread(L);
    //L | delay; function; params.... ; thread;
    lua_insert(L, -(cnt));
    //L | delay; thread;function; params.... ;
    lua_xmove(L, L1, cnt - 1);
    //L | delay;thread;
    //L1 | function; params....;
    timer_handler = get_inc_main_counter();
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    //L | delay; thread; TIMERS_TABLE;
    lua_pushinteger(L, timer_handler);
    lua_newtable(L);
    {
        //L | delay; thread; TIMERS_TABLE;timer_handler; timer_table
        lua_pushinteger(L, 1);
        lua_pushvalue(L, -5);
        //L | delay; thread; TIMERS_TABLE;timer_handler; timer_table ,1, thread
        lua_settable(L, -3);
        lua_pushinteger(L, 2);
        lua_pushvalue(L, -6);
        //L | delay; thread; TIMERS_TABLE;timer_handler; timer_table ,2, delay
        lua_settable(L, -3);

        lua_pushinteger(L, 5);
        lua_pushinteger(L, 0);
        lua_settable(L, -3);
    }
    //L | delay; thread; TIMERS_TABLE;timer_handler; timer_table
    lua_settable(L, -3);
    //L | delay; thread; TIMERS_TABLE;
    lua_pop(L, 3);
    lua_pushinteger(L, timer_handler);
    // L | task handler
    return 1;
}

//In
// L |  timer_handler
//Out
// L | empty
int del_timer_in_proccess(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    //L | timer_handler; TIMERS_TABLE;
    lua_pushvalue(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_handler;
    lua_pushnil(L);
    lua_settable(L, -3);
    //L | timer_handler; TIMERS_TABLE;
    lua_pop(L, 2);
    return 0;
}

//In
// L |  timer_handler
//Out
// L | empty
int start_timer_in_proccess(lua_State* L)
{
    long int sec;
    long int ms;

    get_ms(&sec, &ms);

    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    //L | timer_handler; TIMERS_TABLE;
    lua_pushvalue(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_handler;
    lua_gettable(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_table
    lua_pushinteger(L, 5);
    lua_gettable(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_table; star/stop
    if (lua_tointeger(L,-1) == 0)
    {
        lua_pop(L, 1);
        lua_pushinteger(L, 5);
        lua_pushinteger(L, 1);
        lua_settable(L, -3);

        lua_pushinteger(L, 3);
        lua_pushinteger(L, sec);
        lua_settable(L, -3);

        lua_pushinteger(L, 4);
        lua_pushinteger(L, ms);
        lua_settable(L, -3);
        //L | timer_handler; TIMERS_TABLE; timer_table;
    }
    else
    {
        lua_pop(L, 1);
    }
    //L | timer_handler; TIMERS_TABLE; timer_table;
    lua_pop(L, 3);
    return 0;
}

//In
// L |  timer_handler
//Out
// L | empty
int stop_timer_in_proccess(lua_State* L)
{
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    //L | timer_handler; TIMERS_TABLE;
    lua_pushvalue(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_handler;
    lua_gettable(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_table
    lua_pushinteger(L, 5);
    lua_gettable(L, -2);
    //L | timer_handler; TIMERS_TABLE; timer_table; star/stop
    if (lua_tointeger(L,-1) == 1)
    {
        lua_pop(L, 1);
        lua_pushinteger(L, 5);
        lua_pushinteger(L, 0);
        lua_settable(L, -3);
        //L | timer_handler; TIMERS_TABLE; timer_table;
    }
    else
    {
        lua_pop(L, 1);
    }
    //L | timer_handler; TIMERS_TABLE; timer_table;
    lua_pop(L, 3);
    return 0;
}

//In
// L | empty
//Out
// L | empty
int fetch_by_timers_table(lua_State* L)
{
    int result = 0;
    long int sec;
    long int ms;

    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TIMERS);
    result = 1;
    //L |  TIMERS_TABLE;
    lua_pushnil(L); /* first key */
    while (lua_next(L, -2) != 0)
    {
        result = 0;
        //L |  TIMERS_TABLE; key (timer_handler); value(timer_table)
        lua_pushinteger(L, 5);
        lua_gettable(L, -2);
        //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);start/stop
        if (lua_tointeger(L,-1) == 1)
        {
            lua_pop(L, 1);
            //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);
            get_ms(&sec, &ms);
            lua_pushinteger(L, 2);
            lua_gettable(L, -2);
            //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);delay;
            lua_pushinteger(L, 3);
            lua_gettable(L, -3);
            //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);delay;sec
            lua_pushinteger(L, 4);
            lua_gettable(L, -4);
            //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);delay;sec; ms
            ms = (sec - lua_tointeger(L, -2)) * 1000
                    + (ms - lua_tointeger(L, -1));
            if (ms >= lua_tointeger(L, -3))
            {
                lua_pop(L, 3);
                //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);
                lua_pushinteger(L, 1);
                lua_gettable(L, -2);
                //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);thread;
                add_task_in_threads_table(L);
                lua_pushinteger(L, 5);
                lua_pushinteger(L, 0);
                //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);5(start/stop);0
                lua_settable(L, -3);
                //L |  TIMERS_TABLE; key (timer_handler); value(timer_table);
            }
            else
            {
                lua_pop(L, 3);
            }
        }
        else
        {
            lua_pop(L, 1);
        }
        //L | TIMERS_TABLE; key (timer_handler); value(timer_table);
        lua_pop(L, 1);
    }
    //L |  TIMERS_TABLE;
    lua_pop(L, 1);
    return result;
}
