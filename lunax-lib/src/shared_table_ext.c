/*
** share_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "shared_table_ext.h"
#include "shared_table.h"

#ifdef  SH_SIMPLE
// lstate |  newtable1
// Out
// lstate |  newtable1
uint32_t create_shared_table_inner(lua_State* lstate)
{
    uint32_t p_sh;
    p_sh = genlight();

    lua_pushinteger(lstate, p_sh);
    lua_setfield(lstate, -2, "__LIGHT");
    lua_pushinteger(lstate, EV_ASYNC_SYMPLE_UPDATE);
    lua_setfield(lstate, -2, "__ETYPE");
    lua_pushinteger(lstate, -1);
    lua_setfield(lstate, -2, "__ESIZE");

    // metatable for test
//          lua_newtable(lstate);
//          {
//              // lstate | newtable1; newtable2
//
//            // serge_p
//            lua_pushstring(lstate, "__gc");lua_pushcfunction(lstate, gc_test_clean);lua_settable(lstate, -3);
//            lua_pushstring(lstate, "shared table");
//            lua_setfield(lstate,-2,"name");
//          }
//          lua_setmetatable(lstate,-2);
    // lstate | newtable1;
    return p_sh;
}
#else

//In
//lstate | meta_shared_table;
//Out
//lstate | meta_shared_table;
//static void DBG_DIFFS(lua_State* lstate, char * prefix)
//{
//    //lstate | meta_shared_table;
//    lua_pushnil(lstate);
//    while (lua_next(lstate, -2) != 0)
//    {
//        //lstate | meta_shared_table; key; value;
//        if ((lua_type(lstate, -2) == LUA_TNUMBER)
//                && (lua_type(lstate, -1) == LUA_TTABLE))
//        {
//            lua_pushinteger(lstate, 0);
//            lua_gettable(lstate, -2);
//            //lstate | meta_shared_table; key; value; data
//            printf("%s ----> [%d] =  %d\n", prefix,lua_tointeger(lstate, -3), lua_tointeger(lstate, -1));
//            lua_pop(lstate, 2);
//        }
//        else
//        {
//            lua_pop(lstate, 1);
//        }
//    }
//}
//In
// lstate | shared_table
//Out
// lstate | empty
int sync_diff_index(lua_State* lstate)
{
    int diff_cnt;
    int levent_state;
    int curr_diff =1;
    if (lua_getmetatable(lstate,-1) == 0)
    {
        lua_pop(lstate,1);
        return 0;
    }
    lua_remove(lstate,-2);
    // lstate | metatable
    do
    {
        lua_getfield(lstate, -1, "__DS");
        diff_cnt = lua_tointeger(lstate, -1);
        lua_pop(lstate, 1);
        // lstate | metatable
        lua_getglobal(lstate, shtable_events);
        // lstate | metatable; shared_table_events;
        lua_pushinteger(lstate, gettid());
        lua_gettable(lstate, -2);
        if (lua_type(lstate, -1) == LUA_TNIL)
        {
            curr_diff = diff_cnt;
            lua_pop(lstate, 2);
            break;
        }
        // lstate | metatable; shared_table_events; table_events_lights;
        lua_getfield(lstate, -3, "__LIGHT");
        // lstate | metatable; shared_table_events; table_events_lights; light
        lua_gettable(lstate, -2);
        if (lua_type(lstate, -1) == LUA_TNIL)
        {
            lua_pop(lstate, 3);
            break;
        }
        // lstate | metatable; shared_table_events; table_events_lights; event_state;
        levent_state = lua_tointeger(lstate, -1);
        lua_pop(lstate, 3);
        if (levent_state == -1)
        {
            break;
        }

        // lstate | metatable;
        curr_diff = diff_cnt - levent_state;
    } while (0);
    // lstate | metatable;
//=====================
   // DBG_DIFFS(lstate,"[ sync_diff_index ]");
//=====================
    lua_pushstring(lstate, "__index");
    lua_pushinteger(lstate, curr_diff);
    // lstate | metatable; __index; curr_diff
    lua_gettable(lstate, -3);
    // lstate | metatable; __index; table(shared_table_diff);
    lua_settable(lstate, -3);

    lua_pop(lstate,1);
    return 0;

}

// In
// lstate |  newtable1(shared_table)
// Out
// lstate |  newtable1(shared_table)

uint32_t create_shared_table_inner(lua_State* lstate)
{
    uint32_t p_sh;
    p_sh = genlight();

    lua_newtable(lstate);
    // lstate |  newtable1(shared_table); newtable1(proxy_table)
    {
        lua_pushinteger(lstate, p_sh);
        lua_setfield(lstate, -2, "__LIGHT");

        lua_pushinteger(lstate, 0);
        lua_setfield(lstate, -2, "__MAX");
    }
    // lstate |  newtable1(shared_table); newtable1(proxy_table)
    lua_newtable(lstate);
    // lstate |  newtable1(shared_table); newtable1(proxy_table); newtable2(shared_table_diff);
    {
        lua_pushinteger(lstate, p_sh);
        lua_setfield(lstate, -2, "__LIGHT");
        lua_pushinteger(lstate, EV_ASYNC_SYMPLE_UPDATE);
        lua_setfield(lstate, -2, "__ETYPE");
        lua_pushinteger(lstate, -1);
        lua_setfield(lstate, -2, "__ESIZE");
    }
    lua_pushinteger(lstate,1);
    lua_insert(lstate, -2);
    // lstate |  newtable1(shared_table); newtable1(proxy_table); 1 ; newtable2(shared_table_diff);
    lua_pushvalue(lstate,-1);
    // lstate |  newtable1(shared_table); newtable1(proxy_table); 1 ; newtable2(shared_table_diff);  newtable2(shared_table_diff);
    lua_insert(lstate,-3);
    // lstate |  newtable1(shared_table); newtable1(proxy_table);newtable2(shared_table_diff); 1 ; newtable2(shared_table_diff);
    lua_settable(lstate,-4);

    // lstate |  newtable1(shared_table); newtable1(proxy_table);newtable2(shared_table_diff);
    lua_pushinteger(lstate,1);
    lua_setfield(lstate,-3,"__DS");

    // lstate |  newtable1(shared_table); newtable1(proxy_table);newtable2(shared_table_diff);
    lua_pushvalue(lstate,-1);
    // lstate |  newtable1(shared_table); newtable1(proxy_table);newtable2(shared_table_diff); newtable2(shared_table_diff);
    lua_pushstring(lstate, "__index");
    lua_insert(lstate,-2);
    lua_settable(lstate, -4);
    // lstate |  newtable1(shared_table); newtable1(proxy_table);newtable2(shared_table_diff);

    lua_pushstring(lstate, "__newindex");
    lua_insert(lstate,-2);
    lua_settable(lstate, -3);

    // lstate |  newtable1(shared_table); newtable1(proxy_table);
    lua_setmetatable(lstate,-2);
    return p_sh;
}


// In
// lstate | meta_shared_table;
// Out
// lstate | meta_shared_table;
static int pack_shared_table_diff(lua_State* lstate, int diffcnt, int ind)
{
    int i;
    int xind = (diffcnt-ind)+1;
    lua_pushinteger(lstate,1);
    lua_gettable(lstate,-2);
    //printf("xind : %d ind : %d diffcnt : %d\n",xind, ind, diffcnt);
    //lstate | meta_shared_table; shared_table_diff[1]
    for (i = 2; i<= xind; i++)
    {
        lua_pushinteger(lstate,i);
        lua_gettable(lstate,-3);
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[i];
        lua_pushnil(lstate);
        while (lua_next(lstate, -2) != 0)
        {
            //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[i]; key; value;
            lua_pushvalue(lstate,-2);
            lua_pushvalue(lstate,-2);
            //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[i]; key; value;  key; value;
            lua_settable(lstate,-6);
            //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[i]; key; value;
            lua_pop(lstate,1);
        }
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[i];
        lua_pop(lstate,1);
        lua_pushinteger(lstate,i);
        lua_pushnil(lstate);
        //lstate | meta_shared_table; shared_table_diff[1];i; nil
        lua_settable(lstate,-4);
        //lstate | meta_shared_table; shared_table_diff[1];
    }
    //lstate | meta_shared_table; shared_table_diff[1];
    for (i = 0; i < diffcnt-xind; i++)
    {
        lua_pushinteger(lstate,2+i);
        lua_pushinteger(lstate,xind+i+1);
        //lstate | meta_shared_table; shared_table_diff[1];(2+i);(xind+i+1)
        lua_gettable(lstate,-4);
        //lstate | meta_shared_table; shared_table_diff[1]; (2+i); shared_table_diff[ind+i+1];
        lua_settable(lstate,-4);
        //lstate | meta_shared_table; shared_table_diff[1];
        lua_pushinteger(lstate,xind+i+1);
        lua_pushnil(lstate);
        lua_settable(lstate,-4);
        //lstate | meta_shared_table; shared_table_diff[1];
    }
    if (diffcnt != xind)
    {
        //lstate | meta_shared_table; shared_table_diff[1];
        lua_pushinteger(lstate, 2);
        lua_gettable(lstate, -3);
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[2];
        lua_getmetatable(lstate, -1);
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[2]; meta_shared_table_diff[2]
        lua_pushstring(lstate, "__index");
        lua_pushvalue(lstate, -4);
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[2]; meta_shared_table_diff[2]; __index; shared_table_diff[1]
        lua_settable(lstate, -3);
        //lstate | meta_shared_table; shared_table_diff[1]; shared_table_diff[2]; meta_shared_table_diff[2];
        lua_pop(lstate, 3);
        //lstate | meta_shared_table;
    }
    else
    {
        //lstate | meta_shared_table; shared_table_diff[1];
        lua_pop(lstate, 1);
    }
    //lstate | meta_shared_table;
    //=====================
        //DBG_DIFFS(lstate,"[ pack_shared_table_diff ]");
    //=====================
    return (diffcnt-xind +1);
}


// In
// lstate | meta_shared_table;
// Out
// lstate | meta_shared_table;
static int add_shared_table_diff(lua_State* lstate, int di)
{
    if (di < 2) return di;
    // lstate | meta_shared_table;
    lua_newtable(lstate);
    {
        // lstate | meta_shared_table; newtable(shared_table_diff;
        lua_newtable(lstate);
        lua_pushstring(lstate, "__index");
        lua_pushinteger(lstate, di-1);
        // lstate | meta_shared_table; newtable(shared_table_diff);newtable(porxy_shared...); __index; [di-1]
        lua_gettable(lstate,-5);
        // lstate | meta_shared_table; newtable(shared_table_diff);newtable(porxy_shared...); __index;(shared_table_diff[di-1])
        lua_settable(lstate,-3);
        // lstate | meta_shared_table; newtable(shared_table_diff);newtable(porxy_shared...);
        lua_setmetatable(lstate,-2);
        // lstate | meta_shared_table; ;newtable(shared_table_diff);
        lua_pushinteger(lstate,di);
        lua_pushvalue(lstate,-2);
        // lstate | meta_shared_table; ;newtable(shared_table_diff);di; newtable(shared_table_diff);
        lua_settable(lstate,-4);
    }
    // lstate | meta_shared_table; newtable(shared_table_diff);
    lua_pop(lstate,1);
    // lstate | meta_shared_table;
    //=====================
       // DBG_DIFFS(lstate,"[ add_shared_table_diff ]");
    //=====================
    return di;
}

// In
// L lstate
//lstate | shared_table; key(not use);
// Out
// L lstate
// lstate | shared_table; key(not use);
int sync_shared_table_diff_inner(lua_State* L)
{
    lua_State* lstate;

    lstate = LUNAX_CAST(lua_touserdata(L, -1));

    return sync_shared_table_diff_inner_inner(lstate);
}
// In
//lstate | shared_table; key(not use);
// Out
// lstate | shared_table; key(not use);
int sync_shared_table_diff_inner_inner(lua_State* lstate)
{
    int diff_ds;// last diff which already exist
    int diff_max;// last diff which must exist

    lua_pushvalue(lstate,-2);
    //lstate | shared_table; key; shared_table

    lua_getmetatable(lstate,-1);
    // lstate |  shared_table; key; shared_table; meta_shared_table
    lua_getfield(lstate,-1,"__MAX");
    // lstate |  shared_table; key; shared_table; meta_shared_table; __MAX
    diff_max = lua_tointeger(lstate,-1)+1; // last diff which must exist
    lua_pop(lstate,1);
    lua_getfield(lstate,-1,"__DS");
    diff_ds = lua_tointeger(lstate,-1);
    //printf( "max:%d ds:%d\n",diff_max,diff_ds);
    lua_pop(lstate,1);

    // lstate |  shared_table; key; shared_table; meta_shared_table;
    if (!((diff_max == 1) && (diff_ds == 1)))
    {
        if (diff_max > 0)
        {
            diff_ds = add_shared_table_diff(lstate, diff_ds + 1);
            if (diff_ds != diff_max)
            {
                diff_ds = pack_shared_table_diff(lstate, diff_ds, diff_max);
            }
        }
    }
    // lstate |  shared_table; key; shared_table; meta_shared_table;
    lua_pushinteger(lstate,diff_ds);
    lua_setfield(lstate,-2,"__DS");
    lua_pushstring (lstate,"__newindex");
    lua_pushinteger(lstate,diff_ds);
    // lstate |  shared_table; key; shared_table; meta_shared_table; __newindex; diff_ds
    lua_gettable(lstate,-3);
    // lstate |  shared_table; key; shared_table; meta_shared_table; __newindex; table(shared_table_diff)
    lua_settable(lstate,-3);
    lua_pop(lstate,2);
    // lstate |  shared_table; key;
    return 0;
}

// In
//lstate | shared_table;
// Out
// lstate | shared_table;
int repack_shared_table_diff(lua_State* lstate, int strategy_size)
{
    int diff_ds;// last diff which already exist
    int diff_max;// last diff which must exist

    lua_pushvalue(lstate,-2);
    //lstate | shared_table; shared_table

    lua_getmetatable(lstate,-1);
    // lstate |  shared_table; shared_table; meta_shared_table
    diff_max = strategy_size;
    // lstate |  shared_table; shared_table; meta_shared_table
    lua_getfield(lstate,-1,"__DS");
    diff_ds = lua_tointeger(lstate,-1);

    //printf( "max:%d ds:%d\n",diff_max,diff_ds);
    lua_pop(lstate,1);

    // lstate |  shared_table; key; shared_table; meta_shared_table;
    if (diff_ds != 1)
    {
        if (diff_max > 0)
        {
            diff_ds = pack_shared_table_diff(lstate, diff_ds, diff_max);
        }
    }
    // lstate |  shared_table; key; shared_table; meta_shared_table;
    lua_pushinteger(lstate,diff_ds);
    lua_setfield(lstate,-2,"__DS");
    lua_pushstring (lstate,"__newindex");
    lua_pushinteger(lstate,diff_ds);
    // lstate |  shared_table; shared_table; meta_shared_table; __newindex; diff_ds
    lua_gettable(lstate,-3);
    // lstate |  shared_table; shared_table; meta_shared_table; __newindex; table(shared_table_diff)
    lua_settable(lstate,-3);
    lua_pop(lstate,2);
    // lstate |  shared_table;
    return 0;
}
#endif
