/*
** main_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "lunax.h"

#include "shared_table.h"
#include "pid_table.h"
#include "mem_main.h"
#include <sys/types.h>
#include <unistd.h>

// In
// LMAIN | mtable
// Out
// LMAIN | pid;
int create_pid_table(lua_State* LMAIN, int pid, const char* name)
{
    lua_pushnumber(LMAIN, pid);
    lua_newtable(LMAIN);
    {
        // LMAIN | mtable; int_pid ;newtable0;
        lua_pushstring(LMAIN, name);
        lua_setfield(LMAIN, -2, "name");
    }
    lua_settable(LMAIN, -3);
    // LMAIN | mtable;

    lua_pushnumber(LMAIN, pid);
    lua_gettable(LMAIN, -2);
    // LMAIN | mtable; pid
    lua_remove(LMAIN, -2);
    // LMAIN | pid;
    return 0;
}

//In
//LMAIN | empty
//Out
//LMAIN | empty
int set_alias_pid_table(lua_State* LMAIN, const char* name)
{
    lua_getglobal(LMAIN, mtable);
    lua_pushnumber(LMAIN, gettid());
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TNIL)
    {
        //LMAIN | mtable;nil
        lua_pop(LMAIN, 1);
        //LMAIN | mtable;
        create_pid_table(LMAIN, gettid(), name);
        lua_pop(LMAIN, 1);
    }
    else
    {
        //LMAIN | mtable;value
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            //LMAIN | mtable;table
            lua_pushstring(LMAIN, name);
            lua_setfield(LMAIN, -2, "name");
        }
        else
        {
            return 1;
        }
        lua_pop(LMAIN, 2);
    }
    return 0;
}

//In
// LMAIN | pid_table
//Out
// LMAIN | empty
int delete_pid_table_inner(lua_State* LMAIN)
{
    lua_pushnil(LMAIN); /* first key */
    //LMAIN | pid_table; nil;
    while (lua_next(LMAIN, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
        //		       printf("%s - %s\n",
        //		              lua_typename(L, lua_type(L, -2)),
        //		              lua_typename(L, lua_type(L, -1)));
        //LMAIN | pid_table; key; value;
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            //LMAIN | pid_table; key; shtable
            delete_share_table_inner(LMAIN);
            //LMAIN | pid_table; key;
        //LMAIN | pid_table; key;
        lua_pushvalue(LMAIN, -1);
        //LMAIN | pid_table; key; key
        lua_pushnil(LMAIN);
        //LMAIN | pid_table; key; key; nil
        lua_settable(LMAIN, -4);
        //LMAIN | pid_table; key;
        }
        else
        {
            //LMAIN | pid_table; key; value(name | n);
            lua_pop(LMAIN, 1);
        }
        //LMAIN | pid_table; key;

    }
    lua_pop(LMAIN, 1);
    return 0;
}

//In
// LMAIN | empty
//Out
// LMAIN | empty
int delete_pid_table(int pid)
{
    sem_t* semMAIN;

    semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    lua_State* LMAIN = get_main_table();

    lua_getglobal(LMAIN, mtable);
    lua_pushnumber(LMAIN, pid);
    lua_gettable(LMAIN, -2);
    //LMAIN | mtable; pid_table;
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        delete_pid_table_inner(LMAIN);
        //LMAIN | mtable;
        lua_pushnumber(LMAIN, pid);
        lua_pushnil(LMAIN);
        lua_settable(LMAIN, -3);
        //LMAIN | mtable;
    }
    else
    {
        lua_pop(LMAIN,1);
    }

    lua_pop(LMAIN, 1);
    sem_post(semMAIN);
    return 0;
}

//In
// LMAIN | pid_name;
//Out
// LMAIN | pid_table; pid_number;
// for error LMAIN | empty

int find_pid_table(lua_State* LMAIN)
{
    const char* lname;
    const char* pid_name;
    int pid;
    int isfind = 0;

    pid_name = lua_tostring(LMAIN, -1);
    if (lua_type(LMAIN, -1) == LUA_TSTRING)
    {
        lua_getglobal(LMAIN, mtable);
        lua_pushnil(LMAIN); /* first key */
        // LMAIN | pid_name; mtable; nil;
        while (lua_next(LMAIN, -2) != 0)
        {
            // LMAIN | pid_name; mtable;key; value;
            if (lua_type(LMAIN, -1) == LUA_TTABLE)
            {
                // LMAIN | pid_name; mtable;key; pid_table;
                pid = lua_tonumber(LMAIN, -2);
                lua_getfield(LMAIN, -1, "name");
                // LMAIN | pid_name; mtable;key; pid_table; name
                lname = lua_tostring(LMAIN, -1);
                if (strcmp(pid_name, lname) == 0)
                {
                    isfind = 1;
                    lua_pop(LMAIN, 1);
                    // LMAIN | pid_name; mtable;key;pid_table;
                    break;
                }
                lua_pop(LMAIN, 1);
                // LMAIN | pid_name; mtable;key;pid_table;
            }
            lua_pop(LMAIN, 1);
            // LMAIN | pid_name; mtable;key;
        }
        if (isfind)
        {
            // LMAIN | pid_name; mtable;key;pid_table;
            lua_remove(LMAIN, -2);
            lua_remove(LMAIN, -2);
            lua_remove(LMAIN, -2);
            // LMAIN | pid_table;
        }
        else
        {
            // LMAIN | pid_name; mtable;
            lua_pop(LMAIN, 2);
            return 1;
        }
    }
    else
    {
        // LMAIN | pid_name;
        if (lua_type(LMAIN, -1) == LUA_TNUMBER)
        {
            // LMAIN | pid_number;
            pid = lua_tonumber(LMAIN, -1);
            lua_getglobal(LMAIN, mtable);
            // LMAIN | pid_number;mtable;
            lua_pushvalue(LMAIN, -2);
            // LMAIN | pid_number; mtable; pid_number
            lua_gettable(LMAIN, -2);
            // LMAIN | pid_number;mtable;value
            if (lua_type(LMAIN, -1) == LUA_TNIL)
            {
                lua_pop(LMAIN, 3);
                return 1;
            }
            else
            {
                // LMAIN | pid_number;mtable; pid_table;
                lua_remove(LMAIN, -2);
                lua_remove(LMAIN, -2);
                // LMAIN | pid_table;
            }
        }
        else
        {
            // LMAIN | pid_name;
            lua_pop(LMAIN, 1);
            return 1;
        }
    }
    lua_pushnumber(LMAIN, pid);
    // LMAIN | pid_table; pid_number;
    return 0;
}

//In
// LMAIN | sh_table;
//Out
// LMAIN | empty;
void clear_tid_in_sh_table(lua_State* LMAIN)
{
    lua_pushinteger(LMAIN, gettid());
    lua_pushnil(LMAIN);
    lua_settable(LMAIN,-3);
    lua_pop(LMAIN,1);
}

//In
// LMAIN | sh_table;
//Out
// LMAIN | empty;
void inc_tid_in_sh_table(lua_State* LMAIN)
{
    int cnt;
    lua_pushinteger(LMAIN, gettid());
    lua_pushvalue(LMAIN,-1);
    // LMAIN | sh_table; tid; tid
   // dbg("tid:%d",lua_tointeger(LMAIN,-1));
    lua_gettable(LMAIN,-3);
    if (lua_type(LMAIN,-1) == LUA_TNUMBER)
    {
        cnt = lua_tointeger(LMAIN,-1);
        cnt++;
        lua_pop(LMAIN,1);
        lua_pushinteger(LMAIN,cnt);
        // LMAIN | sh_table; tid; cnt;
        lua_settable(LMAIN,-3);
    }
    else
    {
        lua_pop(LMAIN,1);
        lua_pushinteger(LMAIN,1);
        // LMAIN | sh_table; tid; 1;
        lua_settable(LMAIN,-3);
    }
    lua_pop(LMAIN,1);
}

//In
// LMAIN | empty;
//Out
// LMAIN | empty;
void find_and_inc_tid_in_sh_table(lua_State* lstate)
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, voodoo);
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        lua_pushlightuserdata(LMAIN, LUNAX_RE_CAST(lstate));
        // LMAIN | vooodoo; lstate;
        lua_gettable(LMAIN,-2);
        // LMAIN | vooodoo;  sh_table
        inc_tid_in_sh_table(LMAIN);
        // LMAIN | vooodoo;
    }
    lua_pop(LMAIN,1);
    sem_post(semMAIN);
}

//In
// LMAIN | sh_table;
//Out
// LMAIN | empty;
void dec_tid_in_sh_table(lua_State* LMAIN)
{
    int cnt;
    lua_pushinteger(LMAIN, gettid());
    lua_pushvalue(LMAIN,-1);
    // LMAIN | sh_table; tid; tid
   // dbg("tid:%d",lua_tointeger(LMAIN,-1));
    lua_gettable(LMAIN,-3);

    if (lua_type(LMAIN,-1) == LUA_TNUMBER)
    {
        cnt = lua_tointeger(LMAIN,-1);
        cnt--;
        if (cnt == 0)
        {
            lua_pop(LMAIN,1);
            lua_pushnil(LMAIN);
            // LMAIN | sh_table; tid; nil;
            lua_settable(LMAIN,-3);
        }
        else
        {
            lua_pop(LMAIN,1);
            lua_pushinteger(LMAIN,cnt);
            // LMAIN | sh_table; tid; cnt;
            lua_settable(LMAIN,-3);
        }
    }
    lua_pop(LMAIN,1);
}
//In
// LMAIN | sh_table;
//Out
// LMAIN | empty;
int try_delete_sh_table(lua_State* LMAIN)
{
    int cnt =0;
    lua_pushnil(LMAIN);
    while (lua_next(LMAIN, -2) != 0)
    {
        // LMAIN | sh_table; key; value
        if((lua_type(LMAIN,-2) == LUA_TNUMBER) && (lua_tointeger(LMAIN,-2) > 2))
                    dbg("--> pid:%d num:%d\n",lua_tointeger(LMAIN,-2),lua_tointeger(LMAIN,-1));
        cnt++;
        lua_pop(LMAIN,1);
    }
    dbg("count of field:%d (2 for real delete)",cnt);
    if (cnt == 2)
    {
        real_delete_share_table_inner(LMAIN);
    }
    else
    {
        lua_pop(LMAIN,1);
    }
    return cnt-2;
}

//In
// LMAIN | empty;
//Out
// LMAIN | empty;
int force_delete_sh_table(lua_State* LMAIN)
{
    lua_getglobal(LMAIN,voodoo);
    lua_pushnil(LMAIN);
    while (lua_next(LMAIN, -2) != 0)
    {
        // LMAIN | voodoo; key(lstate); value(sh_table);
        real_delete_share_table_inner(LMAIN);
    }
    lua_pop(LMAIN,1);
    return 0;
}
