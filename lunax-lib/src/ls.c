/*
** ls.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "ls.h"
#include "mem_main.h"
#include "deepcopy.h"
#include "pid_table.h"
#include "shared_table.h"

int ls(lua_State* L)
{
    return show_main_table(L);
}

//In
//L |  empty
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_main_table(lua_State* L)
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        xcopy_deep(LMAIN,L,lua_gettop(LMAIN),2);
    }
    lua_pop(LMAIN,1);
    sem_post(semMAIN);
    return 1;
}

//In
//L |  empty
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_pid2pid_table(lua_State* L)
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, pid2pid);
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        xcopy_deep(LMAIN,L,lua_gettop(LMAIN),2);
    }
    lua_pop(LMAIN,1);
    sem_post(semMAIN);
    return 1;
}
//In
//L |  empty
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_voodoo_table(lua_State* L)
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, voodoo);
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        xcopy_deep(LMAIN,L,lua_gettop(LMAIN),1);
    }
    lua_pop(LMAIN,1);
    sem_post(semMAIN);
    return 1;
}

//In
//L |  string(pid_table_name) | string(shared_table_name)
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_shared_table(lua_State* L)
{
    char * pid_name;
    char* share_name;
    lua_State* lstate;
    sem_t* lsem;

    if (lua_type(L,-1) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    if (lua_type(L,-2) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    pid_name = (char*)lua_tostring(L,-2);
    share_name = (char*)lua_tostring(L,-1);

    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();

    lua_pushstring(LMAIN,pid_name);
    find_pid_table(LMAIN);
    // LMAIN |  pid_table; pid_number;
    lua_pop(LMAIN, 1);
    // LMAIN | pid_table;
    lua_getfield(LMAIN, -1, share_name);

    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        // LMAIN | pid_table; shared_table
        lua_pushvalue(LMAIN, -1);
        // LMAIN | pid_table; shtable; shtable
        lstate = get_lstate(LMAIN);
        lsem = get_lsem(LMAIN);
        lua_pop(LMAIN,lua_gettop(LMAIN));
        sem_post((semMAIN));
    }
    else
    {
        lua_pop(LMAIN,2);
        sem_post((semMAIN));
        lua_pop(L,lua_gettop(L));
        return 0;
    }

    SEM_WAIT(lsem);

    lua_pop(L,lua_gettop(L));
    lua_getglobal(lstate, shtable);
    if (lua_type(lstate,-1) == LUA_TTABLE)
    {
        xcopy_deep(lstate,L,lua_gettop(lstate),5);
    }
    lua_pop(lstate,1);

    sem_post(lsem);
    return 1;
}

//In
//L |  string(pid_table_name) | string(shared_table_name)
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_shared_table_ref(lua_State* L)
{
    char * pid_name;
    char* share_name;
    lua_State* lstate;
    sem_t* lsem;

    if (lua_type(L,-1) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    if (lua_type(L,-2) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    pid_name = (char*)lua_tostring(L,-2);
    share_name = (char*)lua_tostring(L,-1);

    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();

    lua_pushstring(LMAIN,pid_name);
    find_pid_table(LMAIN);
    // LMAIN |  pid_table; pid_number;
    lua_pop(LMAIN, 1);
    // LMAIN | pid_table;
    lua_getfield(LMAIN, -1, share_name);

    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        // LMAIN | pid_table; shared_table
        lua_pushvalue(LMAIN, -1);
        // LMAIN | pid_table; shtable; shtable
        lstate = get_lstate(LMAIN);
        lsem = get_lsem(LMAIN);
        lua_pop(LMAIN,lua_gettop(LMAIN));
        sem_post((semMAIN));
    }
    else
    {
        lua_pop(LMAIN,2);
        sem_post((semMAIN));
        lua_pop(L,lua_gettop(L));
        return 0;
    }

    SEM_WAIT(lsem);

    lua_pop(L,lua_gettop(L));
    lua_getglobal(lstate, shtable_ref);
    if (lua_type(lstate,-1) == LUA_TTABLE)
    {
        xcopy_deep(lstate,L,lua_gettop(lstate),5);
    }
    lua_pop(lstate,1);

    sem_post(lsem);
    return 1;
}

//In
//L |  string(pid_table_name) | string(shared_table_name)
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int ls_get_shared_table_events(lua_State* L)
{
    char * pid_name;
    char* share_name;
    lua_State* lstate;
    sem_t* lsem;

    if (lua_type(L,-1) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    if (lua_type(L,-2) != LUA_TSTRING)
    {
        lua_pop(L,lua_gettop(L));
        return 0;
    }
    pid_name = (char*)lua_tostring(L,-2);
    share_name = (char*)lua_tostring(L,-1);

    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();

    lua_pushstring(LMAIN,pid_name);
    find_pid_table(LMAIN);
    // LMAIN |  pid_table; pid_number;
    lua_pop(LMAIN, 1);
    // LMAIN | pid_table;
    lua_getfield(LMAIN, -1, share_name);

    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        // LMAIN | pid_table; shared_table
        lua_pushvalue(LMAIN, -1);
        // LMAIN | pid_table; shtable; shtable
        lstate = get_lstate(LMAIN);
        lsem = get_lsem(LMAIN);
        lua_pop(LMAIN,lua_gettop(LMAIN));
        sem_post((semMAIN));
    }
    else
    {
        lua_pop(LMAIN,2);
        sem_post((semMAIN));
        lua_pop(L,lua_gettop(L));
        return 0;
    }

    SEM_WAIT(lsem);

    lua_pop(L,lua_gettop(L));
    lua_getglobal(lstate, shtable_events);
    if (lua_type(lstate,-1) == LUA_TTABLE)
    {
        xcopy_deep(lstate,L,lua_gettop(lstate),5);
    }
    lua_pop(lstate,1);

    sem_post(lsem);
    return 1;
}

//In
//L | empty
// Out
// L| empty

int TEST =0;
void debug_monitor (lua_State* L)
{
    char* dbg_name = "__DEBUG__";
    char debug_name[64];
    lua_getglobal(L, "__ADBG");
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_pop(L, 1);
        lua_getglobal(L, "ATID");
        if (lua_type(L, -1) == LUA_TNIL)
        {
            dbg("Unknown Error\n");
            lua_pop(L, lua_gettop(L));
            return;
        }
        sprintf(debug_name, "%s%"LUA_INTEGER_FRMLEN"d", dbg_name, lua_tointeger(L, -1));
        lua_pop(L, 1);
        lua_pushstring(L, debug_name);
        create_table(L);
        lua_pop(L,1);
        lua_setglobal(L,"__ADBG");
    }
    else
    {
        lua_pop(L, 1);
    }
    lua_pushstring(L,"__debug");
    lua_pushstring(L,"__dbg");
    if (attach(L) == 0)
    {
        dbg("Unknown Error.");
        lua_pop(L, lua_gettop(L));
        return;
    }
    if (lua_type(L,-1) == LUA_TTABLE)
    {
        if (TEST == 0)
        {
            TEST = 1;
            printf("Attached to ======> __debug /__dbg\n");
        }
    }
    else
    {
        if (TEST == 1)
        {
            TEST = 0;
            printf("Detached from ======> __debug /__dbg\n");
        }
    }


}
