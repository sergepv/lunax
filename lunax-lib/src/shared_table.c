/*
** share_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include <semaphore.h>

#include "shared_table.h"
#include "deepcopy.h"
#include "mem_main.h"
#include "pid_table.h"
#include "select_table.h"
#include "proxy_table.h"

#include <shared_pmpa.h>
#include "shared_table_ext.h"
#include "shared_events_table.h"

#define LSTATE_KEY 1
#define SEM_KEY 2

uint32_t genlight()
{
    return get_inc_main_counter();
}

//In
// from | table
//Out
// from | empty
uint32_t getlight(lua_State *from)
{
    uint32_t res;
    lua_getfield(from, -1, "__LIGHT");
    res = lua_tointeger(from, -1);
    lua_pop(from, 2);
    return res;
}
//In
// from | light
//Out
// to | table
SH_TABLE_STATE light_to_table(lua_State *from, lua_State *to)
{
    SH_TABLE_STATE res = SH_LIGHT_EXIST;
    switch (lua_type(from, -1))
    {
    case LUA_TNUMBER:
        lua_getglobal(to, shtable_ref);
        if (lua_type(to,-1) == LUA_TNIL)
        {
            dbg("shared table is deleted lstate: %p", LUNAX_RE_CAST(to));
            // to | null
            // from | light
            lua_pop(from, 1);
            return SH_LIGHT_DELETED_ROOT;
        }
        // to | shtable_ref
        // from | light
        lua_pushinteger(to, (uint32_t) lua_tointeger(from, -1));
        // to | shtable_ref; light
        // from | light
        lua_gettable(to, -2);
        if (lua_type(to,-1) == LUA_TNIL)
        {
            dbg("leaf table of shared table is deleted light: %d", lua_tointeger(from, -1));
            // to | shtable_ref;  nill;
            // from | light
            lua_pop(from, 1);
            lua_remove(to, -2);
            return SH_LIGHT_DELETED_LEAF;
        }
        // to | shtable_ref; table
        lua_remove(to, -2);
        res = SH_LIGHT_EXIST;
        break;
    default:
        lua_pushnil(to);
        res = SH_LIGHT_NONEXIST;
        break;
    }
    lua_pop(from, 1);
    return res;
}

//In
//from | shared_table
//Out
//from | empty
//to | light
uint32_t table_to_light(lua_State *from, lua_State *to)
{
    uint32_t res = 0;
    switch (lua_type(from, -1))
    {
    case LUA_TTABLE:
        lua_getfield(from, -1, "__LIGHT");
        res = lua_tointeger(from, -1);
        lua_pop(from, 1);
        lua_pushinteger(to, res);
        break;
    default:
        lua_pushnil(to);
        break;
    }
    lua_pop(from, 1);
    return res;
}

//In
// LMAIN | shtable
//Out
// LMAIN | empty
lua_State* get_lstate(lua_State* LMAIN)
{
    lua_State* lstate;
    lua_pushinteger(LMAIN, LSTATE_KEY);
    lua_gettable(LMAIN, -2);
    lstate = LUNAX_CAST(lua_touserdata(LMAIN, -1));
    lua_pop(LMAIN, 2);
    return lstate;
}

//In
// LMAIN | shtable
//Out
// LMAIN | empty
sem_t* get_lsem(lua_State* LMAIN)
{
    sem_t* lsem;
    lua_pushinteger(LMAIN, SEM_KEY);
    lua_gettable(LMAIN, -2);
    lsem = LUNAX_CAST(lua_touserdata(LMAIN, -1));
    lua_pop(LMAIN, 2);
    return lsem;
}

int check_share_table(lua_State* L, int pid, const char* name)
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    lua_pushnumber(LMAIN, pid);
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        lua_pushstring(LMAIN, name);
        lua_gettable(LMAIN, -2);
        if (lua_type(LMAIN, -1) != LUA_TTABLE)
        {
            sem_post(semMAIN);
            return -1;
        }
    }
    sem_post(semMAIN);
    return 0;
}

//In
//L | any
//Out
//L | lstate; semaphore; light
int get_share_table(lua_State* L, lua_State* lstate, sem_t* lsem)
{
    lua_getglobal(lstate, shtable);
    lua_pushlightuserdata(L, LUNAX_RE_CAST(lstate));
    lua_pushlightuserdata(L, LUNAX_RE_CAST(lsem));
    // lstate | share table
    table_to_light(lstate, L);
    //lstate | empty
    return 0;
}

int gc_test_clean(lua_State* L)
{
    lua_getmetatable(L, -1);
    lua_getfield(L, -1, "name");
    printf("#### GC shared [%s] >>>>>>>>>>>>>\n", lua_tostring(L, -1));
    return 0;
}

//static void *l_alloc2 (void *ud, void *ptr, size_t osize,
//                                           size_t nsize) {
//  (void)ud;  (void)osize;  /* not used */
//  if (nsize == 0) {
//    free(ptr);
//    return NULL;
//  }
//  else
//    return realloc(ptr, nsize);
//}

// In
// L | empty
// LMAIN | empty
// Out
// L | luserdata_lstate; luserdata_lsem; light
// LMAIN | empty
int create_share_table(lua_State* L, int pid, const char* name)
{

    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    lua_State* LMAIN = get_main_table();
    lua_State* lstate;
    sem_t* lsem;
    lua_getglobal(LMAIN, mtable);
    lua_pushnumber(LMAIN, pid);
    lua_gettable(LMAIN, -2);
    // LMAIN | mtable,val (pid_table)
    if (lua_type(LMAIN, -1) == LUA_TNIL)
    {
        // LMAIN | mtable; nil;
        lua_pop(LMAIN, 1);
        // LMAIN | mtable;
        lua_pushvalue(LMAIN,-1);
        // LMAIN | mtable; mtable;
        create_pid_table(LMAIN, pid, "lunax");
        // LMAIN | mtable; pid_table;
    }
    {
        // LMAIN | mtable ;pid_table;

        lua_pushstring(LMAIN, name);
        lua_newtable(LMAIN);
        {
            // LMAIN | mtable ;pid_table; name; shtable;
            lstate = lua_newstate((lua_Alloc) PMPA_DETECT, get_base_addr()); ////PMPA_DETECT or l_alloc2
            lua_pushinteger(LMAIN, LSTATE_KEY);
            lua_pushlightuserdata(LMAIN, LUNAX_RE_CAST(lstate));
            lua_settable(LMAIN, -3);
            // LMAIN | mtable |pid_table;name; shtable;
            lsem = (sem_t*) pmpa_malloc_sf((void*) G_LUNAX_ADDR, sizeof(sem_t));
            if (sem_init(lsem, 1, 0))
            {
                return E_ERROR;
            }
            lua_pushinteger(LMAIN, SEM_KEY);
            lua_pushlightuserdata(LMAIN, LUNAX_RE_CAST(lsem));
            lua_settable(LMAIN, -3);

            lua_pushinteger(LMAIN, pid);
            lua_setfield(LMAIN,-2,"owner");
        }
        // LMAIN | mtable |pid_table; name; shtable(new)
        lua_getglobal(LMAIN,voodoo);
        lua_pushlightuserdata(LMAIN, LUNAX_RE_CAST(lstate));
        lua_pushvalue(LMAIN,-3);
        // LMAIN | mtable |pid_table; name; shtable(new); voodoo; lstate; ; shtable(new)
        lua_settable(LMAIN, -3);
        lua_pop(LMAIN,1);
        // LMAIN | mtable |pid_table; name; shtable(new);
        lua_settable(LMAIN, -3);
        // LMAIN | mtable |pid_table;
        lua_pop(LMAIN, 2);
        // LMAIN | empty
        sem_post(semMAIN);
        // lstate | empty
        lua_newtable(lstate);
        lua_newtable(lstate);
        uint32_t p_sh;

        // lstate | newtable0; newtable1
        p_sh = create_shared_table_inner(lstate);

        lua_pushvalue(lstate, -1);
        // lstate | newtable0; newtable1;newtable1;
        lua_setglobal(lstate, shtable);

        lua_pushinteger(lstate, p_sh);
        lua_pushvalue(lstate, -2);
        // lstate | newtable0; newtable1;p_sh;newtable1;
        lua_settable(lstate, -4);
        lua_pop(lstate, 1);
        // lstate | newtable0;
        lua_setglobal(lstate, shtable_ref);

// Added [TABLE] shared_table_events
        lua_newtable(lstate);
        lua_setglobal(lstate, shtable_events);

        // lstate | empty

        sem_post(lsem);

        lua_pushlightuserdata(L, LUNAX_RE_CAST(lstate));
        lua_pushlightuserdata(L, LUNAX_RE_CAST(lsem));
        lua_pushinteger(L, p_sh);

        // L | luserdata_lstate; luserdata_lsem; light
    }
    return 0;
}

//In
// LMAIN | shtable
// Out
// LMAIN | empty
int delete_share_table_inner(lua_State* LMAIN)
{
    lua_State* lstate;
    sem_t* lsem;
    lua_pushinteger(LMAIN, SEM_KEY);
    lua_gettable(LMAIN, -2);
    // LMAIN | shtable; sem;
    lsem = LUNAX_CAST(lua_touserdata(LMAIN, -1));
    SEM_WAIT(lsem);
    lua_pushinteger(LMAIN, LSTATE_KEY);
    // LMAIN | shtable; sem; lstate_key
    lua_gettable(LMAIN, -3);
    // LMAIN | shtable; sem; lstate/nil
    if (lua_type(LMAIN, -1) == LUA_TLIGHTUSERDATA)
    {
        // LMAIN | shtable; sem; lstate
        lstate = LUNAX_CAST(lua_touserdata(LMAIN, -1));
        lua_pop(LMAIN, 3);

        lua_pushnil(lstate);
        lua_setglobal(lstate, shtable);
        lua_pushnil(lstate);
        lua_setglobal(lstate, shtable_ref);
        lua_pushnil(lstate);
        lua_setglobal(lstate, shtable_events);
        //!!!! lua_close(lstate);
    }
    else
    {
        // LMAIN | shtable; sem; nil;
        lua_pop(LMAIN, 3);
    }
    dbg("################# POST instead of DESTROY > %p \n",(void*)lsem);
    sem_post(lsem);
    //!!!!sem_destroy(lsem);

    lua_getglobal(LMAIN,voodoo);
    //LMAIN | voodoo
    lua_pushlightuserdata(LMAIN,(void*)LUNAX_RE_CAST(lstate));
    lua_gettable(LMAIN,-2);
    //LMAIN | voodoo; sh_table;
    lua_pushnil(LMAIN);
    lua_setfield(LMAIN,-2,"owner");
    lua_pop(LMAIN,2);

    return 0;
}
//In
// LMAIN | shtable
// Out
// LMAIN | empty
int real_delete_share_table_inner(lua_State* LMAIN)
{
    lua_State* lstate;
    sem_t* lsem;
    lua_pushinteger(LMAIN, SEM_KEY);
    lua_gettable(LMAIN, -2);
    // LMAIN | shtable; sem;
    lsem = LUNAX_CAST(lua_touserdata(LMAIN, -1));
    SEM_WAIT(lsem);
    lua_pushinteger(LMAIN, LSTATE_KEY);
    // LMAIN | shtable; sem; lstate_key
    lua_gettable(LMAIN, -3);
    // LMAIN | shtable; sem; lstate/nil
    if (lua_type(LMAIN, -1) == LUA_TLIGHTUSERDATA)
    {
        // LMAIN | shtable; sem; lstate
        lstate = LUNAX_CAST(lua_touserdata(LMAIN, -1));
        lua_pop(LMAIN, 3);
        lua_close(lstate);
    }
    else
    {
        // LMAIN | shtable; sem; nil;
        lua_pop(LMAIN, 3);
    }
    dbg("################# DESTROY > %p \n",(void*)lsem);
    sem_destroy(lsem);

    lua_getglobal(LMAIN,voodoo);
    lua_pushlightuserdata(LMAIN,(void*)LUNAX_RE_CAST(lstate));
    lua_pushnil(LMAIN);
    lua_settable(LMAIN,-3);
    lua_pop(LMAIN,1);

    return 0;
}

//In
//LMAIN |  pid_table
// Out
// LMAIN | shtable
int delete_shtable_inner(lua_State* LMAIN, const char* name)
{

    lua_pushstring(LMAIN, name);
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        //LMAIN |  pid_table ; shtable
        lua_pushvalue(LMAIN,-1);
        //LMAIN |  pid_table ; shtable;shtable;
        delete_share_table_inner(LMAIN);
        //LMAIN |  pid_table ; shtable
    }
    //LMAIN |  pid_table; shtable(or nil);
    lua_pushstring(LMAIN, name);
    lua_pushnil(LMAIN);
    lua_settable(LMAIN, -4);
    //LMAIN |  pid_table; shtable(or nil)
    lua_remove(LMAIN, -2);
    return 0;
}

//In
// L | empty
// LMAIN | empty
//Out
// L | nil
// LMAIN | empty
SH_TABLE_STATE handle_delete_share_table (lua_State *L, lua_State* lstate, SH_TABLE_STATE light_state, uint32_t light_origin)
{
    lua_State* LMAIN;
    sem_t* semMAIN;

    semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    LMAIN = get_main_table();
    lua_getglobal (LMAIN,voodoo);
    lua_pushlightuserdata(LMAIN,LUNAX_RE_CAST(lstate));
    lua_gettable(LMAIN,-2);
    if (lua_type(LMAIN,-1) == LUA_TTABLE)
    {
        //LMAIN | voodoo ; sh_table
        lua_pushvalue(LMAIN,-1);
        //LMAIN | voodoo ; sh_table ; sh_table
        if (light_state == SH_LIGHT_DELETED_ROOT)
        {
            dbg ("DELETED ROOT for lstate:%p",LUNAX_RE_CAST(lstate));
            //LMAIN | voodoo ; sh_table ; sh_table;
            clear_tid_in_sh_table(LMAIN);
            //LMAIN | voodoo ; sh_table ;
            try_delete_sh_table(LMAIN);
            //LMAIN | voodoo ;
            lua_pop(LMAIN,1);
            sem_post(semMAIN);
            null_for_proxy_tables(L, lstate);
        }
        else
        {
            dbg ("DELETED LEAF for lstate:%p",LUNAX_RE_CAST(lstate));
            //LMAIN | voodoo ; sh_table ; sh_table;
            dec_tid_in_sh_table(LMAIN);
            try_delete_sh_table(LMAIN);
            //LMAIN | voodoo ;
            lua_pop(LMAIN,1);
            sem_post(semMAIN);
            null_for_proxy_table(L, lstate,light_origin);
        }
    }
    else
    {
        sem_post(semMAIN);
    }
    lua_pushnil(L);
    return light_state;

}

//In
// L | light;key
//Out
// L | value
SH_TABLE_STATE getvalue_share_table(lua_State *L, lua_State* lstate, sem_t* lsem)
{
    int res = SH_LIGHT_EXIST;
    uint32_t l_light;
    uint32_t l_light_origin;
    SH_TABLE_STATE light_state;
    l_light_origin = lua_tointeger(L,-2);

    SEM_WAIT(lsem);
    lua_pushvalue(L, -2);
    // L | light;key;light
    //lstate |  empty
    light_state = light_to_table(L, lstate);
    if ((light_state == SH_LIGHT_DELETED_LEAF) || (light_state == SH_LIGHT_DELETED_ROOT))
    {
        // shared table was deleted
        // check, was it deleted leaf table or root table
        // L | light;key;
        //lstate |  nil
        lua_pop(lstate,1);
        sem_post(lsem);
        lua_pop(L,2);
        return handle_delete_share_table(L,  lstate, light_state, l_light_origin);

    }
    // L | light;key;
    //lstate | value
    if (light_state == SH_LIGHT_NONEXIST)
    //if (lua_type(lstate, -1) != LUA_TTABLE)
    {
        lua_pop(lstate, 1);
        sem_post(lsem);
        lua_pop(L, 2);
        lua_pushnil(L);
        return SH_LIGHT_NONEXIST;
    }
    //lstate | shared_table
    xcopy1(L, lstate, -1);
    // L | light;key
    //lstate | shared_table;key;
    lua_pop(L, 2);
    // L | empty
    //lstate | shared_table; key;

//---------------------------------
#ifndef  SH_SIMPLE
    //lstate | shared_table; key; shared_table;
    lua_pushvalue(lstate,-2);
    sync_diff_index(lstate);
#endif
//---------------------------------

    lua_gettable(lstate, -2);
    //lstate | shared_table; value_shared_table;
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        lua_getglobal(lstate, shtable_ref);
        //lstate | shared_table; value_shared_table;shtable_ref;
        lua_pushvalue(lstate, -2);
        //lstate | shared_table; value_shared_table; shtable_ref; value_shared_table;
        l_light = table_to_light(lstate, L);
        //lstate | shared_table; value_shared_table; shtable_ref;
        // L | light
        lua_pushinteger(lstate, l_light);
        //lstate | shared_table; value_shared_table; shtable_ref; light;
        lua_pushvalue(lstate, -3);
        //lstate | shared_table; value_shared_table; shtable_ref; light;value_shared_table;
        lua_settable(lstate, -3);
        //lstate | shared_table; value_shared_table; shtable_ref;
        lua_pop(lstate, 3);
        res = SH_LIGHT_EXIST;
    }
    else
    {
        xcopy1(lstate, L, -1);
        //lstate | shared_table; value_shared_table;
        // L | value
        lua_pop(lstate, 2);
        res = SH_LIGHT_NONEXIST;
    }

    sem_post(lsem);
    //lstate | empty;
    // L | value
    return res;
}

//In
// L | light;key
//Out
// L | value
SH_TABLE_STATE getvalue_share_table_nosem(lua_State *L, lua_State* lstate)
{
    int res = 0;
    uint32_t l_light;
    uint32_t l_light_origin;
    SH_TABLE_STATE light_state;
    l_light_origin = lua_tointeger(L,-2);

    lua_pushvalue(L, -2);
    // L | light;key;light
    light_state = light_to_table(L, lstate);
    if ((light_state == SH_LIGHT_DELETED_LEAF) || (light_state == SH_LIGHT_DELETED_ROOT))
    {
        // shared table was deleted
        // check, was it deleted leaf table or root table
        // L | light;key;
        //lstate |  nil
        lua_pop(lstate,1);
        lua_pop(L,2);
        return handle_delete_share_table(L,  lstate, light_state, l_light_origin);

    }
    // L | light;key;
    //lstate | value
    if (lua_type(lstate, -1) != LUA_TTABLE)
    {
        lua_pop(lstate, 1);
        lua_pop(L, 2);
        lua_pushnil(L);
        return 0;
    }
    //lstate | shared_table
    xcopy1(L, lstate, -1);
    // L | light;key
    //lstate | shared_table;key;
    lua_pop(L, 2);
    // L | empty
    //lstate | shared_table; key;
//---------------------------------
#ifndef  SH_SIMPLE
    //lstate | shared_table; key; shared_table;
    lua_pushvalue(lstate,-2);
    sync_diff_index(lstate);
#endif
//---------------------------------
    lua_gettable(lstate, -2);
    //lstate | shared_table; value_shared_table;
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        lua_getglobal(lstate, shtable_ref);
        //lstate | shared_table; value_shared_table;shtable_ref;
        lua_pushvalue(lstate, -2);
        //lstate | shared_table; value_shared_table; shtable_ref; value_shared_table;
        l_light = table_to_light(lstate, L);
        //lstate | shared_table; value_shared_table; shtable_ref;
        // L | light
        lua_pushinteger(lstate, l_light);
        //lstate | shared_table; value_shared_table; shtable_ref; light;
        lua_pushvalue(lstate, -3);
        //lstate | shared_table; value_shared_table; shtable_ref; light;value_shared_table;
        lua_settable(lstate, -3);
        //lstate | shared_table; value_shared_table; shtable_ref;
        lua_pop(lstate, 3);
        res = 1;
    }
    else
    {
        xcopy1(lstate, L, -1);
        //lstate | shared_table; value_shared_table;
        // L | value
        lua_pop(lstate, 2);
    }

    //lstate | empty;
    // L | value
    return res;
}

//In
// L | light;key
// lstate | empty
//Out
// L | value
// lstate | shared_table; key
int getvalue_share_table_for_write(lua_State *L, lua_State* lstate, sem_t* lsem,
        int first)
{
    int light;
    int res = 0;
    if (first == 1)
    {
        SEM_WAIT(lsem);
        //lstate | empty
    }
    else
    {
        lua_pop(lstate, 2);
        //lstate | empty
    }

    lua_pushvalue(L, -2);
    // L | light;key;light
    light_to_table(L, lstate);
    // L | light;key;
    //lstate | shared_table
    xcopy1(L, lstate, -1);
    // L | light;key
    //lstate | shared_table;key;
    lua_pop(L, 2);
    // L | empty
    //lstate | shared_table; key;
//---------------------------------
#ifndef  SH_SIMPLE
    //lstate | shared_table; key; shared_table;
    lua_pushvalue(lstate,-2);
    sync_diff_index(lstate);
#endif
//---------------------------------
    lua_pushvalue(lstate, -1);
    //lstate | shared_table; key;key
    lua_gettable(lstate, -3);
    //lstate | shared_table; key ;value_shared_table;
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        lua_pushvalue(lstate, -1);
        //lstate | shared_table; key ;value_shared_table;value_shared_table;
        table_to_light(lstate, L);
        //lstate | shared_table; key;value_shared_table;
        // L | light
        lua_getglobal(lstate, shtable_ref);
        //lstate | shared_table; key; value_shared_table; shtable_ref;
        light = lua_tointeger(L, -1);
        lua_pushinteger(lstate, light);
        //lstate | shared_table; key;value_shared_table; shtable_ref;light;
        lua_pushvalue(lstate, -3);
        //lstate | shared_table; key;shtable_ref;light;value_shared_table;;
        lua_settable(lstate, -3);
        //lstate | shared_table; key;value_shared_table; shtable_ref;
        lua_pop(lstate, 2);
        res = 1;
    }
    else
    {
        xcopy1(lstate, L, -1);
        //lstate | shared_table; key; value_shared_table;
        // L | value
        lua_pop(lstate, 1);
    }

    //sem_post(lsem); - it is not a finish of this transaction !!!!
    return res;
}

//In
// lstate | shared_table; key;
//Out
// lstate | shared_table; key_next; value_next;
// OR
// (return 0) lstate | shared_table;
static int lua_next_i(lua_State *lstate, int idx)
{
    int next_key;
    if (lua_isnumber(lstate, -1))
    {
        next_key = lua_tointeger(lstate, -1) + 1;
    }
    else
    {
        next_key = 1;
    }
    lua_pop(lstate, 1);
    lua_pushinteger(lstate, next_key);
    lua_rawgeti(lstate,-2,next_key);
    // lstate | shared_table; next_key; value(or nil)
    if(lua_isnil(lstate, -1))
    {
        lua_pop(lstate, 2);
        return 0;
    }
    return 2;
}

//In
// lstate | shared_table; key;
//Out
// lstate | shared_table; key_next; value_next;
// OR
// (return 0) lstate | empty
int lua_next_ext(lua_State *lstate, int idx, uint8_t iflag)
{
    int res;
    if (lua_getmetatable(lstate, -2) == 0)
    {
        lua_pop(lstate, 2);
        return 0;
    }
    // lstate | shared_table; key; metatable
    lua_pushstring(lstate, "__index");
    lua_gettable(lstate, -2);
    if (!lua_istable(lstate, -1))
    {
        lua_pop(lstate, 4);
        return 0;
    }
    // lstate | shared_table; key; metatable; table(shared_table_diff);
    lua_pushvalue(lstate, -3);
    // lstate | shared_table; key; metatable; table(shared_table_diff); key
    lua_remove(lstate, -3);
    // lstate | shared_table; key; table(shared_table_diff); key
    res = 0;
    if(lua_isnil(lstate, -1))
    {
    	res = (iflag)?lua_next_i(lstate, -2):lua_next(lstate, -2);
    }
    else
    {
    	 // lstate | shared_table; key; table(shared_table_diff); key
    	lua_pushvalue(lstate, -1);
    	// lstate | shared_table; key; table(shared_table_diff); key; key
    	lua_rawget(lstate, -3);
    	// lstate | shared_table; key; table(shared_table_diff); key; value
    	if(lua_isnil(lstate, -1) == 0)
    	{
    		lua_pop(lstate, 1);
    		res = (iflag)?lua_next_i(lstate, -2):lua_next(lstate, -2);
    	}
    	else
    	{
    		lua_pop(lstate, 2);
    	}
    }
    if (res == 0)
    {
        // lstate | shared_table; key; table(shared_table_diff);
        lua_pushvalue(lstate, -2);
        lua_remove(lstate, -3);
        // lstate | shared_table; table(shared_table_diff); key;
        lua_pushnil(lstate);
        lua_pushnil(lstate);
        do
        {
            lua_pop(lstate, 2);
            // lstate | shared_table; table(shared_table_diff); table(shared_table_diff)_nextkey;
            res = lua_next_ext(lstate, -2, iflag);
            if (res == 0)
            {
                // lstate | shared_table;
                lua_pop(lstate, 1);
                return 0;
            }
            // lstate | shared_table; table(shared_table_diff); table(shared_table_diff)_nextkey; table(shared_table_diff)_nexvalue;
            lua_pushvalue(lstate, -2);
            lua_rawget(lstate, -4);
        } while (lua_isnil(lstate, -1) == 0);
        lua_pop(lstate, 1);
        // lstate | shared_table; table(shared_table_diff); table(shared_table_diff)_nextkey; table(shared_table_diff)_nexvalue;
        lua_remove(lstate, -3);
        // lstate | shared_table; table(shared_table_diff)_nextkey; table(shared_table_diff)_nexvalue;
        return 2;
    }
    else
    {
        // lstate | shared_table; key; table(shared_table_diff); table(shared_table_diff)_nextkey; table(shared_table_diff)_nexvalue;
        lua_remove(lstate, -3);
        lua_remove(lstate, -3);
        // lstate | shared_table; table(shared_table_diff)_nextkey; table(shared_table_diff)_nexvalue;
    }
    return 2;
}

//In
// L | light;key
//Out
// L | key; value
SH_TABLE_STATE next_share_table(lua_State *L, lua_State *lstate, sem_t *lsem, uint8_t iflag)
{
    int res = SH_LIGHT_EXIST;
    int res_next;
    uint32_t l_light;
    uint32_t l_light_origin;
    SH_TABLE_STATE light_state;
    l_light_origin = lua_tointeger(L, -2);

    SEM_WAIT(lsem);
    lua_pushvalue(L, -2);
    // L | light;key;light
    //lstate |  empty
    light_state = light_to_table(L, lstate);
    if ((light_state == SH_LIGHT_DELETED_LEAF) || (light_state == SH_LIGHT_DELETED_ROOT))
    {
        // shared table was deleted
        // check, was it deleted leaf table or root table
        // L | light;key;
        //lstate |  nil
        lua_pop(lstate, 1);
        sem_post(lsem);
        lua_pop(L, 2);
        return handle_delete_share_table(L, lstate, light_state, l_light_origin);
    }
    // L | light;key;
    //lstate | value
    if (light_state == SH_LIGHT_NONEXIST)
    //if (lua_type(lstate, -1) != LUA_TTABLE)
    {
        lua_pop(lstate, 1);
        sem_post(lsem);
        lua_pop(L, 2);
        lua_pushnil(L);
        return SH_LIGHT_NONEXIST;
    }
    //lstate | shared_table
    xcopy1(L, lstate, -1);
    // L | light;key
    //lstate | shared_table;key;
    lua_pop(L, 2);
    // L | empty
    //lstate | shared_table; key;

//---------------------------------
#ifndef SH_SIMPLE
    //lstate | shared_table; key; shared_table;
    lua_pushvalue(lstate, -2);
    sync_diff_index(lstate);
    //lstate | shared_table; key;
    lua_pushvalue(lstate,-2);
    //lstate | shared_table; key; shared_table;
    lua_insert(lstate,-2);
    //lstate | shared_table; shared_table; key;
    res_next = lua_next_ext(lstate, -2, iflag);
    if (res_next != 0)
    {
    	//lstate | shared_table; shared_table; key_shared_table; value_shared_table;
    	lua_remove(lstate,-4);
    	//lstate | shared_table; key_shared_table; value_shared_table;
    }
#else
    //lstate | shared_table; key;
    res_next = (iflag)?lua_next_i(lstate, -2):lua_next(lstate, -2);
#endif
    //---------------------------------
    while (res_next)
    {
        //lstate | shared_table; key_shared_table; value_shared_table;
        if (lua_type(lstate, -2) == LUA_TSTRING)
        {
            char *keystr = lua_tostring(lstate, -2);
            if ((strcmp(keystr, "__LIGHT") == 0) ||
                (strcmp(keystr, "__ETYPE") == 0) ||
                (strcmp(keystr, "__ESIZE") == 0))
            {
                lua_pop(lstate, 1);
#ifndef SH_SIMPLE
                //lstate | shared_table; key;
                lua_pushvalue(lstate,-2);
                //lstate | shared_table; key; shared_table;
                lua_insert(lstate,-2);
                //lstate | shared_table; shared_table; key;
                res_next = lua_next_ext(lstate, -2, iflag);
                if (res_next != 0)
                {
                	//lstate | shared_table; shared_table; key_shared_table; value_shared_table;
                	lua_remove(lstate,-4);
                	//lstate | shared_table; key_shared_table; value_shared_table;
                }
#else
                res_next = (iflag)?lua_next_i(lstate, -2):lua_next(lstate, -2);
#endif
                continue;
            }
        }
        break;
    }
    if (res_next == 0)
    {
        //lstate | shared_table;
        lua_pop(lstate, 1);
        lua_pushnil(L);
        lua_pushnil(L);
        res = SH_LIGHT_NONEXIST;
    }
    else
    {
        //lstate | shared_table; key_shared_table; value_shared_table;
        // L empty
        xcopy1(lstate, L, -2);
        //lstate | shared_table; key_shared_table; value_shared_table;
        // L | key
        if (lua_type(lstate, -1) == LUA_TTABLE)
        {
            lua_getglobal(lstate, shtable_ref);
            //lstate | shared_table; key_shared_table; value_shared_table;shtable_ref;
            lua_pushvalue(lstate, -2);
            //lstate | shared_table; key_shared_table;value_shared_table; shtable_ref; value_shared_table;
            l_light = table_to_light(lstate, L);
            //lstate | shared_table; key_shared_table; value_shared_table; shtable_ref;
            // L | key; light
            lua_pushinteger(lstate, l_light);
            //lstate | shared_table; key_shared_table; value_shared_table; shtable_ref; light;
            lua_pushvalue(lstate, -3);
            //lstate | shared_table; key_shared_table; value_shared_table; shtable_ref; light;value_shared_table;
            lua_settable(lstate, -3);
            //lstate | shared_table; key_shared_table; value_shared_table; shtable_ref;
            lua_pop(lstate, 4);
            res = SH_LIGHT_EXIST;
            DBG_STACK(L, "L1");
        }
        else
        {
            xcopy1(lstate, L, -1);
            //lstate | shared_table; key_shared_table; value_shared_table;
            // L | key; value
            lua_pop(lstate, 3);
            res = SH_LIGHT_NONEXIST;
        }
    }

    sem_post(lsem);
    //lstate | empty;
    // L | key; value
    return res;
}

//In
// lstate | value_table;
//Out
// lstate | empty
int shared_table_ref_remove(lua_State *lstate)
{
    int res = 0;
    uint32_t old_light;
    // lstate | value_table;
    lua_pushnil(lstate); /* first key */
    while (lua_next(lstate, -2) != 0)
    {
        // lstate | value_table;shtable_ref;value_table; key;value
        if (lua_type(lstate, -1) == LUA_TTABLE)
        {
            res += shared_table_ref_remove(lstate);
        }
        else
        {
            lua_pop(lstate, 1);
        }
        // lstate | value_table;shtable_ref;value_table; key;
    }
    // lstate | value_table;
    old_light = getlight(lstate);
    // lstate | empty
    if (old_light)
    {
        dbg("remove light from shared_table_ref :%d\n", old_light);
        res += 1;
        lua_getglobal(lstate, shtable_ref);
        lua_pushinteger(lstate, old_light);
        // lstate | shtable_ref; light;
        lua_pushnil(lstate);
        lua_settable(lstate, -3);
        // lstate |  shtable_ref;
        lua_pop(lstate, 1);
        // lstate | empty
    }
    return res;
}

//In
// L | state;
// lstate | shared_table; key
//Out
// L | empty
// lstate | empty
int setvalue_share_table_clean(lua_State *L, sem_t* sem)
{
    lua_State* lstate;

    //SEM_WAIT(sem); it was called in getvalue_share_table_for_write !!!!!
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    // L | empty;
    lua_pop(lstate, 2);
    // lstate | empty
    sem_post(sem);
    return 0;
}

//In
// L | param1 | param2; state;
// lstate | shared_table; key
//Out
// L | state
// lstate | shared_table; key
int setvalue_share_table_create(lua_State *L, sem_t* sem)
{
    lua_State* lstate;
    //SEM_WAIT(sem); it was called in getvalue_share_table_for_write !!!!!
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    // L | param1 ; param2; state
    lua_insert(L, -3);
    // L | state; param1 ; param2;
    //if (lua_type(L, -1) == LUA_TTABLE)
    {
        // lstate | shared_table; key
        lua_pushvalue(lstate, -1);
        // lstate | shared_table; key; key;
        lua_gettable(lstate, -3);
        // lstate | shared_table; key; value_table;

        if (lua_type(lstate, -1) == LUA_TTABLE)
        {
            if (shared_table_ref_remove(lstate))
            {
                lua_gc(lstate, LUA_GCCOLLECT, 0);
                //lua_gc(lstate,LUA_GCSETPAUSE,150);
                //lua_gc(lstate,LUA_GCSETSTEPMUL,200);
                //lua_gc(lstate,LUA_GCSTEP,0);
            }
        }
        else
        {
            lua_pop(lstate, 1);
        }
    }
    // lstate | shared_table; key;
    // L | state; param1 ; param2;
    xcopy1(L, lstate, lua_gettop(L));
    // lstate | shared_table; key; param2;
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
//		lua_newtable(lstate);
//		{
//		  // serge_p
//	      lua_pushstring(lstate, "__gc");lua_pushcfunction(lstate, gc_test_clean);lua_settable(lstate, -3);
//		  lua_pushstring(lstate, "shared table 1");
//		  lua_setfield(lstate,-2,"name");
//		}
//		lua_setmetatable(lstate,-2);
    }
    // lstate | shared_table; key; param2;
    lua_pushvalue(lstate, -2);
    lua_pushvalue(lstate, -2);
    // lstate | shared_table; key; param2; key; param2;
    lua_settable(lstate, -5);
    lua_pop(lstate, 1);
    lua_pop(L, 2);
    //sem_post(sem); must call setvalue_share_table_clean
    return 0;
}

//In
// L | param1 | param2; state;
// lstate | shared_table; key
//Out
// L | state
// lstate | shared_table; key
int setvalue_share_table_merge(lua_State *L, sem_t* sem)
{
    lua_State* lstate;
    //SEM_WAIT(sem); it was called in getvalue_share_table_for_write !!!!!
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_insert(L, -3);
    // L | state; param1 ; param2;
    // lstate | shared_table; key;
    xcopy1(L, lstate, lua_gettop(L));
    lua_pop(L, 2);
    // L | state
    // lstate | shared_table; key; param2;
    if (lua_type(lstate, -1) == LUA_TTABLE)
    {
        // lstate | shared_table; key; param2_table;
        xmerge(lstate);
        // lstate | shared_table; key; param2_table;
        lua_pop(lstate, 1);

        //printf("==========> %d",lua_gc(lstate, LUA_GCCOUNT, 0));
        lua_gc(lstate, LUA_GCCOLLECT, 0);
    }
    else
    {
        // lstate | shared_table; key; param2;
        lua_pushvalue(lstate, -2);
        lua_pushvalue(lstate, -2);
        // lstate | shared_table; key; param2; key; param2;
        lua_settable(lstate, -5);
        lua_pop(lstate, 1);
    }
    // lstate | shared_table; key;
    //sem_post(sem); must call setvalue_share_table_clean
    return 0;
}

//In
// L | light;
// lstate | empty
//Out
// L | empty
// lstate | empty
int setstrategy_share_table(lua_State *L, lua_State* lstate, sem_t* lsem, int strategy, int strategy_size)
{
    int res = 0;
    int old_etype;
    SEM_WAIT(lsem);
    // L | light;
    light_to_table(L, lstate);
    // L | empty
    //lstate | shared_table
    {
//
        lua_getfield(lstate, -1, "__ETYPE");
        old_etype = lua_tointeger(lstate,-1);
        lua_pop(lstate,1);
#ifndef SH_SIMPLE
        if ((strategy == EV_ASYNC_SYMPLE_UPDATE) || (strategy == EV_SYNC_SYMPLE_UPDATE))
        {
            if ((old_etype == EV_SYNC_PIPE_UPDATE) || (old_etype == EV_ASYNC_PIPE_UPDATE))
            {
                repack_shared_table_diff(lstate,1);
                set_maxdiff_in_shared_events(lstate, old_etype, 0);
            }
        }
        if ((strategy == EV_SYNC_PIPE_UPDATE) || (strategy == EV_ASYNC_PIPE_UPDATE))
        {
            if ((old_etype == EV_SYNC_PIPE_UPDATE) || (old_etype == EV_ASYNC_PIPE_UPDATE))
            {
                repack_shared_table_diff(lstate,strategy_size);
                set_maxdiff_in_shared_events(lstate, old_etype, strategy_size-1);
            }
        }
#endif
        //
        // lstate |  shared_table;
        lua_pushinteger(lstate, strategy);
        lua_setfield(lstate, -2, "__ETYPE");

        lua_pushinteger(lstate, strategy_size);
        lua_setfield(lstate, -2, "__ESIZE");
    }
    // L | empty
    //lstate | shared_table
    lua_pop(lstate, 1);
    sem_post(lsem);
    return res;
}

//In
// L | light;
// lstate | empty
//Out
// L | event_strategy
// lstate | empty
int getstrategy_share_table(lua_State *L, lua_State* lstate, sem_t* lsem)
{
    int res = 0;
    SEM_WAIT(lsem);
    // L | light;
    light_to_table(L, lstate);
    // L | empty
    //lstate | shared_table
    lua_getfield(lstate, -1, "__ETYPE");
    // L | empty
    //lstate | shared_table; event_strategy
    xcopy1(lstate, L, -1);
    lua_pop(lstate, 2);
    // L | event_strategy
    //lstate | empty
    sem_post(lsem);
    return res;
}

//In
// L | light;
// lstate | empty
//Out
// L | event_strategy; event_startetgy_size
// lstate | empty
int getstrategy_share_table_inner(lua_State *L, lua_State* lstate)
{
    int res = 0;
    // L | light;
    light_to_table(L, lstate);
    // L | empty
    //lstate | shared_table
    lua_getfield(lstate, -1, "__ETYPE");
    lua_getfield(lstate, -2, "__ESIZE");
    // L | empty
    //lstate | shared_table; event_strategy; event_strategy_size
    xcopy1(lstate, L, -2);
    xcopy1(lstate, L, -1);
    lua_pop(lstate, 3);
    // L | event_strategy;  event_strategy_size
    //lstate | empty
    return res;
}
//In
// L | table;
//Out
// L| table
char* show_table_to_line(lua_State* L, char* str)
{
    char* start_str = str;
    /* table is in the stack */
    lua_pushnil(L); /* first key */
    while (lua_next(L, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
//		       printf("%s - %s\n",
//		              lua_typename(L, lua_type(L, -2)),
//		              lua_typename(L, lua_type(L, -1)));
        if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TNUMBER))
            sprintf(str, "[%"LUA_INTEGER_FRMLEN"d]:%"LUA_INTEGER_FRMLEN"d ;", lua_tointeger(L, -2),
                    lua_tointeger(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TSTRING))
            sprintf(str, "[%"LUA_INTEGER_FRMLEN"d]:%s ;", lua_tointeger(L, -2),
                    lua_tostring(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TTABLE))
            sprintf(str, "[%"LUA_INTEGER_FRMLEN"d]:%s ;", lua_tointeger(L, -2),
                    lua_typename(L, lua_type(L, -1)));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TLIGHTUSERDATA))
            sprintf(str, "[%s]:%p ;", lua_tostring(L, -2),
                    lua_touserdata(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TLIGHTUSERDATA))
            sprintf(str, "[%"LUA_INTEGER_FRMLEN"d]:%p ;", lua_tointeger(L, -2),
                    lua_touserdata(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TNUMBER))
            sprintf(str, "[%s]:%"LUA_INTEGER_FRMLEN"d ;", lua_tostring(L, -2),
                    lua_tointeger(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TSTRING))
            sprintf(str, "[%s]:%s ;", lua_tostring(L, -2), lua_tostring(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TTABLE))
            sprintf(str, "[%s]:%s;", lua_tostring(L, -2),
                    lua_typename(L, lua_type(L, -1)));
        else
            sprintf(str, "unknown types: [%s]:%s ;",
                    lua_typename(L, lua_type(L, -2)),
                    lua_typename(L, lua_type(L, -1)));
        /* removes 'value'; keeps 'key' for next iteration */
        str += strlen(str);
        lua_pop(L, 1);
        // L | table; key ;
    }
    return start_str;
}

//In
// L | table
//Out
// L | empty
void show_table(lua_State* L)
{
    char* str[1024] =
    { 0 };
    /* table is in the stack */
    lua_pushnil(L); /* first key */
    while (lua_next(L, -2) != 0)
    {
        /* uses 'key' (at index -2) and 'value' (at index -1) */
//		       printf("%s - %s\n",
//		              lua_typename(L, lua_type(L, -2)),
//		              lua_typename(L, lua_type(L, -1)));
        // L | table; key ; value
        if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TNUMBER))
            printf("[%"LUA_INTEGER_FRMLEN"d] = %"LUA_INTEGER_FRMLEN"d\n", lua_tointeger(L, -2), lua_tointeger(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TSTRING))
            printf("[%"LUA_INTEGER_FRMLEN"d] = %s\n", lua_tointeger(L, -2), lua_tostring(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TTABLE))
            printf("[%"LUA_INTEGER_FRMLEN"d] = {%s}\n", lua_tointeger(L, -2),
                    show_table_to_line(L, (char*) str));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TLIGHTUSERDATA))
            printf("[%s] = %p\n", lua_tostring(L, -2), lua_touserdata(L, -1));
        else if ((lua_type(L, -2) == LUA_TNUMBER)
                && (lua_type(L, -1) == LUA_TLIGHTUSERDATA))
            printf("[%"LUA_INTEGER_FRMLEN"d] = %p\n", lua_tointeger(L, -2), lua_touserdata(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TNUMBER))
            printf("[%s] = %"LUA_INTEGER_FRMLEN"d\n", lua_tostring(L, -2), lua_tointeger(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TSTRING))
            printf("[%s] = %s\n", lua_tostring(L, -2), lua_tostring(L, -1));
        else if ((lua_type(L, -2) == LUA_TSTRING)
                && (lua_type(L, -1) == LUA_TTABLE))
            printf("[%s] = {%s}\n", lua_tostring(L, -2),
                    show_table_to_line(L, (char*) str));
        else
            printf("unknown types: %s - %s\n", lua_typename(L, lua_type(L, -2)),
                    lua_typename(L, lua_type(L, -1)));
        /* removes 'value'; keeps 'key' for next iteration */
        lua_pop(L, 1);
        // L | table; key
    }
    lua_pop(L, 1);
}

// TO DO UPDATE !!!!!
//In
// L | pid_number; string
//Out
// L | empty
int dbg_show_shared_table(lua_State* L)
{
    const char* name;
    int num;

    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    lua_State* LMAIN = get_main_table();
    lua_State* lstate;
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        // L | name
        name = lua_tostring(L, -1);
        lua_pop(L, 1);
        if (strcmp(name, mtable) == 0)
        {
            lua_getglobal(LMAIN, mtable);
            // LMAIN | mhtable
            printf("\n---------- %s[] ------------\n", mtable);
            show_table(LMAIN);
            // LMAIN | empty
        }
        else
        {
            if (lua_type(L, -2) == LUA_TNUMBER)
            {
                num = lua_tonumber(L, -2);
                lua_getglobal(LMAIN, mtable);
                lua_pushnumber(LMAIN, num);
                lua_gettable(LMAIN, -2);
                if (lua_type(LMAIN, -1) == LUA_TTABLE)
                {
                    lua_pushstring(LMAIN, name);
                    lua_gettable(LMAIN, -2);
                    if (lua_type(LMAIN, -1) == LUA_TTABLE)
                    {
                        lua_pushinteger(LMAIN, LSTATE_KEY);
                        lua_gettable(LMAIN, -2);
                        if (lua_type(LMAIN, -1) != LUA_TLIGHTUSERDATA)
                        {
                            luaL_error(L, "Error: BAD_L_STATE_ARG :%d[%s]  \n",
                                    num, name);
                        }
                        lstate = LUNAX_CAST(lua_touserdata(LMAIN, -1));
                    }
                    sem_post(semMAIN);
                    lua_getglobal(lstate, shtable);
                    if (lua_type(lstate, -1) != LUA_TTABLE)
                    {
                        luaL_error(L, "Error: BAD_SHARED TABLE_ARG :%d[%s]  \n",
                                num, name);
                    }
                    printf("\n-----------%d[%s] ------------\n", num, name);
                    show_table(lstate);
                }
                else
                {
                    sem_post(semMAIN);
                    luaL_error(L, "Error: BAD_NUMBER_ARG :%d \n", num);
                }
            }
        }
    }
    else
    {
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            num = lua_tonumber(L, -1);
            lua_getglobal(LMAIN, mtable);
            lua_pushnumber(LMAIN, num);
            lua_gettable(LMAIN, -2);
            if (lua_type(LMAIN, -1) == LUA_TTABLE)
            {
                sem_post(semMAIN);
                printf("\n---------- %d[] ------------\n", num);
                show_table(LMAIN);
            }
            else
            {
                sem_post(semMAIN);
                luaL_error(L, "Error: BAD_NUMBER_ARG :%d \n", num);
            }
        }
        else
        {
            sem_post(semMAIN);
            luaL_error(L, "Error: BAD_ARG\n");
        }
    }
    printf("--------------------------------\n");
    return 0;
}
