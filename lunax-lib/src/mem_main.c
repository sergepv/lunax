/*
** mem_main.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "lunax.h"
#include <shared_pmpa.h>
#include <semaphore.h>
#include "deepcopy.h"

#include "mem_main.h"
#include "pid_table.h"
#include "shared_table.h"
#include "select_table.h"

#define LUA_STATE_MAIN_SIZE 32

#if defined(__i386__)
extern uint32_t G_LUNAX_ADDR;
#elif defined(__x86_64__)
extern uint64_t G_LUNAX_ADDR;
#elif defined(__arm__)
extern uint32_t G_LUNAX_ADDR;
#else
#error "Unknown platform architecture."
#endif

//static void *l_alloc1 (void *ud, void *ptr, size_t osize,
//                                           size_t nsize) {
//  (void)ud;  (void)osize;  /* not used */
//  if (nsize == 0) {
//    free(ptr);
//    return NULL;
//  }
//  else
//    return realloc(ptr, nsize);
//}

lua_State* create_main_table()
{
    lua_State* ls;
    ls = lua_newstate((lua_Alloc) PMPA_DETECT, (void*) G_LUNAX_ADDR); //PMPA_DETECT or l_alloc1
    lua_newtable(ls);
    lua_setglobal(ls, mtable);
    lua_newtable(ls);
    lua_setglobal(ls, pid2pid);
    lua_newtable(ls);
    lua_setglobal(ls, voodoo);
    return ls;
}

//In
//LMAIN |  empty
// Out
// LMAIN | empty
int delete_main_table()
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    //LMAIN |  mtable

    lua_pushnil(LMAIN); /* first key */
    //LMAIN |  mtable; nil
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN |  mtable; key;value;
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            //LMAIN |  mtable; key;pid_table
            delete_pid_table_inner(LMAIN);
            //LMAIN |  mtable; key;
        }
        else
        {
            lua_pop(LMAIN, 1);
        }
        lua_pushvalue(LMAIN, -1);
        lua_pushnil(LMAIN);
        //LMAIN |  mtable; key; key;nil;
        lua_settable(LMAIN, -4);
        //LMAIN |  mtable; key;
    }
    lua_pushnil(LMAIN);
    lua_setglobal(LMAIN, mtable);
    lua_pop(LMAIN, 1);
    force_delete_sh_table(LMAIN);
    lua_close(LMAIN);

    sem_post(semMAIN);

    sem_destroy(semMAIN);
    return 0;
}

//In
//L | empty
//LMAIN |  empty
// Out
//L | empty
// LMAIN | empty
int delete_from_main_table(lua_State* L, pid_t pid, const char* name)
{
    lua_State* lstate = NULL;
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);

    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    //LMAIN | mtable
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        //LMAIN | mtable
        lua_pushinteger(LMAIN, pid);
        lua_gettable(LMAIN, -2);
        //LMAIN | mtable; pid_table
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            delete_shtable_inner(LMAIN, name);
            // LMAIN | mtable; shtable;
            lua_pushvalue(LMAIN,-1);
            // LMAIN | mtable; shtable; shtable;
            lstate = get_lstate(LMAIN);
            lua_getglobal (LMAIN,voodoo);
            // LMAIN | mtable; shtable; voodoo
            lua_insert(LMAIN,-2);
            // LMAIN | mtable; voodoo; shtable;
            lua_pushvalue(LMAIN,-1);
            dbg (" SELF DELETED ROOT");
            //LMAIN | mtable; voodoo ; sh_table ; sh_table;
            clear_tid_in_sh_table(LMAIN);
            //LMAIN | mtable; voodoo ; sh_table ;
             try_delete_sh_table(LMAIN);
            //LMAIN | mtable; voodoo ;

            lua_pop(LMAIN, 2);
        }
        else
        {
            lua_pop(LMAIN, 2);
        }
    }
    else
    {
        lua_pop(LMAIN, 1);
    }
    //LMAIN | empty
    sem_post(semMAIN);
    if (lstate)
    {
        null_for_proxy_tables(L, lstate);
    }
    return 0;
}

//In
//L |  empty
// LMAIN | empty
// Out
// L| table
// LMAIN | empty
int show_main_table(lua_State* L)
{
    int indx = 1;
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        lua_newtable(L);
        // L | table0
    }
    else
    {
        return 0;
    }
    //LMAIN mtable
    lua_pushnil(LMAIN); /* first key */
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN mtable; key(pid_number); value(pid_table);
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            //LMAIN mtable; key(pid_number); pid_table;
            lua_pushinteger(L, indx++);
            lua_newtable(L);
            //L | table0; indx ; table1
            {
                //LMAIN mtable; key(pid_number); pid_table;
                xcopy1(LMAIN, L, -2);

                //L | table0; indx ; table1; key(pid_number)
                lua_setfield(L, -2, "pid");

                //LMAIN mtable; key; pid_table;
                lua_getfield(LMAIN, -1, "name");
                //LMAIN mtable; key; pid_table; name;
                xcopy1(LMAIN, L, -1);
                //L | table0; indx ; table1; name
                lua_setfield(L, -2, "name");
                //L | table0; indx ; table1;
            }
            lua_settable(L, -3);
            //L | table0;
            //LMAIN mtable; key; pid_table; name;
            lua_pop(LMAIN, 1);
        }
        /* removes 'value'; keeps 'key' for next iteration */
        lua_pop(LMAIN, 1);
    }
    lua_pop(LMAIN, 1);
    sem_post(semMAIN);
    return 1;
}

void* get_base_addr()
{
    return (void*) G_LUNAX_ADDR;
}

lua_State* get_main_table()
{
    uint8_t* addr = (uint8_t*) G_LUNAX_ADDR;
    addr += pmpa_get_offset();
#ifdef ARCH_32BIT
    uint32_t* base = (uint32_t*) addr;
#else
    uint64_t* base = (uint64_t*) addr;
#endif
    return (lua_State*) (LUNAX_CAST(*base));
}

sem_t* get_main_sem()
{
    uint8_t* addr = (uint8_t*) G_LUNAX_ADDR;
    addr += pmpa_get_offset();
#ifdef ARCH_32BIT
    uint32_t* base = (uint32_t*) (addr + sizeof(uint32_t*) + sizeof(uint32_t*));
#else
    uint64_t* base = (uint64_t*)(addr + sizeof(uint64_t*) + sizeof(uint64_t*));
#endif
    return (sem_t*) base;
}

uint32_t get_inc_main_counter()
{
#ifdef ARCH_32BIT
    uint32_t* pres;
    uint8_t* addr = (uint8_t*) G_LUNAX_ADDR;
    addr += pmpa_get_offset();
    uint32_t* base = (uint32_t*) (addr + sizeof(uint32_t*));
    pres = (uint32_t*) base;
#else
    uint64_t* pres;
    uint8_t* addr = (uint8_t* )G_LUNAX_ADDR;
    addr+= pmpa_get_offset();
    uint64_t* base = (uint64_t*)(addr + sizeof(uint64_t*));
    pres = (uint64_t*)base;
#endif
    __atomic_add_fetch(pres, 1, __ATOMIC_SEQ_CST);
    return *pres;
}

int init_mem_main(void* addr)
{
    lua_State* ls;
#ifdef ARCH_32BIT
    uint32_t* base;
#else
    uint64_t* base;
#endif
    pmpa_init_lunax(addr, SIZE_SHMEMORY);
    G_LUNAX_ADDR = (POINTER_INT) addr;
    base = pmpa_malloc((void*) G_LUNAX_ADDR,
            LUA_STATE_MAIN_SIZE + sizeof(sem_t));
    ls = create_main_table();

#ifdef ARCH_32BIT
    *base = (uint32_t) (LUNAX_RE_CAST((uint32_t ) ls));
    base++;
    *base = 42;
    base++;
    if (sem_init((sem_t *) base, 1, 0))
    {
        return E_ERROR;
    }
#else
    *base = (uint64_t)LUNAX_RE_CAST((uint64_t)ls);
    base++;
    *base = 42;
    base++;
    if (sem_init((sem_t *)base,1,0))
    {
        return E_ERROR;
    }
#endif
    sem_post((sem_t*) base);
    return E_OK;
}

int attach_to_mem_main(void* addr)
{
#ifdef ARCH_32BIT
    G_LUNAX_ADDR = (uint32_t) addr;
#else
    G_LUNAX_ADDR = (uint64_t)addr;
#endif
    return E_OK;
}

//In.
//L empty
//Out
// L | luserdata_lstate; luserdata_lsem; light
int is_table(lua_State* L, int pid, const char* name)
{
    int res = 0;
    lua_State* lstate;
    sem_t* lsem;
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, mtable);
    lua_pushinteger(LMAIN, pid);
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        lua_getfield(LMAIN, -1, name);
        if (lua_type(LMAIN, -1) == LUA_TTABLE)
        {
            //LMAIN | MAIN_TABLE: pid; shtable
            lua_pushvalue(LMAIN,-1);
            //LMAIN | MAIN_TABLE: pid; shtable; shtable;
            lsem = get_lsem(LMAIN);
            //LMAIN | MAIN_TABLE: pid; shtable
            lstate = get_lstate(LMAIN);
            SEM_WAIT(lsem);
            //LMAIN | MAIN_TABLE: pid;
            //L | empty
            lua_pop(LMAIN, 2);
            //LMAIN | empty
            get_share_table(L, lstate, lsem);
            //L | lstate; semaphore; light
            sem_post(lsem);
            res = 1;
        }
        else
        {
            lua_pop(LMAIN, 3);
        }

    }
    else
    {
        lua_pop(LMAIN, 2);
    }
    sem_post(semMAIN);
    return res;
}

//In
//LMAIN | empty
//Out
//LMAIN | empty
int add_pid_to_pid2pid(unsigned long long thr)
{
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_getglobal(LMAIN, pid2pid);
    lua_pushinteger(LMAIN, gettid());
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TNIL)
    {
        lua_pop(LMAIN, 1);
        //LMAIN| pid2pid
        lua_pushinteger(LMAIN, gettid());
        //LMAIN| pid2pid; tid
        lua_newtable(LMAIN);
        {
            lua_pushinteger(LMAIN, 0);
            lua_pushinteger(LMAIN, getpid());
            lua_settable(LMAIN, -3);

            lua_pushinteger(LMAIN, 1);
            lua_pushinteger(LMAIN, thr);
            lua_settable(LMAIN, -3);

        }
        lua_settable(LMAIN, -3);
    }
    else
    {
        lua_pop(LMAIN, 1);
    }
    lua_pop(LMAIN, 1);
    sem_post(semMAIN);
    return 0;
}

//In
//LMAIN | empty
//Out
//LMAIN | empty
POINTER_INT* lunax_threads_join(void)
{
    POINTER_INT* res = NULL;
    unsigned int pid;
    unsigned long long thr;
    int cnt = 0;
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_getglobal(LMAIN, pid2pid);
    lua_pushinteger(LMAIN, gettid());
    lua_gettable(LMAIN, -2);
    if (lua_type(LMAIN, -1) == LUA_TTABLE)
    {
        //LMAIN | pid2pid; pid2thr
        lua_pushinteger(LMAIN, 1);
        lua_gettable(LMAIN, -2);
        //LMAIN | pid2pid; pid2thr; thr
        if (lua_tointeger(LMAIN,-1) != 0)
        {
            lua_pop(LMAIN, 3);
            sem_post(semMAIN);
            return NULL;
        }
        lua_pop(LMAIN, 1);
        //LMAIN | pid2pid; pid2thr;
        lua_pushinteger(LMAIN, 0);
        lua_gettable(LMAIN, -2);
        //LMAIN | pid2pid; pid2thr; PID
        pid = lua_tointeger(LMAIN, -1);
        lua_pop(LMAIN, 2);
        //LMAIN | pid2pid;
        lua_pushnil(LMAIN);
        while (lua_next(LMAIN, -2) != 0)
        {
            //LMAIN | pid2pid; key(TID); value(pid2thr)
            lua_pushinteger(LMAIN, 0);
            lua_gettable(LMAIN, -2);
            if (pid == lua_tointeger(LMAIN, -1))
            {
                lua_pop(LMAIN, 1);
                lua_pushinteger(LMAIN, 1);
                lua_gettable(LMAIN, -2);
                //LMAIN | pid2pid; key(TID); value(pid2thr); real_thread
                thr = lua_tointeger(LMAIN, -1);
                if (thr)
                {
                    cnt++;
                    res = realloc(res, cnt * sizeof(POINTER_INT));
                    res[cnt - 1] = thr;
                }
            }
            lua_pop(LMAIN, 2);
            //LMAIN | pid2pid; key(TID);
        }
    }
    lua_pop(LMAIN, 1);
    cnt++;
    res = realloc(res, cnt * sizeof(POINTER_INT));
    res[cnt - 1] = 0;
    sem_post(semMAIN);
    return res;
}

//In
//LMAIN |  empty
// Out
// LMAIN | empty
int delete_pid2pid_table()
{
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, pid2pid);
    //LMAIN |  pid2pid

    lua_pushnil(LMAIN);
    //LMAIN |  pid2pid; nil
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN |  pid2pid; key(tid);value(pid2thr_table);
        lua_pop(LMAIN,1);
        //LMAIN |  pid2pid; key(tid);
        lua_pushvalue(LMAIN,-1);
        //LMAIN |  pid2pid; key(tid);key(tid)
        lua_pushnil(LMAIN);
        lua_settable(LMAIN,-4);
        //LMAIN |  pid2pid; key(tid);
    }
    //LMAIN |  pid2pid;
    lua_pushnil(LMAIN);
    lua_setglobal(LMAIN, pid2pid);
    lua_pop(LMAIN, 1);
    sem_post(semMAIN);
    return 0;
}

//In
//LMAIN |  empty
// Out
// LMAIN | empty
int delete_pid2thr_tables( int tid)
{
    int real_pid;
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_State* LMAIN = get_main_table();
    lua_getglobal(LMAIN, pid2pid);
    //LMAIN |  pid2pid

    lua_pushnil(LMAIN);
    //LMAIN |  pid2pid; nil
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN |  pid2pid; key(tid);value(pid2thr_table);
        if (lua_tointeger(LMAIN,-2) == tid)
        {
            lua_pushinteger(LMAIN,0);
            lua_gettable(LMAIN,-2);
            real_pid = lua_tointeger(LMAIN,-1);
            //LMAIN |  pid2pid; key(tid);value(pid2thr_table); real_pid
            lua_pop(LMAIN,3);
            break;
        }
        else
        {
            lua_pop(LMAIN, 1);
        }
        //LMAIN |  pid2pid; key(tid);
    }
    //LMAIN |  pid2pid;
    lua_pushnil(LMAIN);
    //LMAIN |  pid2pid; nil
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN |  pid2pid; key(tid);value(pid2thr_table);
        lua_pushinteger(LMAIN,0);
        lua_gettable(LMAIN,-2);
        //LMAIN |  pid2pid; key(tid);value(pid2thr_table);real_pid
        if (real_pid == lua_tointeger(LMAIN,-1))
        {
            lua_pop(LMAIN,2);
            //LMAIN |  pid2pid; key(tid);
            lua_pushvalue(LMAIN,-1);
            //LMAIN |  pid2pid; key(tid);key(tid)
            lua_pushnil(LMAIN);
            lua_settable(LMAIN,-4);
        }
        else
        {
            lua_pop(LMAIN, 2);
        }
        //LMAIN |  pid2pid; key(tid);
    }
    //LMAIN |  pid2pid;
    lua_pop(LMAIN, 1);
    sem_post(semMAIN);
    return 0;
}
