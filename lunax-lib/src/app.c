/*
** apps.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "app.h"
#include "mem_main.h"
#include <lua.h>
#include "lauxlib.h"
#include <lualib.h>

#include <pthread.h>

typedef struct
{
    char* data;
    int len;
    const char* extra_name;
} tdump;

int lunax_writer(lua_State *L, const void *p, size_t sz, void *ud)
{
    tdump* dump = (tdump*) ud;
    char *newdata;

    newdata = (char *) realloc(dump->data, dump->len + sz);
    if (newdata)
    {
        memcpy(newdata + dump->len, p, sz);
        dump->data = newdata;
        dump->len += sz;
    }
    else
    {
        free(newdata);
        return 1;
    }
    return 0;
}

void* lunax_thread(void *param)
{
    lua_State* L;
    tdump* dt = (tdump*) param;

    L = luaxL_newstate((char*) dt->extra_name); /* Create Lua state variable + init global semaphore */
    luaL_openlibs(L); /* Load Lua libraries */

    lua_newtable(L);
    {
        lua_pushstring(L, dt->extra_name);
        lua_setfield(L, -2, "name");
        lua_pushinteger(L, pthread_self());
        lua_setfield(L, -2, "thr");

    }
    lua_setglobal(L, "__LUNAX_IOTABLE");

    if (luaL_loadbuffer(L, dt->data, dt->len, "thread function"))
    {
        luaL_error(L, "ERROR. Function fail in reading.\n");
        return 0;
    }
    free(dt->data);

    if (lua_pcall(L, 0, 0, 0)) /* Run the loaded Lua script */
    {
        const char* err = lua_tostring(L, -1);
        printf("Error:%s\n", err);

        lua_getglobal(L, "debug"); // stack: err debug
        lua_getfield(L, -1, "traceback"); // stack: err debug debug.traceback

        // debug.traceback()
        if(lua_pcall(L, 0, 1, 0))
        {
          const char* err = lua_tostring(L, -1);
          printf("Error in debug.traceback() call: %s\n", err);
        }
        else
        {
          const char* stackTrace = lua_tostring(L, -1);
          printf("C stack traceback: %s\n", stackTrace);
        }
        //luaL_error(L, "ERROR. lua_pcall() failed.\n");
        return 0;
    }

    lua_close(L); /* Clean up, free the Lua state var */
    pthread_exit(0);
}

//In
//L | empty
//LMAIN | empty
//Out
//L | table
//LMAIN | empty
int app_get_procs(lua_State* L)
{
    int lcnt;
    lua_State* LMAIN = get_main_table();
    sem_t* semMAIN = get_main_sem();
    SEM_WAIT(semMAIN);
    lua_newtable(L);
    //L out_table;
    lua_getglobal(LMAIN, pid2pid);
    lua_pushnil(LMAIN);
    while (lua_next(LMAIN, -2) != 0)
    {
        //LMAIN | pid2pid; key(TID); value(pid2thr_table)
        lua_pushinteger(LMAIN, 0);
        lua_gettable(LMAIN, -2);
        //LMAIN | pid2pid; key(TID); value(pid2thr_table); PID
        lua_pushinteger(L, lua_tointeger(LMAIN, -1));
        //L out_table; PID
        lua_gettable(L, -2);
        if (lua_type(L, -1) == LUA_TNIL)
        {
            //L out_table;NIL
            lua_pop(L, 1);
            lua_pushinteger(L, lua_tointeger(LMAIN, -1));
            lua_pushinteger(L, 1);
            lua_settable(L, -3);
        }
        else
        {
            //L out_table; int
            lcnt = lua_tointeger(L, -1);
            lua_pop(L, 1);
            lua_pushinteger(L, lua_tointeger(LMAIN, -1));
            lua_pushinteger(L, lcnt + 1);
            lua_settable(L, -3);
        }
        //L out_table;
        lua_pop(LMAIN, 2);
        //LMAIN | pid2pid; key(TID);
    }
    //LMAIN | pid2pid;
    lua_pop(LMAIN, 1);

    sem_post(semMAIN);
    //L out_table;
    //LMAIN | empty
    return 1;
}

//In
//L | function
//LMAIN | empty
//Out
// L| tid
//LMAIN | empty
int app_create_thread(lua_State* L)
{
    tdump* pdump_func;
    pthread_t tid;
    pthread_attr_t attr;
    int res;

    pdump_func = malloc(sizeof(tdump));

    pdump_func->len = 0;
    pdump_func->data = NULL;
    pdump_func->extra_name = NULL;
    if (lua_dump(L, lunax_writer, pdump_func, 1))
    {
        luaL_error(L, "ERROR. Function fail in writing.\n");
        return 0;
    }

    lua_getglobal(L, "__LUNAX_IOTABLE");
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, "name");
        pdump_func->extra_name = lua_tostring(L, -1);
        lua_pop(L, 1);
    }
    else
    {
        pdump_func->extra_name = NULL;
    }
    lua_pop(L, 1);

    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 16777216);
    pthread_attr_setguardsize(&attr, 8192 * 4);

    res = pthread_create(&tid, &attr, lunax_thread, pdump_func);
    if (res)
    {
        luaL_error(L, "ERROR. Create thread %d.\n", res);
        return 0;
    }

    lua_pushinteger(L,tid);
    return 1;
}

//In
//L | tid
//LMAIN | empty
//Out
// L| empty
//LMAIN | empty
int app_cancel_thread(lua_State* L)
{
    pthread_t tid;
    tid = lua_tointeger(L,-1);
    pthread_cancel(tid);
    pthread_join(tid, NULL);
    lua_pop(L,1);
    return 0;
}
