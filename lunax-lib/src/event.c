/*
** events_table.h
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/


#include "event.h"
#include "lua.h"
#include "proxy_table.h"
#include "events_table.h"
#include "task_timer.h"

//In
// L  table_value ; type_event
//Out
//L | empty
int set_event_strategy(lua_State* L)
{

    if (lua_gettop(L) == 3)
    {
        if ((lua_type(L,-1) != LUA_TNUMBER) && (lua_type(L,-2) != LUA_TNUMBER) && (lua_type(L,-3) != LUA_TTABLE))
        {
            luaL_error(L, "Error %d: Bad argument. setStrategy(table, number, number)\n", E_BAD_ARG);
            return 0;
        }

    }
    else if (lua_gettop(L) == 2)
    {
        if ((lua_type(L,-1) != LUA_TNUMBER) && (lua_type(L,-2) != LUA_TTABLE))
        {
            luaL_error(L, "Error %d: Bad argument. setStrategy(table, number)\n", E_BAD_ARG);
            return 0;
        }
        lua_pushinteger(L,-1);
    }
    else
    {
        luaL_error(L, "Error %d: Bad argument. setStrategy(table, number, [number])\n", E_BAD_ARG);
        return 0;
    }
    return set_event_strategy_by_proxy_table(L);
}

//In
// L  table_value ;
//Out
//L | event_strategy
int get_event_strategy(lua_State* L)
{

    return get_event_strategy_by_proxy_table(L);
}

//In
// L | nonexist_table ; handler;; ms; sec; timeout
//Out
//L | ehid
static int set_event_handler_co(lua_State *L, int status, lua_KContext ctx)
{
    int st;
    int res;
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,5);
            lua_pushinteger(L, W_TIMEOUT);
            return 1;
        }
    }
    // L | nonexist_table ; handler; ms; sec; timeout
    set_read_check_attr(L);

    lua_pushvalue(L, -5);
    // L | nonexist_table ; handler; ms; sec; timeout; nonexist_table ;
    st = resolve_nonexist_table(L);
    if (st == 0)
    {
        lua_pop(L, 1);
        // L | nonexist_table ; handler; ms; sec; timeout;
        return lua_yieldk(L, 5, 0, &set_event_handler_co);
    }
    // L | nonexist_table ; handler; ms; sec; timeout; proxy_table;
    lua_insert(L, -5);
    // L | nonexist_table ; proxy_table; handler; ms; sec; timeout;
    lua_remove(L, -6);
    lua_pop(L,3);

    // L | proxy_table ; handler;
    res = set_event_handler_by_proxy_table(L);
    lua_pushinteger(L, res);
    return 1;
}
//In
// L | OPTIONAL(attr); table_value ; handler
//Out
//L | ehid
int set_event_handler(lua_State* L)
{
    int attr = ATTR_NONE;
    int res = 0;
    int status;
    long int sec = 0;
    long int ms = 0;
    long int timeout = 0;

    int cnt = lua_gettop(L);
    if (cnt == 3)
    {
        if (lua_type(L,-3) == LUA_TNUMBER)
        {
            attr = lua_tointeger(L, -3);
            lua_remove(L, -3);
        }
        else
        {
            // L | attr_table; table_value ; handler;
            lua_pushinteger(L,0);
            lua_gettable(L,-4);
            // L | attr_table; table_value ; handler; attr
            attr = lua_tointeger(L, -1);
            lua_pop(L,1);
            // L | attr_table; table_value ; handler;
            lua_pushinteger(L,1);
            lua_gettable(L,-4);
            timeout = lua_tointeger(L, -1);
            get_ms(&sec, &ms);
            lua_pop(L,1);
            lua_remove(L,-3);

        }
    }
    // L | table_value ; handler
    switch (attr)
    {
    case ATTR_NONE:
        res = set_event_handler_by_proxy_table(L);
        break;
    case ATTR_CHECK:
        // L | table_value ; handler
        status = is_exist_table(L, -2);
        // L | table_value ; handler; meta or absent
        if (status == 0)
        {
            // L | table_value ; handler;
            lua_pop(L, 2);
            res = 1;
        }
        else
        {
            // L | table_value ; handler;
            res = set_event_handler_by_proxy_table(L);
        }
        break;
    case ATTR_WAIT:
        if (lua_pushthread(L) == 1)
        {
            luaL_error(L, "Error: ATTR: Wait can be used in Task only. \n");
            lua_pop(L, 3);
            res = 1;
        }
        else
        {
            // L | table_value ; handler ; thread
            lua_pop(L, 1);
            // L | table_value ; handler
            status = is_exist_table(L, -2);
            // L | table_value ; handler;
            if (status == 0)
            {
                // L | nonexist_table ; handler;
                lua_pushinteger(L,ms);
                lua_pushinteger(L,sec);
                lua_pushinteger(L,timeout);
                // L | nonexist_table ; handler; ms; sec; timeout
                return set_event_handler_co(L, 0, 0);
            }
            else
            {
                // L | table_value ; handler;
                res = set_event_handler_by_proxy_table(L);
            }
        }
        break;
    default:
        break;
    }

    lua_pushinteger(L, res);
    return 1;
}

//In
// L  table_value ;  optional(ehid)
//Out
//L | empty
int unset_event_handler(lua_State* L)
{
    return unset_event_handler_by_proxy_table(L);
}

//In
// L | proxy_table; num_events; ms; sec; timeout;
//Out
//L | empty
static int is_event_handlers_co(lua_State *L, int status, lua_KContext ctx)
{
    int res;
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        //printf("ms: %ld\n", ms);
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,5);
            lua_pushinteger(L, W_TIMEOUT);
            return 1;
        }
    }

    int num = lua_tointeger(L, -4);
    // L | proxy_table; num_events; ms; sec; timeout;

    lua_pushvalue(L, -5);
    res = check_events_by_events_table(L);

    if (res < num)
    {
        // L | proxy_table;num_events; ms; sec; timeout;
        return lua_yieldk(L, 5, 0, &is_event_handlers_co);
    }
    lua_pop(L, 5);

    // L | empty
    lua_pushinteger(L, W_SET);
    return 1;
}

//In
// L | nonexist_table ; num_events; ms; sec; timeout
//Out
//L | proxy_table; num_events;
static int is_event_handlers_wait_proxy_co(lua_State *L, int status, lua_KContext ctx)
{
    int st;
    long int sec = 0;
    long int ms = 0;

    if (lua_tointeger(L,-1) !=0)
    {
        get_ms(&sec, &ms);
        ms = (sec - lua_tointeger(L, -2)) * 1000 + (ms - lua_tointeger(L, -3));
        if (ms >= lua_tointeger(L, -1))
        {
            lua_pop(L,5);
            lua_pushinteger(L, W_TIMEOUT);
            return 1;
        }
    }
    set_read_check_attr(L);

    lua_pushvalue(L, -5);
    // L | nonexist_table ; num_events; ms; sec; timeout; nonexist_table ;
    st = resolve_nonexist_table(L);
    if (st == 0)
    {
        lua_pop(L, 1);
        // L | nonexist_table ; num_events; ms; sec; timeout;
        return lua_yieldk(L, 5, 0, &is_event_handlers_wait_proxy_co);
    }
    // L | nonexist_table ; num_events; ms; sec; timeout; proxy_table;
    lua_insert(L, -5);
    // L | nonexist_table ; proxy_table; num_events; ms; sec; timeout;
    lua_remove(L, -6);

    // L |  proxy_table; num_events; ms; sec; timeout;
    return is_event_handlers_co(L,0,0);
}

//In
// L | OPTIONAL(attr); proxy_table; num_events;
//Out
//L | empty
int is_event_handlers(lua_State* L)
{
    int status;
    int attr = ATTR_NONE;
    int num;
    int res = 0;
    long int sec = 0;
    long int ms = 0;
    long int timeout = 0;
 
    int cnt = lua_gettop(L);
    if (cnt == 3)
    {
        if (lua_type(L,-3) == LUA_TNUMBER)
        {
            attr = lua_tointeger(L, -3);
            lua_remove(L, -3);
        }
        else
        {
            // L | attr_table; proxy_table; num_events;
            lua_pushinteger(L,0);
            lua_gettable(L,-4);
            // L | attr_table; proxy_table; num_events; attr
            attr = lua_tointeger(L, -1);
            lua_pop(L,1);
            // L | attr_table; proxy_table; num_events;
            lua_pushinteger(L,1);
            lua_gettable(L,-4);
            timeout = lua_tointeger(L, -1);
            get_ms(&sec, &ms);
            lua_pop(L,1);
            lua_remove(L,-3);

        }
    }
    // L |  proxy_table; num_events;
    num = lua_tointeger(L, -1);
    lua_pop(L, 1);
    // L |  proxy_table;

    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_pop(L, 1);
        luaL_error(L, "Error: Incorrect type. \n");
        return 0;
    }
    switch (attr)
    {
    case ATTR_NONE:
        status = is_exist_table(L, -1);
        // L | proxy_table; meta or absent
        if (status != 0)
        {
            res = check_events_by_events_table(L);
        }
        break;
    case ATTR_CHECK:
        status = is_exist_table(L, -1);
        // L | proxy_table; meta or absent
        if (status != 0)
        {
            res = check_events_by_events_table(L);
        }
        break;
    case ATTR_WAIT:

        // L | proxy_table;
        status = is_exist_table(L, -1);
        // L | proxy_table;
        if (status == 0)
        {
            dbg("Warning. Field doesn't exist in table yet.");
            // L | nonexist_table;
            lua_pushinteger(L, num);
            // L | nonexist_table; num_events;
            lua_pushinteger(L,ms);
            lua_pushinteger(L,sec);
            lua_pushinteger(L,timeout);
            // L | nonexist_table; num_events; ms; sec; timeout
            return is_event_handlers_wait_proxy_co(L, 0, 0);
        }
        lua_pushinteger(L, num);
        lua_pushinteger(L,ms);
        lua_pushinteger(L,sec);
        lua_pushinteger(L,timeout);
        return is_event_handlers_co(L, 0, 0);
        break;
    default:
        break;
    }

    lua_pushinteger(L, res);
    return 1;
}
