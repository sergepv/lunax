/*
** proxy_table.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "lunax.h"
#include "proxy_table.h"
#include "shared_table.h"
#include "select_table.h"
#include "events_table.h"
#include "shared_events_table.h"
#include "deepcopy.h"
#include "shared_table_ext.h"

static char STATE;
static char SEM;
static char TRANSACTION_WRITE_TYPE;
static char TRANSACTION_WRITE_STATUS;
static char TRANSACTION_WRITE_STATUS_OBJECT;
static char TRANSACTION_READ_CHECK;
static char TRANSACTION_READ_CHECK_WAIT_NONEXIST_TABLE;

int call_libfuncs_create(lua_State* L)
{
    lua_pushinteger(L, TRANSACTION_WRITE_CREATE);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
    lua_pushinteger(L, TRANSACTION_WRITE_STATUS_FIRST);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
    return 0;
}

int call_libfuncs_merge(lua_State* L)
{
    lua_pushinteger(L, TRANSACTION_WRITE_MERGE);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
    lua_pushinteger(L, TRANSACTION_WRITE_STATUS_FIRST);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
    return 0;
}

int call_libfuncs_update(lua_State* L)
{
    lua_pushinteger(L, TRANSACTION_WRITE_UPDATE);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
    lua_pushinteger(L, TRANSACTION_WRITE_STATUS_FIRST);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
    return 0;
}

//In
// L | empty
//Out
//L | attr code
int call_libfuncs_check(lua_State* L)
{
    lua_pushinteger(L, 1);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK);
    lua_pushinteger(L, ATTR_CHECK);
    return 1;
}

//In
// L | optional(timeout)
//Out
//L | optional(timeout); attr code;
int call_libfuncs_wait(lua_State* L)
{
    lua_pushinteger(L, 1);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK);
    lua_pushinteger(L, 1);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK_WAIT_NONEXIST_TABLE);
    if (lua_gettop(L) == 1)
    {
        lua_newtable(L);
        {
            lua_pushinteger(L,0);
            lua_pushinteger(L, ATTR_WAIT);
            lua_settable(L,-3);

            lua_pushinteger(L,1);
            //L | timeout ; table; 1
            lua_pushvalue(L, -3);
            lua_settable(L,-3);
            //L | timeout ; table;
            lua_remove(L,-2);
        }
    }
    else
    {
        lua_pushinteger(L, ATTR_WAIT);
    }
    return 1;
}

//In
// L | empty
//Out
//L | empty
void set_read_check_attr(lua_State* L)
{
    lua_pushinteger(L, 1);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK);
}

//In
// L | empty
//Out
//L | empty
void clear_read_check_attr(lua_State* L)
{
    lua_pushinteger(L, 0);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK);
}

//In
// L | (table in objindex)
//Out
// L | empty
int is_exist_table (lua_State *L, int objindex)
{
    int res = 0;
    int status;
    status = lua_getmetatable(L, objindex);
    // L | meta or absent
    if (status != 0)
    {
        // L | meta
        lua_getfield(L,-1, NONEXIST_IDENT_KEY);
        if (lua_type(L,-1) == LUA_TNIL)
        {
            res = 1;
        }

        lua_pop(L,2);
    }
    return res;
}

//In
// L | nonexist_table;
//Out
// L| proxy_table or nonexist_table

int resolve_nonexist_table(lua_State* L)
{
    int deep;
    int current =0;
    lua_getmetatable(L,-1);

    //L| nonexist_table; meta
    lua_pushinteger(L,0);
    lua_setfield(L,-2, NONEXIST_IDENT_KEY);

    lua_pushinteger(L,NONEXIST_DEEP_KEY);
    //L| nonexist_table; meta; NONEXIST_DEEP_KEY
    lua_gettable(L,-2);
    //L| nonexist_table; meta; deep
    deep = lua_tointeger(L,-1);
    lua_pop(L,1);
    lua_pushinteger(L,NONEXIST_TABLE_KEY);
    lua_gettable(L,-2);
    //L| nonexist_table; meta; table;
    while(current <= deep)
    {
        lua_pushinteger(L, NONEXIST_KEYS_KEY + current);
        //L| nonexist_table;meta; table; index
        lua_gettable(L, -3);
        //L| nonexist_table; meta;table; key
        lua_gettable(L, -2);
        //L| nonexist_table; meta; table; value_table;
        if (is_exist_table(L, -1) == 0)
        {
            lua_pop(L, 3);
            return 0;
        }
        lua_remove(L,-2);
        //L| nonexist_table; meta; value_table;
        current++;
    }
    //L| nonexist_table; meta; value_table;
    lua_remove(L,-2);
    lua_remove(L,-2);
    return 1;
}

//In
// L | table; key
//Out
// L| value
static int get_value4index_nonexist(lua_State* L)
{
    int deep;
    lua_getmetatable(L,-2);

    lua_getfield(L,-1, NONEXIST_IDENT_KEY);
    if (lua_tointeger(L,-1) == 0)
    {
        lua_pop(L,2);
        return 1;
    }
    lua_pop(L,1);
    // L | table; key; meta;
    lua_insert(L,-2);
    // L | table; meta; key;
    lua_pushinteger(L,NONEXIST_DEEP_KEY);
    lua_gettable(L,-3);
    //L| table;meta; key ; deep
    deep = lua_tointeger(L,-1);
    deep++;
    lua_pop(L,1);
    lua_pushinteger(L,NONEXIST_DEEP_KEY);
    lua_pushinteger(L,deep);
    lua_settable(L,-4);
    //L| table;meta; key ;
    lua_pushinteger(L,NONEXIST_KEYS_KEY+deep);
    lua_pushvalue(L,-2);
    //L| table;meta; key ; keydeep_index,key
    lua_settable(L,-4);
    lua_pop(L,2);
    //L| table;
    return 1;
}
//In
// L | table; key;light;
//Out
// L|  table; key;value
static SH_TABLE_STATE get_value4index_read(lua_State* L, pid_t pid, const char* name,
        lua_State* lstate, sem_t* lsem)
{
    SH_TABLE_STATE res;
    // L | table; key;light;
    lua_pushvalue(L, -2);
    // L | table; key;light; key

    res = getvalue_share_table(L, lstate, lsem);
    if (res == SH_LIGHT_EXIST)
    {
        get_proxy(L, lstate, lsem, pid, name);
        // L | table; key;value
    }
    else
    {
        // L | table; key;value
        if (res != SH_LIGHT_DELETED_ROOT)
        {
            lua_rawgetp(L, LUA_REGISTRYINDEX,
                    (void*) &TRANSACTION_READ_CHECK_WAIT_NONEXIST_TABLE);
            if (lua_tointeger(L,-1) != 0)
            {
                if (lua_type(L, -2) == LUA_TNIL)
                {
                    // L | table; key; value; 1 ;
                    lua_pop(L, 2);
                    lua_newtable(L);
                    {
                    }
                    lua_newtable(L);
                    {
                        // L | table; key; newtable; newmetatable;

                        lua_pushinteger(L, NONEXIST_DEEP_KEY);
                        lua_pushinteger(L, 0);
                        // L | table; key; newtable;newmetatable;NONEXIST_DEEP_KEY; 0;
                        lua_settable(L, -3);

                        lua_pushinteger(L, NONEXIST_TABLE_KEY);
                        lua_pushvalue(L, -5);
                        // L | table; key; newtable; newmetatable; NONEXIST_TABLE_KEY; table
                        lua_settable(L, -3);

                        lua_pushinteger(L, NONEXIST_KEYS_KEY);
                        lua_pushvalue(L, -4);
                        // L | table; key; newtable;newmetatable; NONEXIST_KEYS_KEY; key
                        lua_settable(L, -3);

                        lua_pushstring(L, NONEXIST_IDENT_KEY);
                        lua_pushinteger(L, 1);
                        lua_settable(L, -3);

                        lua_pushstring(L, "__index");
                        lua_pushcfunction(L, get_value4index_nonexist);
                        lua_settable(L, -3);
                    }
                    lua_setmetatable(L, -2);
                    // L | table; key; newtable;

                }
                else
                {
                    lua_pop(L, 1);
                }
            }
            else
            {
                // L | table; key; value; 0 ;
                lua_pop(L, 1);
            }
        }
        // L | table; key;value
    }
    lua_pushinteger(L, 0);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK_WAIT_NONEXIST_TABLE);
    lua_pushinteger(L, 0);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_READ_CHECK);
    return res;
}

//In
// L | table; key;light;
//Out
// L|  table; key;value
static int get_value4index_write(lua_State* L, pid_t pid, const char* name,
        lua_State* lstate, sem_t* lsem)
{
    int res = 0;
    SH_TABLE_STATE resstate;
    int first;
    sem_t* lsem_write;
    // L | table; key;light;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
    // L | table; key;light;first

    first = lua_tointeger(L, -1);
    if (first == TRANSACTION_WRITE_STATUS_CHECK)
    {
        // L | table; key;light;first
        lua_rawgetp(L, LUA_REGISTRYINDEX,
                (void*) &TRANSACTION_WRITE_STATUS_OBJECT);
        // L | table; key;light;first;object
        first = lua_compare(L, -1, -5, LUA_OPEQ);
        if (first == 0)
        {
            first = TRANSACTION_WRITE_STATUS_READ;
        }
        else
        {
            first = TRANSACTION_WRITE_STATUS_NEXT;
        }
        lua_pop(L, 1);
    }
    lua_pop(L, 1);
    // L | table; key;light;

    lua_pushvalue(L, -2);
    // L | table; key;light; key
    switch (first)
    {
    case TRANSACTION_WRITE_STATUS_FIRST:
        if (getvalue_share_table_for_write(L, lstate, lsem, 1) == 1)
        {
            get_proxy(L, lstate, lsem, pid, name);
            // L | table; key; value
        }
        lua_pushvalue(L, -1);
        lua_rawsetp(L, LUA_REGISTRYINDEX,
                (void*) &TRANSACTION_WRITE_STATUS_OBJECT);
        lua_pushinteger(L, TRANSACTION_WRITE_STATUS_CHECK);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
        break;

    case TRANSACTION_WRITE_STATUS_NEXT:
        if (getvalue_share_table_for_write(L, lstate, lsem, 0) == 1)
        {
            get_proxy(L, lstate, lsem, pid, name);
            // L | table; key; value
        }
        lua_pushvalue(L, -1);
        lua_rawsetp(L, LUA_REGISTRYINDEX,
                (void*) &TRANSACTION_WRITE_STATUS_OBJECT);
        lua_pushinteger(L, TRANSACTION_WRITE_STATUS_CHECK);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
        break;

    case TRANSACTION_WRITE_STATUS_READ:
        lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
        lsem_write = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);

        if (lsem == lsem_write)
        {
            resstate = getvalue_share_table_nosem(L, lstate);
        }
        else
        {
            resstate = getvalue_share_table(L, lstate, lsem);
        }
        if (resstate == SH_LIGHT_EXIST)
        {
            get_proxy(L, lstate, lsem, pid, name);
            // L | table; key;value
        }
        lua_pushinteger(L, TRANSACTION_WRITE_STATUS_READ);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_STATUS);
        res = 1;
        break;
    default:
        break;
    }

    // L | table; key; value
    return res;
}

//In
// L | table; key
//Out
// L| value
int get_value4index(lua_State* L)
{
    pid_t pid;
    const char* name;
    lua_State* lstate;
    sem_t* lsem;
    lua_State* lstate_raw;
    sem_t* lsem_raw;

    int flag;
    if (lua_type(L, -1) == LUA_TNIL)
    {
        luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
        return 0;
    }
    lua_getmetatable(L, -2);
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        // L | table; key; metatable
        lua_getfield(L, -1, "pid");
        // L | table; key; metatable; int
        pid = lua_tointeger(L, -1);
        lua_getfield(L, -2, "name");
        // L | table; key; metatable ;int;string
        name = lua_tostring(L, -1);
        lua_pop(L, 2);
        // L | table; key; metatable;

        lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
        flag = lua_tointeger(L, -1);
        lua_pop(L, 1);

        // L | table; key; metatable;
        lua_getfield(L, -1, "shtable");

        if (lua_type(L,-1) == LUA_TNIL)
        {
            if (flag)
            {
                dbg("WRITE TRANSACTION: proxy table was deleted. ");
            }
            else
            {
                dbg(" proxy table was deleted. ");
            }
            raise_error(L,"CRmShtableError");
            lua_pop(L, 4);
            lua_pushnil(L);
            lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
            lua_pushnil(L);
            return 1;
        }

        // L | table; key; metatable; light;
        lua_getfield(L, -2, "sem");
        lsem_raw = lua_touserdata(L, -1);
        lsem = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -2, "state");
        lstate_raw = lua_touserdata(L, -1);
        lstate = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_remove(L, -2);

        // L | table; key;light;
        if (flag)
        {

            if (get_value4index_write(L, pid, name, lstate, lsem) == 0)
            {
                lua_pushlightuserdata(L, lstate_raw);
                lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &STATE);
                lua_pushlightuserdata(L, lsem_raw);
                lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
            }

        }
        else
        {
            SH_TABLE_STATE res;
            res = get_value4index_read(L, pid, name, lstate, lsem);
            if ((res == SH_LIGHT_DELETED_LEAF) || (res == SH_LIGHT_DELETED_ROOT))
            {
                raise_error(L,"CRmShtableError");
            }
            // L | table; key;value
        }

        //L | table; key; value;
        lua_remove(L, -2);
        lua_remove(L, -2);
        //L | value;
        return 1;
    }
    luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
    return 0;
}

int set_value4index(lua_State* L)
{
    luaL_error(L, "Error %d: Shared Table. Operation over lunax only.\n",
            E_ACCESS_DENIED);
    return 0;
}

int gc_clean(lua_State* L)
{
    lua_getmetatable(L, -1);
    lua_getfield(L, -1, "name");
    dbg("#### GC [%s] --->",lua_tostring(L,-1));
    return 0;
}

////// ------------------------------------------------------ __pairs and  __ipairs

//In
// L | table; key;light;
//Out
// L|  table; newkey;value
static SH_TABLE_STATE get_value4next(lua_State* L, pid_t pid, const char* name,
        lua_State* lstate, sem_t* lsem)
{
    SH_TABLE_STATE res;
    // L | table; key;light;
    lua_pushvalue(L, -2);
    // L | table; key;light; key

    res = next_share_table(L, lstate, lsem, 0);
    // L | table; key; newkey; value
    lua_remove(L, -3);
    // L | table;  newkey; value
    if (res == SH_LIGHT_EXIST)
    {
        get_proxy(L, lstate, lsem, pid, name);
        // L | table; newkey;value
    }
    // L | table; newkey;value
    return res;
}

//In
// L | table; key;light;
//Out
// L|  table; newkey;value
static SH_TABLE_STATE get_value4next_i(lua_State* L, pid_t pid, const char* name,
        lua_State* lstate, sem_t* lsem)
{
    SH_TABLE_STATE res;
    // L | table; key;light;
    lua_pushvalue(L, -2);
    // L | table; key;light; key

    res = next_share_table(L, lstate, lsem, 1);
    // L | table; key; newkey; value
    lua_remove(L, -3);
    // L | table;  newkey; value
    if (res == SH_LIGHT_EXIST)
    {
        get_proxy(L, lstate, lsem, pid, name);
        // L | table; newkey;value
    }
    // L | table; newkey;value
    return res;
}

//In
// L | table; key
//Out
// L| key, value
// OR
// L| nil
static int get_values4stateless_pair_iter(lua_State* L)
{
    pid_t pid;
    const char* name;
    lua_State* lstate;
    sem_t* lsem;
    lua_State* lstate_raw;
    sem_t* lsem_raw;

    lua_getmetatable(L, -2);
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        // L | table; key; metatable
        lua_getfield(L, -1, "pid");
        // L | table; key; metatable; int
        pid = lua_tointeger(L, -1);
        lua_getfield(L, -2, "name");
        // L | table; key; metatable ;int;string
        name = lua_tostring(L, -1);
        lua_pop(L, 2);
        // L | table; key; metatable;

        lua_getfield(L, -1, "shtable");

        if (lua_type(L,-1) == LUA_TNIL)
        {
        	dbg(" proxy table was deleted. ");
            raise_error(L,"CRmShtableError");
            lua_pop(L, 4);
            lua_pushnil(L);
            lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
            lua_pushnil(L);
            return 1;
        }

        // L | table; key; metatable; light;
        lua_getfield(L, -2, "sem");
        lsem_raw = lua_touserdata(L, -1);
        lsem = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -2, "state");
        lstate_raw = lua_touserdata(L, -1);
        lstate = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_remove(L, -2);

        // L | table; key;light;

        SH_TABLE_STATE res;
        res = get_value4next(L, pid, name, lstate, lsem);
        if ((res == SH_LIGHT_DELETED_LEAF) || (res == SH_LIGHT_DELETED_ROOT))
        {
        	raise_error(L,"CRmShtableError");
        }
        //L | table; key; value;
        if (lua_isnil(L,-2))
        {
        	lua_pop(L,3);
        	lua_pushnil(L);
        	return 1;
        }
        //L | table; key; value;
        lua_remove(L, -3);
        //L | key; value;
        return 2;
    }
    luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
    return 0;
}

//In
// L | table; key
//Out
// L| key, value
// OR
// L| nil
static int get_values4stateless_ipair_iter(lua_State* L)
{
    pid_t pid;
    const char* name;
    lua_State* lstate;
    sem_t* lsem;
    lua_State* lstate_raw;
    sem_t* lsem_raw;

    lua_getmetatable(L, -2);
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        // L | table; key; metatable
        lua_getfield(L, -1, "pid");
        // L | table; key; metatable; int
        pid = lua_tointeger(L, -1);
        lua_getfield(L, -2, "name");
        // L | table; key; metatable ;int;string
        name = lua_tostring(L, -1);
        lua_pop(L, 2);
        // L | table; key; metatable;

        lua_getfield(L, -1, "shtable");

        if (lua_type(L,-1) == LUA_TNIL)
        {
        	dbg(" proxy table was deleted. ");
            raise_error(L,"CRmShtableError");
            lua_pop(L, 4);
            lua_pushnil(L);
            lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
            lua_pushnil(L);
            return 1;
        }

        // L | table; key; metatable; light;
        lua_getfield(L, -2, "sem");
        lsem_raw = lua_touserdata(L, -1);
        lsem = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_getfield(L, -2, "state");
        lstate_raw = lua_touserdata(L, -1);
        lstate = LUNAX_CAST(lua_touserdata(L, -1));
        lua_pop(L, 1);
        lua_remove(L, -2);

        // L | table; key;light;

        SH_TABLE_STATE res;
        res = get_value4next_i(L, pid, name, lstate, lsem);
        if ((res == SH_LIGHT_DELETED_LEAF) || (res == SH_LIGHT_DELETED_ROOT))
        {
        	raise_error(L,"CRmShtableError");
        }
        //L | table; key; value;
        if (lua_isnil(L,-2))
        {
        	lua_pop(L,3);
        	lua_pushnil(L);
        	return 1;
        }
        //L | table; key; value;
        lua_remove(L, -3);
        //L | key; value;
        return 2;
    }
    luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
    return 0;
}

//In
// L | proxy_table;
//Out
// L| function, proxy_table, nil
int get_value4pairs(lua_State* L)
{
	lua_pushcfunction(L, get_values4stateless_pair_iter);
	lua_pushvalue(L, -2);
	lua_pushnil(L);
	// L | proxy_table;function; proxy_table; nil
	lua_remove(L,-4);
	return 3;
}

//In
// L | proxy_table;
//Out
// L| function, proxy_table, nil
int get_value4ipairs(lua_State* L)
{
	lua_pushcfunction(L, get_values4stateless_ipair_iter);
	lua_pushvalue(L, -2);
	lua_pushnil(L);
	// L | proxy_table;function; proxy_table; nil
	lua_remove(L,-4);
	return 3;
}
//----------------------------------------------------------------------------------------------------------

//In
// L |  light;table0; table1
//Out
// L |  light;table0; table1
int fill_metatable(lua_State* L, int pid, const char* name)
{
    lua_pushstring(L, "__index");
    lua_pushcfunction(L, get_value4index);
    lua_settable(L, -3);

    lua_pushstring(L, "__newindex");
    lua_pushcfunction(L, set_value4index);
    lua_settable(L, -3);

    lua_pushstring(L, "__newindex");
    lua_pushcfunction(L, set_value4index);
    lua_settable(L, -3);

    lua_pushstring(L, "__metatable");
    lua_pushstring(L, "shared table: access denied");
    lua_settable(L, -3);

    lua_pushstring(L, "__gc");
    lua_pushcfunction(L, gc_clean);
    lua_settable(L, -3);

    lua_pushstring(L, "pid");
    lua_pushinteger(L, pid);
    lua_settable(L, -3);

    lua_pushstring(L, "name");
    lua_pushstring(L, name);
    lua_settable(L, -3);

    lua_pushstring(L, "__pairs");
    lua_pushcfunction(L, get_value4pairs);
    lua_settable(L, -3);

    lua_pushstring(L, "__ipairs");
    lua_pushcfunction(L, get_value4ipairs);
    lua_settable(L, -3);
    return 0;
}

//In
// L |   light;
//Out
// L |  light;table0;
int create_proxy_table(lua_State* L, lua_State* lstate, sem_t* lsem, pid_t pid,
        const char* name)
{
    lua_newtable(L);
    {
        lua_pushstring(L, "p1");
        lua_pushinteger(L, 43);
        lua_settable(L, -3);
    }
    lua_newtable(L);
    {
        // L |  light;table0; table1
        fill_metatable(L, pid, name);
        // L |  light;table0; table1

        lua_pushstring(L, "sem");
        lua_pushlightuserdata(L, LUNAX_RE_CAST(lsem));
        lua_settable(L, -3);

        lua_pushstring(L, "state");
        lua_pushlightuserdata(L, LUNAX_RE_CAST(lstate));
        lua_settable(L, -3);

        lua_pushvalue(L, -3);
        // L |  light;table0; table1;light
        lua_setfield(L, -2, "shtable");
        // L |  light;table0; table1;
    }
    lua_setmetatable(L, -2);
    // L |  light;table0;
    return 0;
}

//In
// L | safe ;param1 ; param2
//lstate | shared_table; key;
//Out
// L | empty
// lstate | empty
int copy_value_by_proxy_table(lua_State* L)
{
    int res = 0;
    events_type strategy;
    int stratetgy_size;

    int write_type;
    sem_t* lsem;

    lua_remove(L, -3);
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &SEM);
    if (lua_type(L,-1) == LUA_TNIL)
    {
        dbg(" WRITE TRANSACTION canceled");
        lua_pushinteger(L, TRANSACTION_WRITE_STOP);
        lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
        return res;
    }
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &STATE);
    // L | param1 ; param2; state;
    lua_rawgetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
    write_type = lua_tointeger(L, -1);
    lua_pop(L, 1);
    // L | param1 ; param2; state;
    switch (write_type)
    {
    case TRANSACTION_WRITE_CREATE:
        // L | param1 ; param2; state;
        //lstate | shared_table; key;
        setvalue_share_table_create(L, lsem);
        // L | state;
        //lstate | shared_table; key;
        setvalue_share_table_clean(L, lsem);
        break;
    case TRANSACTION_WRITE_MERGE:
        setvalue_share_table_merge(L, lsem);
        // L | state;
        //lstate | shared_table; key;
        setvalue_share_table_clean(L, lsem);
        break;
    case TRANSACTION_WRITE_UPDATE:
        // L | param1 ; param2; state;
        lua_pushvalue(L, -1);
        // L | param1 ; param2; state;state;
        //lstate | shared_table; key;
        get_event_strategy_by_proxy_table_inner(L);
        // L | param1 ; param2; state;strategy ;startetgy_size
        //lstate | shared_table; key;

        stratetgy_size = lua_tointeger(L, -1);
        strategy = lua_tointeger(L, -2);
        lua_pop(L, 2);
        // L | param1 ; param2; state;
        //lstate | shared_table; key;
        res = update_event_by_proxy_table_inner(L, strategy, stratetgy_size);

        // L | param1 ; param2; state;
        //lstate | shared_table; key;
        if (res == 0)
        {
#ifndef  SH_SIMPLE
            // L | param1 ; param2; state;
            //lstate | shared_table; key;
            sync_shared_table_diff_inner(L);
#endif
            // L | param1 ; param2; state;
            //lstate | shared_table; key;
            setvalue_share_table_merge(L, lsem);
            // L |  state;
            //lstate | shared_table; key;
        }
        else
        {
            lua_remove(L, -2);
            lua_remove(L, -2);
        }
        // L | state;
        //lstate | shared_table; key;
        setvalue_share_table_clean(L, lsem);
        break;
    default:
        break;
    }

    lua_pushinteger(L, TRANSACTION_WRITE_STOP);
    lua_rawsetp(L, LUA_REGISTRYINDEX, (void*) &TRANSACTION_WRITE_TYPE);
    return res;
}

//In
// L  table_value ; type_event; size of buffer
//Out
//L | empty
int set_event_strategy_by_proxy_table(lua_State* L)
{
    int etype;
    int esize;
    lua_State* lstate;
    sem_t* lsem;

    esize = lua_tointeger(L, -1);
    lua_pop(L, 1);
    etype = lua_tointeger(L, -1);
    lua_pop(L, 1);
    // L  proxy_table;
    lua_getmetatable(L, -1);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
        return 0;
    }
    // L | proxy_table; metatable;

    lua_getfield(L, -1, "sem");
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "state");
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "shtable");
    // L | proxy_table; metatable;light;
    setstrategy_share_table(L, lstate, lsem, etype,esize);
    // L | proxy_table; metatable;
    lua_pop(L, 2);
    return 0;
}

//In
// L  table_value
//Out
//L | event_strategy
int get_event_strategy_by_proxy_table(lua_State* L)
{
    lua_State* lstate;
    sem_t* lsem;
    // L  proxy_table;
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
        return 0;
    }
    lua_getmetatable(L, -1);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        luaL_error(L, "Error %d: Shared Table.\n", E_BAD_ARG);
        return 0;
    }
    // L | proxy_table; metatable;
    lua_getfield(L, -1, "sem");
    lsem = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "state");
    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    lua_getfield(L, -1, "shtable");
    // L | proxy_table; metatable;light;
    lua_remove(L, -2);
    lua_remove(L, -2);
    // L | light;
    getstrategy_share_table(L, lstate, lsem);
    // L | event_strategy
    return 1;
}

//In
// L  state
// lstate | shared_table; key
//Out
//L | event_strategy; event_startetgy_size
// lstate | shared_table; key
int get_event_strategy_by_proxy_table_inner(lua_State* L)
{
    lua_State* lstate;

    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    lua_pop(L, 1);
    //L | empty
    //lstate | shared_table; key;
    lua_pushvalue(lstate, -2);
    //lstate | shared_table; key; shared_table;

    lua_getfield(lstate, -1, "__LIGHT");
    //lstate | shared_table; key; shared_table;light;
    xcopy1(lstate, L, -1);
    //lstate | shared_table; key; shared_table;light;
    // L | light;
    lua_pop(lstate, 2);

    // L | light;
    //lstate | shared_table; key;
    getstrategy_share_table_inner(L, lstate);
    // L | event_strategy; event_startetgy_size
    //lstate | shared_table; key;

    return 2;
}

//In
// L  table_value ; handler
//Out
//L | empty
int set_event_handler_by_proxy_table(lua_State* L)
{
    return add_handler(L);
}

//In
// L  table_value ; optional(ehid)
//Out
//L | empty
int unset_event_handler_by_proxy_table(lua_State* L)
{
    return del_handler(L);
}

//In
// L  state
// lstate | shared_table; key
//Out
//L | state
// lstate | shared_table; key
int update_event_by_proxy_table_inner(lua_State* L, events_type strategy, int startetgy_size)
{
    int res = 0;
    lua_State* lstate;

    lstate = LUNAX_CAST(lua_touserdata(L, -1));
    //L | state
    //lstate | shared_table; key;
    lua_pushvalue(lstate, -2);
    //lstate | shared_table; key; shared_table;

    lua_getfield(lstate, -1, "__LIGHT");
    //lstate | shared_table; key; shared_table;light;
    xcopy1(lstate, L, -1);
    //lstate | shared_table; key; shared_table;light;
    // L | state; light;
    lua_pop(lstate, 2);

    res = update_event_in_shared_events(L, lstate, strategy, startetgy_size);
    //lstate | shared_table; key;
    // L | state;
    return res;
}
