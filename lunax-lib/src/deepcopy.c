/*
** deepcopy.c
**
** Lunax -- a Lua/C framework for creating multitask applications.
**
** Copyright (C) 2018-2019 Sergii Paveliev. All rights reserved.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**
** [ Apache 2.0 license - see the file LICENSE. ]
*/

#include "lunax.h"
#include "deepcopy.h"
#include <string.h>
#include "shared_table.h"
#include "shared_table_ext.h"

//In
// lstate | shared_table; key; param_table;
//Out
//lstate |  shared_table; key; param_table;
int xmerge(lua_State *lstate)
{
    uint32_t light;
    int light_flag = 0;
    lua_pushvalue(lstate, -2);
    // lstate | shared_table; key; param_table;key;
    lua_gettable(lstate, -4);
    // lstate | shared_table; key; param_table; target_value
    if (lua_type(lstate, -1) != LUA_TTABLE)
    {
        lua_pop(lstate, 1);
        lua_newtable(lstate);

        create_shared_table_inner(lstate);

        // lstate | shared_table; key; param_table; table;
        lua_pushvalue(lstate, -3);
        // lstate | shared_table; key; param_table; table; key;

        lua_pushvalue(lstate, -2);
        // lstate | shared_table; key; param_table; table ;key; table;
        lua_settable(lstate, -6);
        // lstate | shared_table; key; param_table; table ;
    }
    else
    {
        // lstate | shared_table; key; param_table; sharedX_table ;
        light_flag = 1;
        lua_getfield(lstate, -1, "__LIGHT");
        light = lua_tointeger(lstate, -1);
        lua_pop(lstate, 1);
        // lstate | shared_table; key; param_table; sharedX_table ;
    }
    // lstate | shared_table; key; param_table; sharedX_table ;
    lua_pushnil(lstate); /* first key */
    //lstate |  shared_table; key; param_table;sharedX_table ;nill
    while (lua_next(lstate, -3) != 0)
    {
        //lstate | shared_table; key; param_table; sharedX_table; key; value
        if (lua_type(lstate, -1) == LUA_TTABLE)
        {
            // lstate | shared_table; key; param_table;sharedX_table; key; paramY_table;
            xmerge(lstate);
            // lstate | shared_table; key; param_table;sharedX_table; key; paramY_table;
            lua_pop(lstate, 1);
            // lstate | shared_table; key; param_table;sharedX_table; key;
        }
        else
        {
            //lstate |  shared_table; key; param_table; sharedX_table; key; value ;
            lua_pushvalue(lstate, -2);
            //lstate |  shared_table; key; param_table; sharedX_table; key; value ; key
            lua_insert(lstate, -3);
            //lstate |  shared_table; key; param_table; sharedX_table; key; key; value;
            lua_settable(lstate, -4);
            //lstate |  shared_table; key; param_table; sharedX_table; key;
        }
    }
    if (light_flag)
    {
        lua_pushinteger(lstate, light);
        lua_setfield(lstate, -2, "__LIGHT");
    }
    //lstate |  shared_table; key; param_table; sharedX_table;
    lua_pop(lstate, 1);
    //lstate |  shared_table; key; param_table;
    return 0;
}

int xcopy1(lua_State *L, lua_State *T, int n)
{
    int f;
    const char* vl;
    switch (lua_type(L, n))
    {
    case LUA_TNIL:
        lua_pushnil(T);
        break;
    case LUA_TBOOLEAN:
        lua_pushboolean(T, lua_toboolean(L, n));
        break;
    case LUA_TNUMBER:
        f = lua_tointeger(L, n);
        lua_pushinteger(T, f);
        break;
    case LUA_TSTRING:
        vl = lua_tostring(L, n);
        lua_pushlstring(T, vl, strlen(vl));
        break;
    case LUA_TLIGHTUSERDATA:
        lua_pushlightuserdata(T, (void*) lua_touserdata(L, n));
        break;
    case LUA_TTABLE:
        xcopy(L, T, n);
        break;
    default:
        luaL_error(L, "Error: Deep copy\n");
        //assert(0);
        break;
    }
    return 0;
}

/* table is in the stack at index 't' */
int xcopy(lua_State *L, lua_State *T, int t)
{
    int w;
    lua_newtable(T);
    w = lua_gettop(T);
    lua_pushnil(L); /* first key */
    while (lua_next(L, t) != 0)
    {
        xcopy1(L, T, -2);
        xcopy1(L, T, lua_gettop(L));
        lua_settable(T, w);
        lua_pop(L, 1);
    }
    create_shared_table_inner(T);

    return 0;
}

int xcopy2_deep(lua_State *L, lua_State *T, int n, int deep)
{
    int f;
    const char* vl;
    switch (lua_type(L, n))
    {
    case LUA_TNIL:
        lua_pushnil(T);
        break;
    case LUA_TBOOLEAN:
        lua_pushboolean(T, lua_toboolean(L, n));
        break;
    case LUA_TNUMBER:
        f = lua_tointeger(L, n);
        lua_pushinteger(T, f);
        break;
    case LUA_TSTRING:
        vl = lua_tostring(L, n);
        lua_pushlstring(T, vl, strlen(vl));
        break;
    case LUA_TLIGHTUSERDATA:
        lua_pushlightuserdata(T, (void*) lua_touserdata(L, n));
        break;
    case LUA_TTABLE:
        if (deep)
        {
            xcopy_deep(L, T, n,deep-1);
        }
        else
        {
            lua_pushlightuserdata(T, (void*)lua_topointer(L,n));
        }
        break;
    default:
        luaL_error(L, "Error: Deep copy\n");
        //assert(0);
        break;
    }
    return 0;
}


/* table is in the stack at index 't' */
int xcopy_deep(lua_State *L, lua_State *T, int t, int deep)
{
    int w;
    lua_newtable(T);
    w = lua_gettop(T);
    lua_pushnil(L); /* first key */
    while (lua_next(L, t) != 0)
    {
        xcopy2_deep(L, T, -2,deep);
        if (lua_type(L, -1) == LUA_TTABLE)
        {
            if (deep)
            {
                xcopy_deep(L, T, lua_gettop(L),deep -1);
            }
            else
            {
                lua_pushlightuserdata(T, (void*)lua_topointer(L,lua_gettop(L)));
            }
        }
        else
            xcopy2_deep(L, T, -1,deep);
        lua_settable(T, w);
        lua_pop(L, 1);
    }
    return 0;
}
