# All Target

RM := rm -rf
BUILD_DIR := ./build
NFS_BUILD_DIR := /var/nfs/root/build
LUATEST_DIR := ./tests/lua-5.3.4-tests
TEST_DIR := ./tests/lunax-tests
LIBS_DIR := ./libs
HUB_BUILD_DIR ?= /var/nfs/root/build


.PHONY: all clean lua lib lua_clean lib_clean test_lua tests app app_clean


hub_all: all
	cp $(BUILD_DIR)/* $(HUB_BUILD_DIR)


nfs_all: test all
	cp $(BUILD_DIR)/* $(NFS_BUILD_DIR)

all: lua lib app

lib:lua

clean: lua_clean lib_clean app_clean
#$(RM) -rf $(BUILD_DIR)/*

# test:
# 	$(shell source /home/serge/invest/developer-package/SDK/environment-setup-cortexa7t2hf-neon-vfpv4-ostl-linux-gnueabi)

# Targets for lua-mod
lua:lua-mod
	mkdir -p $(BUILD_DIR)
	$(MAKE) -C $< linux
	cp lua-mod/src/luax $(BUILD_DIR)
	cp lua-mod/src/luaxc $(BUILD_DIR)
	cp lua-mod/src/libluax.a $(BUILD_DIR)
	cp lua-mod/src/libluax.so $(BUILD_DIR)
	cp lua-mod/src/lua.h $(BUILD_DIR)
	cp lua-mod/src/luaconf.h $(BUILD_DIR)
	cp lua-mod/src/lauxlib.h $(BUILD_DIR)
	cp lua-mod/src/lualib.h $(BUILD_DIR)


lua_clean:lua-mod
	$(MAKE) -C $< clean
	rm -rf $(BUILD_DIR)/luax
	rm -rf $(BUILD_DIR)/luaxc
	rm -rf $(BUILD_DIR)/libluax.a
	rm -rf $(BUILD_DIR)/libluax.so
	rm -rf $(BUILD_DIR)/lua.h
	rm -rf $(BUILD_DIR)/luaconf.h
	rm -rf $(BUILD_DIR)/lauxlib.h
	rm -rf $(BUILD_DIR)/lualib.h

# Targets for lunax-lib
lib:lunax-lib
	mkdir -p $(BUILD_DIR)
	$(MAKE) -C $< all
	cp $</lunax.so $(BUILD_DIR)
	cp $</lunax.so $(LIBS_DIR)

lib_clean:lunax-lib
	$(MAKE) -C $< clean
	rm -rf $(BUILD_DIR)/lunax.so
	rm -rf $(LIBS_DIR)/lunax.so

# Targets for luaxpp
app:luaxpp
	mkdir -p $(BUILD_DIR)
	$(MAKE) -C $< all
	cp $</luaxpp $(BUILD_DIR)

app_clean:luaxpp
	$(MAKE) -C $< clean
	rm -rf $(BUILD_DIR)/luaxpp
	rm -rf $(BUILD_DIR)/lunaxprj.lua

# Other Targets
test_lua: lua
	cd $(LUATEST_DIR) && ../../$(BUILD_DIR)/luax -e"_U=true" all.lua

tests:lua lib app
	cp $(LIBS_DIR)/luaunit.lua $(BUILD_DIR)
	$(eval export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(shell pwd)/build)
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_base/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_timers/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_events/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_threads/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_tasks/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_try/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_delete/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_events_ext/
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_pairs/
	
dbg:lua lib app
	cp $(LIBS_DIR)/luaunit.lua $(BUILD_DIR)
	$(eval export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(shell pwd)/build)
	cd $(BUILD_DIR) && ./luaxpp -test ../$(TEST_DIR)/testsuite_pairs/